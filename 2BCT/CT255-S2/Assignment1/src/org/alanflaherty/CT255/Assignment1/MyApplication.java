package org.alanflaherty.CT255.Assignment1;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;

/**
 * Created by alan on 15/01/18.
 */
public class MyApplication extends JFrame
{
    private int toolbar_height = 50;
    private int padding = 20;

    private int box_height = 20;
    private int box_width = 20;

    private int num_rows = 20;
    private int num_cols = 20;

    private int spacing = 2;

    public MyApplication()
    {
        Dimension windowsize = getPreferedSize();
        this.setSize(windowsize);
        this.setVisible(true);

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                System.out.println("clicked " + e.getComponent().getName());
                e.getComponent().repaint();
            }
        });
    }

    private Dimension getPreferedSize() {
        Dimension d = new Dimension( padding + (num_cols * box_width) + padding,
                toolbar_height + (num_rows * box_height) + padding );

        return d;
    }

    private Color randomColor() {
        Color c = new Color(random255(),random255(),random255());
        return c;
    }

    private int random255()
    {
        return (int)(Math.random()*256);
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        for(int i=0; i< num_cols; i++) {
            for(int j=0; j<num_rows; j++)
            {
                g.setColor(randomColor());
                g.fillRect( padding + (box_width * i), toolbar_height + (box_height * j),  box_width - spacing, box_height - spacing);
            }
        }
    }
}
