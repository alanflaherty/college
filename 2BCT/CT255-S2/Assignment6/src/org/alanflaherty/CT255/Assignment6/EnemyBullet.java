package org.alanflaherty.CT255.Assignment6;

import java.awt.*;

/**
 * Created by alan on 26/02/18.
 */

// TODO: bullet should be removed when it is off screen

public class EnemyBullet extends Sprite2D{
    private double dy = InvadersApplication.BULLET_SPEED;
    private int windowWidth;

    public EnemyBullet(Image i, int windowWidth){
        super(i, null, null);
        this.windowWidth = windowWidth;
    }

    public boolean move(){
        y += dy;

        if(y > 800) { // -10
            return true;
        }

        return false;
    }

    public void setBulletSpeed(double dy){
        this.dy = dy;
    }
}
