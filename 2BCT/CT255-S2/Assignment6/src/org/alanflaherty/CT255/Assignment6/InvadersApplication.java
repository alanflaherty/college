package org.alanflaherty.CT255.Assignment6;

import java.awt.*;
import java.awt.event.*;
import java.awt.geom.Rectangle2D;
import java.awt.image.*;
import java.util.*;
import java.util.Timer;
import javax.swing.*;

/**
 * Created by alan on 05/02/18.
 */
public class InvadersApplication extends JFrame implements Runnable, KeyListener {

    protected static final Dimension WindowSize = new Dimension (800, 600);

    private static boolean isGraphicsinitalised = false;
    private BufferStrategy strategy;

    // icons
    private Image shipImage;
    private Image shipImageDead;
    private Image alienImage_1;
    private Image alienImage_2;
    private Image alienImage_dead;
    private Image bulletImage;
    private Image enemyBulletImage1;
    private Image topShipImage1;
    private Image topShipImage2;

    // Player
    private Player PlayerShip;

    // Enemy Ships
    private Enemy[] AliensArray = new Enemy[NUMALIENS];

    // Ship at top of screen
    private EnemyTop TopShip;

    // player bullets
    private static int MAX_BULLETS = 3; // REDUCE to 3
    private ArrayList<PlayerBullet> bulletsList = new ArrayList<PlayerBullet>(MAX_BULLETS);

    // enemy bullets
    private ArrayList<EnemyBullet> enemyBullets = new ArrayList<EnemyBullet>();

    // start screen
    private StartScreen startScreen;

    // Game Setttings
    public static double BULLET_SPEED = 10;
    private static final int NUMALIENS = 30;

    private static int ALIEN_POINTS = 10;
    private static int TOPSHIP_POINTS = 10;
    private static int BULLET_POINTS = 50;

    private static int TOPSHIP_CHANCE_SHOOT = 85;
    private static int ENEMY_CHANCE_SHOOT = 25;

    private static double ENEMY_MOVE_SPEED = 1.5;
    private static double ENEMY_LEVEL_SPEED_INCREASE = 1.05;

    // Game State
    private GameState gameState = GameState.StartScreen;

    private int gameScore = 0;
    private int maxGameScore = 0;
    private int activeEnemyCount = NUMALIENS;

    // triggers for calling on main thread;
    private boolean shouldAddTopShip = false;
    private boolean shouldShootBullet = false;

    // timer
    private Timer timer;

    //constructor
    public InvadersApplication() {
        timer = new Timer("TopShip");
        startTopShipTimer();

        // load icons from resources
        loadAllIcons();

        //display the window, centered on the screen.
        Dimension screensize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int x = screensize.width/2 - WindowSize.width/2;
        int y = screensize.height/2 - WindowSize.height/2;

        setBounds(x, y, WindowSize.width, WindowSize.height);
        setVisible(true);
        setResizable(false);

        this.setTitle("Space Invaders, (sort of)");

        startScreen = new StartScreen(this);

        // create and initalise the new player's spaceship
        PlayerShip = new Player(shipImage, WindowSize.width);
        PlayerShip.setPosition(300, 530);

        // send keyboard events arriving into this JFrame back to its own event handlers
        addKeyListener(this);

        // create and start our animation
        Thread t = new Thread(this);
        t.start();

        createBufferStrategy(2);
        strategy = getBufferStrategy();

        isGraphicsinitalised = true;
    }

    private void startTopShipTimer() {
        timer.schedule(
                new TimerTask() {
                    @Override
                    public void run() {
                        shouldAddTopShip = true;
                    }
                },
                20000          // delay
        );
    }

    private Image loadImage(String imagePath){
        return Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images" + imagePath));
    }

    private void loadAllIcons(){
        // player
        shipImage = loadImage("/player_ship.png");
        shipImageDead = loadImage("/player_ship_dead.png");

        // player bullet
        bulletImage = loadImage("/bullet.png");

        // enemy ship
        alienImage_1 = loadImage("/alien_ship_1.png");
        alienImage_2 = loadImage("/alien_ship_2.png");
        alienImage_dead = loadImage("/alien_ship_dead.png");

        enemyBulletImage1 = loadImage("/alien_bullet.png");

        // top ship
        topShipImage1 = loadImage("/alien_big_1.png");
        topShipImage2 = loadImage("/alien_big_2.png");

        // TODO: Create custom dead image for topship..
    }

    private void startNewGame() {
        ENEMY_MOVE_SPEED = 1;
        gameScore = 0;
        initalGameOver = true;

        gameState = GameState.InProgress;

        bulletsList = new ArrayList<PlayerBullet>(MAX_BULLETS);
        enemyBullets = new ArrayList<EnemyBullet>(100);

        // reset trigger variables
        shouldAddTopShip = false;
        shouldShootBullet = false;

        startNewWave();

        TopShip = null;
        startTopShipTimer();
    }

    private void startNewWave() {
        // initalise variables
        ENEMY_MOVE_SPEED *= ENEMY_LEVEL_SPEED_INCREASE;
        activeEnemyCount = NUMALIENS;

        for(int i = 0; i < 5; i++) {
            for(int j = 0; j < 6; j++) {
                AliensArray[i+j*5] = new Enemy(alienImage_1, alienImage_2, alienImage_dead);
                AliensArray[i+j*5].setPosition( (i * 65) + 5, (j * 50) + 100);
                AliensArray[i+j*5].setXSpeed(ENEMY_MOVE_SPEED);
                AliensArray[i+j*5].initaliseDirection();
            }
        }
    }

    private void gameOver() {
        gameState = GameState.GameOver;

        // stop catching key presses while game over screen is shown, this should
        // prevent the user from gettting dropped straight back into the game.
        catchKeyPresses = false;
    }

    private boolean initalGameOver = true;
    public void run() {
        boolean moveDown = false;
        boolean moveDownNext = false;

        while(true) {
            // 1: sleep for 1/50 sec
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                System.out.printf("Thread sleep interrupted: ", e.getMessage());
            }

            if(gameState == GameState.GameOver) {

                if(initalGameOver) {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        System.out.printf("Thread sleep interrupted: ", e.getMessage());
                    }
                    initalGameOver = false;
                }

                // set start screen active
                // gameState = GameState.StartScreen;
            }
            else if(gameState == GameState.InProgress) {
                moveDown = moveDownNext;
                moveDownNext = false;

                // 2: animate game objects
                for (int i = 0; i < NUMALIENS; i++) {
                    if (AliensArray[i].draw) {
                        if (moveDown) {
                            AliensArray[i].moveDownSwitchDirection();
                            AliensArray[i].move();

                            // check for movedown count, start ship across screen at top
                        } else {
                            moveDownNext |= AliensArray[i].move();
                        }

                        if(AliensArray[i].alive && isLastInColumn(i) && randomChance(ENEMY_CHANCE_SHOOT)) {
                            EnemyBullet bullet = new EnemyBullet(enemyBulletImage1, WindowSize.width);
                            bullet.setPosition(AliensArray[i].x + (50/2), AliensArray[i].y + 32);
                            enemyBullets.add(bullet);
                        }
                    }
                }

                // add bullet here to avoid the problem with a ConcurrentModificationException
                // likely due to adding to the list from another thread
                if(shouldShootBullet){
                    shootBullet();
                }

                if(shouldAddTopShip && (TopShip== null || TopShip!= null && TopShip.alive == false)){
                    TopShip = new EnemyTop(topShipImage1,topShipImage2,alienImage_dead);
                    TopShip.setPosition(900, 50);
                    TopShip.setXSpeed(ENEMY_MOVE_SPEED);

                    shouldAddTopShip = false;
                }

                Iterator<PlayerBullet> playerBulletIterator = bulletsList.iterator();
                while(playerBulletIterator.hasNext()) { // concurrent modification..
                    PlayerBullet bullet = playerBulletIterator.next();

                    if (bullet.move() == true) {
                        playerBulletIterator.remove();
                        break;
                    }
                    if(TopShip != null && TopShip.alive && TopShip.collide(bullet)){
                        playerBulletIterator.remove();
                        TopShip.alive = false;

                        score(TOPSHIP_POINTS);

                        // TODO: check, this feels like a bit of a hack, but should stop these
                        // backing up
                        shouldAddTopShip =false;
                        startTopShipTimer();
                        break;
                    }
                    for (int i = 0; i < NUMALIENS; i++) {
                        if (AliensArray[i].collide(bullet)) {
                            AliensArray[i].alive = false;   //TODO: replace with method call here, behaviour

                            activeEnemyCount--;
                            playerBulletIterator.remove();
                            score(ALIEN_POINTS);
                        }
                    }
                }

                Iterator<EnemyBullet> it = enemyBullets.iterator();
                while(it.hasNext()) {
                    EnemyBullet bullet = it.next();
                    if (bullet.move() == true) {
                        it.remove();
                    }

                    if(PlayerShip.collide(bullet)) {
                        gameOver();
                    }

                    // Is this causing the issues with collection modification
                    Iterator<PlayerBullet> innerPlayerBulletIterator = bulletsList.iterator();
                    while(innerPlayerBulletIterator.hasNext()){
                        PlayerBullet playerBullet = innerPlayerBulletIterator.next();

                        // remove both bullets
                        if(playerBullet.collide(bullet)) {
                            it.remove();
                            innerPlayerBulletIterator.remove();

                            score(BULLET_POINTS);
                        }
                    }
                }

                if(TopShip != null) {
                    if (TopShip.move()) {
                        TopShip.reverseDirection();
                    }
                    if (TopShip.alive && randomChance(TOPSHIP_CHANCE_SHOOT)) {
                        EnemyBullet topShipBullet = new EnemyBullet(enemyBulletImage1, WindowSize.width);
                        topShipBullet.setPosition(TopShip.x + (64 / 2), TopShip.y + 32);
                        topShipBullet.setBulletSpeed(BULLET_SPEED * 1.5);
                        enemyBullets.add(topShipBullet);
                    }
                }

                PlayerShip.move();

                for (int i = 0; i < NUMALIENS; i++) {
                    if (AliensArray[i].collide(PlayerShip)) {
                        gameOver();
                        break;
                    }
                }

                if(activeEnemyCount == 0){
                    startNewWave();
                }
            }

            this.repaint(); // 3: force an application repaint
        }
    }

    private boolean isLastInColumn(int index){
        while(true){
            index += 5;
            if(index >= AliensArray.length) {
                return true;
            }
            if(AliensArray[index].alive == true) {
                return false;
            }
        }
    }

    public boolean randomChance(int percent){
        // this is a % chance per second, ie every 50 frames
        double result = Math.random();
        if( result < ((double)percent/(100.00 * 50)) ) {
            return true;
        }
        return false;
    }

    public void keyTyped(KeyEvent e){}

    private boolean catchKeyPresses = true;
    // Three keyboard Event-Handling functions
    public void keyPressed(KeyEvent e){
        if(catchKeyPresses) {
            if (e.getKeyCode() == KeyEvent.VK_LEFT) {
                PlayerShip.setXSpeed(-4);
            } else if (e.getKeyCode() == KeyEvent.VK_RIGHT) {
                PlayerShip.setXSpeed(4);
            } else if (e.getKeyCode() == KeyEvent.VK_SPACE) {
                if (gameState == GameState.StartScreen) {
                    startNewGame();
                } else if (gameState == GameState.InProgress) {
                    //shootBullet();
                    shouldShootBullet = true;
                } else {
                    // TODO: add highest score to the start screen
                    gameState = GameState.StartScreen;
                }
            }
        }
    }

    public void keyReleased(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_RIGHT)
            PlayerShip.setXSpeed(0);
    }

    public void shootBullet() {
        if(bulletsList.size() < MAX_BULLETS ) {
            // add a new bullet to our list
            PlayerBullet b = new PlayerBullet(bulletImage, WindowSize.width);
            b.setPosition(PlayerShip.x + 50 / 2, PlayerShip.y);
            bulletsList.add(b);
        }

        shouldShootBullet = false;
    }

    private void score(int points) {
        gameScore += points;
        if(gameScore > maxGameScore){
            maxGameScore = gameScore;
        }
    }

    // applications paint method
    public void paint(Graphics g) {

        if(isGraphicsinitalised) { // don't try to draw uninitalised objects!
            g = strategy.getDrawGraphics();

            if(gameState == GameState.StartScreen) {
                startScreen.render(g);
            }
            else if(gameState == GameState.InProgress) {
                paintGameObjects(g);
            }
            else {// Game State == GameOver

                //
                paintGameObjects(g, false);

                g.setColor(Color.WHITE);
                Font f = g.getFont();

                // Game Over
                Font newFont = f.deriveFont(100f);
                g.setFont(newFont);
                String gameOver = "Game Over";
                Rectangle2D stringBounds = g.getFontMetrics().getStringBounds(gameOver, null);

                g.setColor(Color.WHITE);
                g.drawString(gameOver, (WindowSize.width - (int) stringBounds.getWidth()) / 2, 300);

                // Game Score
                newFont = f.deriveFont(90f);
                g.setFont(newFont);
                String score = "Score " + gameScore;
                stringBounds = g.getFontMetrics().getStringBounds(score, null);

                g.setColor(Color.WHITE);
                g.drawString(score, (WindowSize.width - (int) stringBounds.getWidth()) / 2, 400);

                // catch key presses again after the screen is shown;
                catchKeyPresses = true;
            }

            // g.dispose();
            strategy.show();
        }
    }

    private void paintGameObjects(Graphics g) {
        paintGameObjects(g, true);
    }

    private void paintGameObjects(Graphics g, boolean paintScore) {
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, WindowSize.width, WindowSize.height);

        if (paintScore) {
            g.setColor(Color.WHITE);
            String score = "Best Score " + maxGameScore + " Game Score: " + gameScore;
            Rectangle2D stringBounds = g.getFontMetrics().getStringBounds(score, null);

            g.drawString(score, 790 - (int) stringBounds.getWidth(), 50);
        }

        // redraw all game objects
        for (int i = 0; i < NUMALIENS; i++) {
            AliensArray[i].paint(g);
        }

        Iterator iterator = bulletsList.iterator();
        while (iterator.hasNext()) {
            PlayerBullet b = (PlayerBullet) iterator.next();
            b.paint(g);
        }

        Iterator<EnemyBullet> it = enemyBullets.iterator();
        while (it.hasNext()) {
            it.next().paint(g);
        }

        if(TopShip!=null) {
            TopShip.paint(g);
        }

        PlayerShip.paint(g);
    }
}
