package org.alanflaherty.CT255.Assignment6;

import java.awt.*;

/**
 * Created by alan on 19/02/18.
 */
public class PlayerBullet extends Sprite2D{
    private double dy = InvadersApplication.BULLET_SPEED;
    private int windowWidth;

    public PlayerBullet(Image i, int windowWidth){
        super(i, null, null);
        this.windowWidth = windowWidth;

        // set position
    }

    public boolean move(){
        y -= dy;

        if(y < 50) { // -10
            return true;
        }

        return false;
    }
}
