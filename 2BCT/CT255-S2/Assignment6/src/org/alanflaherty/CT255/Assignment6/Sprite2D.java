package org.alanflaherty.CT255.Assignment6;

import java.awt.*;
/**
 * Created by alan on 05/02/18.
 */
public abstract class Sprite2D {
    // member data
    protected double x, y;
    protected double xSpeed = 0;
    protected Image myImage;
    protected Image myImage2;
    protected Image deadImage;

    long framesDrawn = 0;

    // todo: use method instead of setting property
    protected int deadFrameCount = 25;
    protected int deadFrames = 0;
    protected boolean alive = true;
    protected boolean draw = true;

    protected int animationFrameCount = 100;

    // constructor
    public Sprite2D(Image i, Image i_2, Image d){
        myImage = i;
        myImage2 = i_2;
        deadImage = d;
    }

    public abstract boolean move();

    public void setPosition(double xx, double yy) {
        x = xx;
        y = yy;
    }

    public void setXSpeed(double dx) {
        xSpeed =  dx;
    }

    public void paint(Graphics g) {

        if(!alive) {
            deadFrames++;
            if (deadFrames > deadFrameCount) {
                draw = false; // < remove from source list...
                return;
            }

            g.drawImage(deadImage, (int) x, (int) y, null);
        }
        else {
            if (myImage2 == null) {
                g.drawImage(myImage, (int) x, (int) y, null);
            } else {
                framesDrawn++;
                if (framesDrawn % animationFrameCount < (animationFrameCount/2))
                    g.drawImage(myImage, (int) x, (int) y, null);
                else
                    g.drawImage(myImage2, (int) x, (int) y, null);
            }
        }
    }

    // Todo: this collide method falls apart at higher speeds, need to take the
    // possibility of an interpolated collision before the next frame in the game
    public boolean collide(Sprite2D sprite) {
        if(!alive || !sprite.alive)
            return false;

        int w1 = this.myImage.getWidth(null);
        int h1 = this.myImage.getHeight(null);
        double x1 = this.x;
        double y1 = this.y;

        int w2 = sprite.myImage.getWidth(null);
        int h2 = sprite.myImage.getHeight(null);
        double x2 = sprite.x;
        double y2 = sprite.y;

        if (
                ( (x1<x2 && x1+w1>x2) || (x2<x1 && x2+w2>x1) ) &&
                        ( (y1<y2 && y1+h1>y2) || (y2<y1 && y2+h2>y1) )
                ) {

            return true;
        }

        return false;
    }
}
