package org.alanflaherty.CT255.Assignment6;

import javax.swing.*;
import java.awt.*;

/**
 * Created by alan on 05/03/18.
 */
public class StartScreen {
    private Image alienImage_1;
    private Image topShipImage1;
    private Image enemyBulletImage1;

    private InvadersApplication parentApplication;
    public StartScreen(InvadersApplication parentApplication){
        this.parentApplication = parentApplication;

        loadAllIcons();
    }

    public void render(Graphics g){

        //TODO: shouldn't need to do this as the
        //  Parent class should be clearing the screen
        //  for us.
        g.setColor(Color.BLACK);
        g.fillRect(0, 0, 800, 600);

        int left = 300;
        int top = 200;
        g.setColor(Color.WHITE);
        g.drawRect(left, top, 220, 200);

        //
        g.drawString("POINTS PER ENEMY", left + 50, top + 20);

        // Display each of the graphics with a points total
        g.drawImage(alienImage_1, left + 35, top + 40, null);
        g.drawString("10 Points", left + 130, top + 60);

        g.drawImage(topShipImage1, left + 30, top + 90, null);
        g.drawString("25 Points", left + 130, top + 110);

        g.drawImage(enemyBulletImage1, left + 55, top + 140, null);
        g.drawString("50 Points", left + 130, top + 150);

        g.drawString("Please press <SPACE> to start.", left + 10, top + 180);

    }

    // TODO: this is where a class should be extracted to manage all the
    //      images that need to be loaded, caching them on the first call.
    private void loadAllIcons() {
        // enemy targets
        alienImage_1 = loadImage("/alien_ship_1.png");
        enemyBulletImage1 = loadImage("/alien_bullet.png");
        topShipImage1 = loadImage("/alien_big_1.png");;
    }

    private Image loadImage(String imagePath) {
        return Toolkit.getDefaultToolkit().getImage(getClass().getResource("/images" + imagePath));
    }
}
