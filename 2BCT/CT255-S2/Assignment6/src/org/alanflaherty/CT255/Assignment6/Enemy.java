package org.alanflaherty.CT255.Assignment6;

import java.awt.*;

/**
 * Created by alan on 05/02/18.
 */
public class Enemy extends Sprite2D {
    public boolean isAlive = true;

    // constructor
    public Enemy(Image i, Image i2, Image d){
        super(i, i2, d);
    }

    private int direction = 1;

    public void initaliseDirection() {
        direction = 1;
    }

    public void moveDownSwitchDirection() {
        // y += myImage.getHeightr(null);
        y += 65;
        direction *= -1;
        x += (2 * direction);
    }

    @Override
    public boolean move() {
        x += (xSpeed * direction);
        boolean ret = false;

        if(x < xSpeed) {
            ret = true;
        }
        else if (x > (800 - (65 + xSpeed))){
            ret = true;
        }

        return ret;
    }
}
