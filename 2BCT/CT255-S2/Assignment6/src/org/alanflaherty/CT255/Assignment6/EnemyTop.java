package org.alanflaherty.CT255.Assignment6;

import java.awt.*;

/**
 * Created by alan on 26/02/18.
 */

public class EnemyTop  extends Sprite2D {
    private long framesActive = 0;

    // constructor
    public EnemyTop (Image i, Image i2, Image d){
        super(i, i2, d);

        animationFrameCount = 50;
    }

    private int direction = -1;

    public void reverseDirection() {
        direction *= -1;
        x += (xSpeed * direction);
    }

    @Override
    public boolean move() {

        if(alive) {
            framesActive++;
            x += (xSpeed * direction * (0.5 * (framesActive / 50) ));

            boolean ret = false;

            // this one does not stop at the edges and the speed should increase the longer it is on the screen.
            if (x < -100) {
                ret = true;
            } else if (x > 900) {
                ret = true;
            }

            return ret;
        }


        return false;
    }
}
