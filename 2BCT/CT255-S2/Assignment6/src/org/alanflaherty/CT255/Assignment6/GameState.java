package org.alanflaherty.CT255.Assignment6;

/**
 * Created by alan on 05/03/18.
 */
public enum GameState {
    StartScreen,
    InProgress,
    GameOver
}
