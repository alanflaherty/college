package org.alanflaherty.CT255.Assignment8;

/**
 * Created by alan on 12/03/18.
 */
public enum GameState {
    Paused,
    Running
}
