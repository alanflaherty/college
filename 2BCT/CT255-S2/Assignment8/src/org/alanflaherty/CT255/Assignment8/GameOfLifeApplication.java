package org.alanflaherty.CT255.Assignment8;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferStrategy;

/**
 * Created by alan on 05/03/18.
 */
public class GameOfLifeApplication extends JFrame implements Runnable, KeyListener, MouseListener {
    private static int CELL_COUNT_ROWS = 40;
    private static int CELL_COUNT_COLUMNS = 40;
    private static int CELL_SIZE = 15;
    private static int TOOLBAR_HEIGHT = 33;
    private static int LEFT_MARGIN = 5;
    private static int BOTTOM_MARGIN = 4;

    protected static final Dimension WindowSize = new Dimension (CELL_SIZE * CELL_COUNT_COLUMNS, CELL_SIZE * CELL_COUNT_ROWS + TOOLBAR_HEIGHT );
    private BufferStrategy strategy;

    private GameState gameState = GameState.Paused;
    private boolean[][][] cells;
    private boolean isGraphicsInitalised = false;

    private int currentScreen = 0;
    private int otherScreen = 1;

    public GameOfLifeApplication(){
        Dimension screensize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int x = screensize.width/2 - WindowSize.width/2;
        int y = screensize.height/2 - WindowSize.height/2;

        setBounds(x, y, WindowSize.width + LEFT_MARGIN, WindowSize.height + BOTTOM_MARGIN);
        setVisible(true);
        setResizable(false);

        this.setTitle("Game of Life");

        addKeyListener(this);
        addMouseListener(this);

        Thread t = new Thread(this);
        t.start();

        createBufferStrategy(2);
        strategy = getBufferStrategy();

        // initalise state
        cells = new boolean[40][40][2];   // extract cols and rows variables
        for (int i = 0; i < CELL_COUNT_COLUMNS; i++) { // TODO: unroll loop
            for (int j = 0; j < CELL_COUNT_ROWS; j++) {
                cells[i][j][currentScreen] = false;
                cells[i][j][otherScreen] = false;
            }
        }

        isGraphicsInitalised = true;
    }

    public void run() {
        while(true) {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                System.out.printf("Thread sleep interrupted: ", e.getMessage());
            }

            if(gameState == GameState.Running) {
                if (isGraphicsInitalised) {
                    //cells[0][0][currentScreen] = !cells[0][0][currentScreen];
                    //cells[0][39][currentScreen] = !cells[0][39][currentScreen];
                    //cells[39][0][currentScreen] = !cells[39][0][currentScreen];
                    //cells[39][39][currentScreen] = !cells[39][39][currentScreen];

                    if(!iterateGameOfLife()) {
                        gameState = GameState.Paused;
                    }
                }
            }

            repaint();
        }
    }

    public void paint(Graphics g){
        Font bold = null;

        if(isGraphicsInitalised) {
            g = strategy.getDrawGraphics();
            if(bold == null) {
                bold = g.getFont().deriveFont(Font.BOLD);
            }
            g.setFont(bold);

            g.setColor(Color.PINK);
            g.fillRect(0, 0, 800, 800);

            for (int i = 0; i < CELL_COUNT_COLUMNS; i++) { // TODO: unroll loop
                for (int j = 0; j < CELL_COUNT_ROWS; j++) {
                    if (cells[i][j][currentScreen]) {
                        g.setColor(Color.BLACK);
                    } else {
                        g.setColor(Color.WHITE);
                    }

                    g.fillRect( (i * CELL_SIZE) + LEFT_MARGIN, (j * CELL_SIZE) + TOOLBAR_HEIGHT, CELL_SIZE, CELL_SIZE);
                }
            }

            if(gameState == GameState.Paused) {
                // paint randomise button
                g.setColor(Color.GREEN);
                g.fillRect(LEFT_MARGIN + 10, TOOLBAR_HEIGHT + 10, 70, 20);

                g.setColor(Color.BLACK);
                g.drawString("Random", LEFT_MARGIN + 15, TOOLBAR_HEIGHT + 25);

                // paint start
                g.setColor(Color.GREEN);
                g.fillRect(LEFT_MARGIN + 90, TOOLBAR_HEIGHT + 10, 50, 20);

                g.setColor(Color.BLACK);
                g.drawString("Start", LEFT_MARGIN + 95, TOOLBAR_HEIGHT + 25);
            }

            strategy.show();
        }
    }

    // Key Events
    public void keyTyped(KeyEvent e) {}

    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_S){
            resetCells();

            // TODO: START/STOP cellular Automata
        }
    }

    private void resetCells() {
        for (int i = 0; i < CELL_COUNT_COLUMNS; i++) { // TODO: unroll loop
            for (int j = 0; j < CELL_COUNT_ROWS; j++) {
                cells[i][j][currentScreen] = false;
            }
        }
    }

    private void randomise(){
        // initalise state
        cells = new boolean[40][40][2];   // extract cols and rows variables
        for (int i = 0; i < CELL_COUNT_COLUMNS; i++) { // TODO: unroll loop
            for (int j = 0; j < CELL_COUNT_ROWS; j++) {
                cells[i][j][currentScreen] = (Math.random() <= 0.25);
            }
        }
    }

    private boolean iterateGameOfLife(){
        boolean change = false;
        for (int x=0;x<40;x++) {
            for (int y=0;y<40;y++) {
                // count the live neighbours of cell [x][y][0]
                int neighbours = 0;
                for (int xx=-1;xx<=1;xx++) {
                    for (int yy=-1;yy<=1;yy++) {
                        if (xx!=0 || yy!=0) {
                            // [x+xx][y+yy][0]
                            // check cell [x+xx][y+yy][0]
                            // but.. what if x+xx==-1, etc. ?
                            int cX, cY;
                            cX = x+xx;
                            cY = y+yy;

                            if(cX == -1 || cX == 40){
                                cX = 39;
                            }
                            if(cY == -1 || cY == 40){
                                cY = 39;
                            }
                            if(cells[cX][cY][currentScreen]) {
                                neighbours++;
                            }
                        }
                    }
                }

                cells[x][y][otherScreen] = (
                        (cells[x][y][currentScreen] == true && (neighbours == 2 || neighbours ==3))
                        || (cells[x][y][currentScreen] == false && (neighbours ==3))
                );

                change |= (cells[x][y][currentScreen] != cells[x][y][otherScreen]);
            }
        }

        // flip the display screen
        currentScreen = (currentScreen + 1) % 2;
        otherScreen = (currentScreen + 1) % 2;

        return change;
    }

    public void keyReleased(KeyEvent e) {}


    // Mouse Events
    public void mouseExited(MouseEvent e) {}

    public void mouseEntered(MouseEvent e) {}

    public void mouseReleased(MouseEvent e) {}

    public void mousePressed(MouseEvent e) {}

    public void mouseClicked(MouseEvent e) {
        int cellX = Math.floorDiv(e.getX() - LEFT_MARGIN, CELL_SIZE);
        int cellY = Math.floorDiv(e.getY() - TOOLBAR_HEIGHT, CELL_SIZE);

        cells[cellX][cellY][currentScreen] = !cells[cellX][cellY][currentScreen];

        if(gameState == GameState.Paused) {
            // randomise board
            Rectangle randomBtnBounds = new Rectangle(LEFT_MARGIN + 10, TOOLBAR_HEIGHT + 10, 70, 20);
            if (randomBtnBounds.contains(e.getX(), e.getY())) {
                randomise();
            }

            // start game of life
            Rectangle startBtnBounds = new Rectangle(LEFT_MARGIN + 90, TOOLBAR_HEIGHT + 10, 50, 20);
            if (startBtnBounds.contains(e.getX(), e.getY())) {
                gameState = GameState.Running;
                // iterateGameOfLife();
            }
        }
        // this.repaint();
    }
}
