package org.alanflaherty.CT255.Assignment9;

import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.DataFlavor;
import java.awt.datatransfer.UnsupportedFlavorException;
import java.awt.event.*;
import java.awt.image.BufferStrategy;
import java.io.*;
import java.util.zip.CRC32;

import static javax.swing.JOptionPane.ERROR_MESSAGE;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;

/**
 * Created by alan on 05/03/18.
 *
 *  header format
 *  file extension:  .gli
 *  space is currently 40 x 40 = 1600 values
 *
 *  # Header
 *  w   width
 *  h   height
 *  len length
 *  ch  checksum
 *                                 |-w-| |-h-| |-len--| |--ch--|
 *  |.GLI.WWHHLLLCCC.| 89 71 76 73 ?? ?? ?? ?? ?? ?? ?? ?? ?? ?? 0A
 *
 *  # Content
 *  |................| 01 00 01 00 01 00 01 00 01 00 01 00 01 00 01
 *  |................| 01 00 01 00 01 00 01 00 01 00 01 00 01 00 01
 *  |................| 01 00 01 00 01 00 01 00 01 00 01 00 01 00 01
 *  ...
 *
 *  # Footer
 *  |END..|            45 4e 44 0d 0a
 */

public class GameOfLifeApplication extends JFrame implements Runnable, KeyListener, MouseListener, MouseMotionListener{
    private static int CELL_COUNT_ROWS = 40;
    private static int CELL_COUNT_COLUMNS = 40;
    private static int CELL_SIZE = 15;
    private static int TOOLBAR_HEIGHT = 33;
    private static int LEFT_MARGIN = 5;
    private static int BOTTOM_MARGIN = 4;

    protected static final Dimension WindowSize = new Dimension(CELL_SIZE * CELL_COUNT_COLUMNS, CELL_SIZE * CELL_COUNT_ROWS + TOOLBAR_HEIGHT);
    private BufferStrategy strategy;

    private GameState gameState = GameState.Paused;
    private boolean[][][] cells;
    private boolean isGraphicsInitalised = false;

    private int currentScreen = 0;
    private int otherScreen = 1;

    public GameOfLifeApplication() {
        Dimension screensize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int x = screensize.width / 2 - WindowSize.width / 2;
        int y = screensize.height / 2 - WindowSize.height / 2;

        setBounds(x, y, WindowSize.width + LEFT_MARGIN, WindowSize.height + BOTTOM_MARGIN);
        setVisible(true);
        setResizable(false);

        this.setTitle("Game of Life");

        addKeyListener(this);
        addMouseListener(this);
        addMouseMotionListener(this);

        Thread t = new Thread(this);
        t.start();

        createBufferStrategy(2);
        strategy = getBufferStrategy();

        // initalise state
        cells = new boolean[CELL_COUNT_COLUMNS][CELL_COUNT_ROWS][2];
        for (int i = 0; i < CELL_COUNT_COLUMNS; i++) {
            for (int j = 0; j < CELL_COUNT_ROWS; j++) {
                cells[i][j][currentScreen] = false;
                cells[i][j][otherScreen] = false;
            }
        }

        isGraphicsInitalised = true;
    }

    private void copyBuffer(int from, int to){
        for (int i = 0; i < CELL_COUNT_COLUMNS; i++) {
            for (int j = 0; j < CELL_COUNT_ROWS; j++) {
                cells[i][j][to] = cells[i][j][from];
            }
        }
    }

    public void run() {
        while (true) {
            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                System.out.printf("Thread sleep interrupted: ", e.getMessage());
            }

            if (gameState == GameState.Running) {
                if (isGraphicsInitalised) {
                    if (!iterateGameOfLife()) {
                        gameState = GameState.Paused;
                    }
                }
            }

            repaint();
        }
    }

    private boolean iterateGameOfLife() {
        boolean change = false;
        for (int x = 0; x < 40; x++) {
            for (int y = 0; y < 40; y++) {
                int neighbours = 0;
                for (int xx = -1; xx <= 1; xx++) {
                    for (int yy = -1; yy <= 1; yy++) {
                        if (xx != 0 || yy != 0) {
                            int cX, cY;
                            cX = x + xx;
                            cY = y + yy;

                            if (cX == -1) {
                                cX = CELL_COUNT_COLUMNS - 1;
                            }
                            else if (cX == CELL_COUNT_COLUMNS){
                                cX = 0;
                            }

                            if (cY == -1) {
                                cY = CELL_COUNT_ROWS - 1;
                            }
                            else if (cY == CELL_COUNT_ROWS) {
                                cY = 0;
                            }

                            if (cells[cX][cY][currentScreen]) {
                                neighbours++;
                            }
                        }
                    }
                }

                cells[x][y][otherScreen] = (
                        (cells[x][y][currentScreen] == true && (neighbours == 2 || neighbours == 3))
                                || (cells[x][y][currentScreen] == false && (neighbours == 3))
                );

                change |= (cells[x][y][currentScreen] != cells[x][y][otherScreen]);
            }
        }

        // flip the display screen
        currentScreen = (currentScreen + 1) % 2;
        otherScreen = (currentScreen + 1) % 2;

        return change;
    }

    public void paint(Graphics g) {
        Font bold = null;

        if (isGraphicsInitalised) {
            g = strategy.getDrawGraphics();
            if (bold == null) {
                bold = g.getFont().deriveFont(Font.BOLD);
            }
            g.setFont(bold);

            g.setColor(Color.PINK);
            g.fillRect(0, 0, 800, 800);

            for (int i = 0; i < CELL_COUNT_COLUMNS; i++) {
                for (int j = 0; j < CELL_COUNT_ROWS; j++) {
                    if (cells[i][j][currentScreen]) {
                        g.setColor(Color.BLACK);
                    } else {
                        g.setColor(Color.WHITE);
                    }

                    g.fillRect((i * CELL_SIZE) + LEFT_MARGIN, (j * CELL_SIZE) + TOOLBAR_HEIGHT, CELL_SIZE, CELL_SIZE);
                }
            }

            if (gameState == GameState.Paused) {
                // paint randomise button
                g.setColor(Color.GREEN);
                g.fillRect(LEFT_MARGIN + 10, TOOLBAR_HEIGHT + 10, 70, 20);

                g.setColor(Color.BLACK);
                g.drawString("Random", LEFT_MARGIN + 15, TOOLBAR_HEIGHT + 25);

                // paint start
                g.setColor(Color.GREEN);
                g.fillRect(LEFT_MARGIN + 90, TOOLBAR_HEIGHT + 10, 50, 20);

                g.setColor(Color.BLACK);
                g.drawString("Start", LEFT_MARGIN + 95, TOOLBAR_HEIGHT + 25);

                // paint load
                g.setColor(Color.GREEN);
                g.fillRect(LEFT_MARGIN + 150, TOOLBAR_HEIGHT + 10, 50, 20);

                g.setColor(Color.BLACK);
                g.drawString("Load", LEFT_MARGIN + 155, TOOLBAR_HEIGHT + 25);

                // paint save
                g.setColor(Color.GREEN);
                g.fillRect(LEFT_MARGIN + 210, TOOLBAR_HEIGHT + 10, 50, 20);

                g.setColor(Color.BLACK);
                g.drawString("Save", LEFT_MARGIN + 215, TOOLBAR_HEIGHT + 25);
            }

            strategy.show();
        }
    }

    // Key Events
    public void keyPressed(KeyEvent e) {
        // write this out on the right hand side of the screen
        if (e.getKeyCode() == KeyEvent.VK_V && e.isControlDown()){
            Clipboard c = Toolkit.getDefaultToolkit().getSystemClipboard();

            String clipboard = "";
            try {
                clipboard = c.getData(DataFlavor.stringFlavor).toString();
            }
            catch(IOException ioex){
                System.err.print(ioex.getMessage());
            }
            catch(UnsupportedFlavorException ufe){
                System.err.print(ufe.getMessage());
            }

            importZeroAndOneBasedInput(clipboard);
            // JOptionPane.showMessageDialog(this, clipboard, "Clipboard contents", INFORMATION_MESSAGE);
        }
        else if (e.getKeyCode() == KeyEvent.VK_S) {
            resetCells();
            copyBuffer(currentScreen, otherScreen);
        }
        else if (e.getKeyCode() == KeyEvent.VK_P) {
            gameState = GameState.Paused;
        }
    }

    private void importZeroAndOneBasedInput(String clipboard) {
        String[] lines = clipboard.split("\n");

        resetCells();
        for (int i =0; i < lines.length; i++) {
            String line = lines[i];
            for(int j=0; j < line.length(); j++) {
                cells[i][j][currentScreen] = (line.charAt(j) == '1');
            }
        }
    }

    public void keyTyped(KeyEvent e) {}
    public void keyReleased(KeyEvent e) {}

    // Mouse Events
    private boolean mouseDown = false;
    public void mousePressed(MouseEvent e) {
        mouseDown = true;
    }

    public void mouseReleased(MouseEvent e) {
        mouseDown = false;
    }

    @Override
    public void mouseDragged(MouseEvent e) {
        if(mouseDown) {
            int cellX = Math.floorDiv(e.getX() - LEFT_MARGIN, CELL_SIZE);
            int cellY = Math.floorDiv(e.getY() - TOOLBAR_HEIGHT, CELL_SIZE);

            if(cellX < CELL_COUNT_COLUMNS && cellY < CELL_COUNT_ROWS) {
                cells[cellX][cellY][currentScreen] = !cells[cellX][cellY][otherScreen];

                System.out.printf("[mouse dragged] x: %d y: %d\n", e.getX(), e.getY());
            }
        }
    }

    public void mouseClicked(MouseEvent e) {
        int cellX = Math.floorDiv(e.getX() - LEFT_MARGIN, CELL_SIZE);
        int cellY = Math.floorDiv(e.getY() - TOOLBAR_HEIGHT, CELL_SIZE);

        boolean buttonClicked = false;
        if (gameState == GameState.Paused) {
            // randomise board
            Rectangle randomBtnBounds = new Rectangle(LEFT_MARGIN + 10, TOOLBAR_HEIGHT + 10, 70, 20);
            if (randomBtnBounds.contains(e.getX(), e.getY())) {
                buttonClicked = true;
                randomise();
            }

            // start game of life
            Rectangle startBtnBounds = new Rectangle(LEFT_MARGIN + 90, TOOLBAR_HEIGHT + 10, 50, 20);
            if (startBtnBounds.contains(e.getX(), e.getY())) {
                buttonClicked = true;
                gameState = GameState.Running;
            }

            // start game of life
            Rectangle loadBtnBounds = new Rectangle(LEFT_MARGIN + 150, TOOLBAR_HEIGHT + 10, 50, 20);
            if (loadBtnBounds.contains(e.getX(), e.getY())) {
                buttonClicked = true;
                loadState();
            }

            // start game of life
            Rectangle saveBtnBounds = new Rectangle(LEFT_MARGIN + 210, TOOLBAR_HEIGHT + 10, 50, 20);
            if (saveBtnBounds.contains(e.getX(), e.getY())) {
                buttonClicked = true;
                saveState();
            }
        }

        if(!buttonClicked) {
            cells[cellX][cellY][currentScreen] = !cells[cellX][cellY][currentScreen];
        }
    }

    public void mouseExited(MouseEvent e) {}
    public void mouseEntered(MouseEvent e) {}
    @Override
    public void mouseMoved(MouseEvent e) {}

    // Save and Load
    private void saveState() {
        final JFileChooser fc = new JFileChooser();

        fc.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }

                return f.getName().endsWith(".gli");
            }

            @Override
            public String getDescription() {
                return "Game of life (*.gli)";
            }
        });

        int returnVal = fc.showSaveDialog(this);

        String saveFileName = fc.getSelectedFile().getPath();
        if(!saveFileName.endsWith(".gli")){
            saveFileName += ".gli";
        }

        if(returnVal!= -1) {
            try {
                byte[] contentBytes = new byte[(CELL_COUNT_ROWS * CELL_COUNT_COLUMNS)];

                for (int i = 0; i < CELL_COUNT_ROWS; i++) {
                    for (int j = 0; j < CELL_COUNT_COLUMNS; j++) {
                        // todo: read 8 bits into a byte, should compress values
                        // using 0x1 and 0x0 essentially waste 15 bits
                        contentBytes[(j * CELL_COUNT_COLUMNS) + i] = (byte) (cells[i][j][currentScreen] == true ? 0x01 : 0x00);
                    }
                }

                long checksum = chk(contentBytes);
                byte[] header = new byte[]{'G', 'L', 'I', 0, 40, 0, 40};

                DataOutputStream output = new DataOutputStream(new FileOutputStream(saveFileName));
                // header
                output.write(header);
                output.writeLong(checksum);
                output.write(10);
                // content
                output.write(contentBytes);
                // end
                output.write(new byte[]{'E', 'N', 'D', '\r', '\n'});
                output.flush();
                output.close();
            }
            catch (Exception ex) // replace catch all
            {
                System.out.print(ex.getMessage());
            }
        }
    }

    private void loadState() {
        //Create a file chooser
        final JFileChooser fc = new JFileChooser();

        // set the file filter
        fc.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }

                return f.getName().endsWith(".gli");
            }

            @Override
            public String getDescription() {
                return "Game of life (*.gli)";
            }
        });

        int returnVal = fc.showOpenDialog(this);
        if(returnVal != -1) {
            try {
                FileInputStream is = new FileInputStream(fc.getSelectedFile().getPath());

                // create new data input stream
                DataInputStream dis = new DataInputStream(is);

                // read header
                char G = (char) dis.readUnsignedByte();
                fileStructureCheck(G == 'G', "Expected start of 'GLI' header");

                char L = (char) dis.readUnsignedByte();
                fileStructureCheck(L == 'L', "Expected part of 'GLI' header");

                char I = (char) dis.readUnsignedByte();
                fileStructureCheck(I == 'I', "Expected end of 'GLI' header");

                int width = (dis.readByte() << 8) + dis.readByte();
                int height = (dis.readByte() << 8) + dis.readByte();

                long checksum = dis.readLong();

                char fn = (char) dis.readByte();
                fileStructureCheck(fn == '\n', "Expected '0x0a' at end of  header");

                int contentSize = width * height;
                byte[] content = new byte[contentSize];
                dis.read(content, 0, contentSize);

                // verify checksum
                long calculatedCheckSum = chk(content);
                fileStructureCheck(checksum == calculatedCheckSum, "Invalid checksum," +
                        " calculated: " + Long.toHexString(calculatedCheckSum) +
                        " file checksum: " + Long.toHexString(checksum));

                if (checksum != calculatedCheckSum) {
                    JOptionPane.showMessageDialog(this, "File contents does not match the provided checksum.", "Invalid checksum", ERROR_MESSAGE);
                }

                for (int i = 0; i < CELL_COUNT_ROWS; i++) {
                    for (int j = 0; j < CELL_COUNT_COLUMNS; j++) {
                        // todo: read 8 bits from a byte, should compress values
                        cells[i][j][currentScreen] = (content[(j * CELL_COUNT_COLUMNS) + i] == 0x1);
                    }
                }

                char E = (char) dis.readUnsignedByte();
                fileStructureCheck(E == 'E', "Expected Start of 'END' block ");

                char N = (char) dis.readUnsignedByte();
                fileStructureCheck(N == 'N', "Expected Part of 'END' block ");

                char D = (char) dis.readUnsignedByte();
                fileStructureCheck(D == 'D', "Expected End of 'END' block ");

                char CR = (char) dis.readUnsignedByte();
                fileStructureCheck(CR == '\r', "Expected 0d ");

                char NL = (char) dis.readUnsignedByte();
                fileStructureCheck(NL == '\n', "Expected 0a ");

                // dis.close();
                is.close();
            }
            catch (FileNotFoundException fex){
                System.err.println("Error: [" + fex.getClass().getName() + "]");
                System.err.println(fex.getMessage());
            }
            catch (IOException ex){
                System.err.println("Error: [" + ex.getClass().getName() + "]");
                System.err.println(ex.getMessage());
            }
            finally {
                copyBuffer(currentScreen, otherScreen);
            }
        }
    }

    private void resetCells() {
        for (int i = 0; i < CELL_COUNT_COLUMNS; i++) {
            for (int j = 0; j < CELL_COUNT_ROWS; j++) {
                cells[i][j][currentScreen] = false;
            }
        }
    }

    private void randomise() {
        cells = new boolean[CELL_COUNT_COLUMNS][CELL_COUNT_ROWS][2];
        for (int i = 0; i < CELL_COUNT_COLUMNS; i++) {
            for (int j = 0; j < CELL_COUNT_ROWS; j++) {
                cells[i][j][currentScreen] = (Math.random() <= 0.25);
            }
        }
    }

    private long chk(byte[] dataBytes) {
        CRC32 crc = new CRC32();
        crc.update(dataBytes);
        return crc.getValue();
    }

    private void fileStructureCheck(boolean valid, String message){
        if(!valid) {
            System.err.println(message);
        }
    }
}