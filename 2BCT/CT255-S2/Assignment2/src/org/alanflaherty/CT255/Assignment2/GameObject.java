package org.alanflaherty.CT255.Assignment2;

import java.awt.*;

/**
 * Created by alan on 07/02/18.
 */
public class GameObject {

    // member
    private double x,y;
    private Color c;

    private int box_height = 20;
    private int box_width = 20;

    // constructor
    public GameObject(){
        x = Math.random() * 800;
        y = Math.random() * 600;

        c = randomColor();
    }

    private Color randomColor() {
        Color c = new Color(random255(),random255(),random255());
        return c;
    }

    private int random255() {
        return (int)(Math.random()*256);
    }

    public void move() {
        x += (Math.random() * 10) - 5;
        y += (Math.random() * 10) - 5;
    }

    public void paint(Graphics g){
        g.setColor( c );
        g.fillRect( (int)x, (int)y,  box_width, box_height);
    }
}
