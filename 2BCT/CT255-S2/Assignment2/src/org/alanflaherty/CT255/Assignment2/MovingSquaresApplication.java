package org.alanflaherty.CT255.Assignment2;

import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import javax.swing.*;

/**
 * Created by alan on 15/01/18.
 */
public class MovingSquaresApplication extends JFrame implements Runnable
{
    private static final Dimension WindowSize = new Dimension(800, 600);
    private static final int NUMGAMEOBJECTS = 30;
    private GameObject[] GameObjectsArray = new GameObject[NUMGAMEOBJECTS];

    public MovingSquaresApplication() {
        this.setSize(WindowSize );
        this.setVisible(true);

        initaliseBoxes();

        this.addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                System.out.println("clicked " + e.getComponent().getName());
                initaliseBoxes();
            }
        });

        Thread t = new Thread(this);
        t.start();
    }

    private void initaliseBoxes() {
        for(int i=0; i<NUMGAMEOBJECTS; i++){
            GameObjectsArray[i] = new GameObject();
        }
    }

    public void run() {
        //
        while(true){
            for(int i=0; i < NUMGAMEOBJECTS; i++) {
                GameObjectsArray[i].move();
            }

            try {
                Thread.sleep(20);
            }catch (InterruptedException e){}

            this.repaint();
        }
    }

    @Override
    public void paint(Graphics g) {
        super.paint(g);

        for(int i=0; i < NUMGAMEOBJECTS; i++) {
            GameObjectsArray[i].paint(g);
        }
    }
}
