package org.alanflaherty.CT255.Assignment4;

import java.awt.*;

/**
 * Created by alan on 05/02/18.
 */
public class Player extends Sprite2D {
    // constructor
    public Player(Image i){
        super(i);
    }

    @Override
    public boolean move()
    {
        x += xSpeed;
        return false;
    }
}
