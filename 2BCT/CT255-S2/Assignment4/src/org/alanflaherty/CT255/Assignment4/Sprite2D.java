package org.alanflaherty.CT255.Assignment4;

import java.awt.*;
/**
 * Created by alan on 05/02/18.
 */
public abstract class Sprite2D {
    // member data
    protected double x, y;
    protected double xSpeed = 0;
    protected Image myImage;

    // constructor
    public Sprite2D(Image i){
        // x = Math.random() * 800;
        // y = Math.random() * 600;
        myImage = i;
    }

    public abstract boolean move();

    // public interface
    public boolean moveEnemy() {
        x += 2;
        // * (Math.random() - Math.random());
        // y += 10 * (Math.random() - Math.random());

        boolean ret = false;

        if(x < 0) {
            ret = true;
        }
        else if (x > (800 - 65)){
            ret = true;
        }
/*
        if(y < 50) {
            y = 50;
        }
        else if (y > 400){
            y = 400;
        }
*/
        return ret;
    }

    public void setPosition(double xx, double yy) {
        x = xx;
        y = yy;
    }

    public void setXSpeed(double dx) {
        xSpeed =  dx;
    }

    public void paint(Graphics g) {
        g.drawImage(myImage, (int)x, (int)y, null);
    }
}
