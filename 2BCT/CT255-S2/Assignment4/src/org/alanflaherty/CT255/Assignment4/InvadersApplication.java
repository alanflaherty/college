package org.alanflaherty.CT255.Assignment4;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import javax.swing.*;

/**
 * Created by alan on 05/02/18.
 */
public class InvadersApplication extends JFrame implements Runnable, KeyListener {

    private static String workingDirectory;
    private static boolean isGraphicsinitalised = false;
    private static final Dimension WindowSize = new Dimension (800, 600);
    private static final int NUMALIENS = 30;
    private Enemy[] AliensArray = new Enemy[NUMALIENS];
    private Player PlayerShip;

    private BufferStrategy strategy;

    //constructor
    public InvadersApplication() {

        // get working directory
        workingDirectory = "/home/alan/git/2BCT/CT255-Semester2/Assignment4/images";

        // workingDirectory = System.getProperty("users.dir");
        System.out.printf("workingDirectory: %s\n", workingDirectory);

        // using idea this prints out null on linux
        System.out.printf("users.dir:\n" + System.getProperty("users.dir"));

        //display the window, centered on the screen.
        Dimension screensize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int x = screensize.width/2 - WindowSize.width/2;
        int y = screensize.height/2 - WindowSize.height/2;

        setBounds(x, y, WindowSize.width, WindowSize.height);
        setVisible(true);

        this.setTitle("Space Invaders, (sort of)");

        ImageIcon icon = new ImageIcon(workingDirectory + "/alien.png");
        Image alienImage = icon.getImage();

        // create and initalise some aliens, passing them each the image we have loaded.
        // AlienRow, AlienCol
        for(int i = 0; i < 5; i++) {
            for(int j = 0; j < 6; j++) {
                AliensArray[i+j*5] = new Enemy(alienImage);
                AliensArray[i+j*5].setPosition( (i * 65) , j * 50);
            }
        }

        // create and initalise the new player's spaceship
        icon = new ImageIcon(workingDirectory + "/player.png");
        Image shipImage = icon.getImage();
        PlayerShip = new Player(shipImage);
        PlayerShip.setPosition(300, 530);

        // send keyboard events arriving into this JFrame back to its own event handlers
        addKeyListener(this);

        // create and start our animation
        Thread t = new Thread(this);
        t.start();

        createBufferStrategy(2);
        strategy = getBufferStrategy();

        isGraphicsinitalised = true;
    }

    public void run() {
        boolean moveDown = false;
        boolean moveDownNext = false;

        while(true) {
            moveDown = moveDownNext;
            moveDownNext = false;
            // 1: sleep for 1/50 sec
            try {
                Thread.sleep(20);
            }catch (InterruptedException e){}

            // 2: animate game objects
            for(int i = 0; i < NUMALIENS; i++) {
                if(moveDown){
                    AliensArray[i].moveDownSwitchDirection();
                    AliensArray[i].move();
                }
                else{
                    moveDownNext |= AliensArray[i].move();
                }
            }

            PlayerShip.move();

            this.repaint(); // 3: force an application repaint
        }
    }

    // Three keyboard Event-Handling functions
    public void keyPressed(KeyEvent e){
        if(e.getKeyCode() == KeyEvent.VK_LEFT)
            PlayerShip.setXSpeed(-4);
        else if (e.getKeyCode() == KeyEvent.VK_RIGHT)
            PlayerShip.setXSpeed(4);
    }

    public void keyReleased(KeyEvent e){
        if(e.getKeyCode() == KeyEvent.VK_LEFT || e.getKeyCode() == KeyEvent.VK_RIGHT)
            PlayerShip.setXSpeed(0);
    }

    public void keyTyped(KeyEvent e){}

    // applications paint method
    public void paint(Graphics g) {

        if(isGraphicsinitalised) { // don't try to draw uninitalised objects!
            g = strategy.getDrawGraphics();

            // clear the canvas with a big black rectangle
            g.setColor(Color.BLACK);
            g.fillRect(0, 0, WindowSize.width, WindowSize.height);

            // redraw all game objects
            for( int i = 0; i < NUMALIENS; i++ ){
                AliensArray[i].paint(g);
            }

            PlayerShip.paint(g);

            // g.dispose();
            strategy.show();
        }
    }
}
