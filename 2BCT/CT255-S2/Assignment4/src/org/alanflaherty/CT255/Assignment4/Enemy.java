package org.alanflaherty.CT255.Assignment4;

import java.awt.*;

/**
 * Created by alan on 05/02/18.
 */
public class Enemy extends Sprite2D {
    // constructor
    public Enemy(Image i){
        super(i);
    }

    private int direction = -1;
    public void moveDownSwitchDirection() {
        // y += myImage.getHeightr(null);
        y += 65;
        direction *= -1;
        x += (2 * direction);
    }

    @Override
    public boolean move()
    {
        x += (2 * direction);
        boolean ret = false;

        if(x < 2) {
            ret = true;
        }
        else if (x > (800 - (65 + 2))){
            ret = true;
        }

        return ret;
    }
}
