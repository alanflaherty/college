package org.alanflaherty.CT255.Assignment10;

/**
 * Created by alan on 16/04/18.
 */
public class AStarNode implements Comparable<AStarNode> {
    private int x;
    private int y;

    private int g;
    private int h;
    private AStarNode parent;

    public AStarNode(int x, int y, int g, int h, AStarNode parent){
        this.x = x;
        this.y = y;
        this.g = g;
        this.h = h;
        this.parent = parent;
    }

    public int getX() {
        return x;
    }

    public int getY() {
        return y;
    }

    public int getG() {
        return g;
    }

    public int getH() {
        return h;
    }

    public int getF() {
        return g + h;
    }

    public AStarNode getParent() {
        return parent;
    }

    @Override
    public int compareTo(AStarNode o) {
        if( Integer.compare(y, o.y) != 0 )
            return Integer.compare(y, o.y);

        return Integer.compare(x, o.x);
    }

    // Note: This essentially turns the AStarNode into a value object
    // based on its x and y, and any two AStarNode's with the same
    // x and y will be considered equal. Though this is not quite
    // correct, it is useful for our purposes
    @Override
    public boolean equals(Object obj) {
        return compareTo((AStarNode) obj) == 0;
    }

    @Override
    public String toString(){
        return String.format("[%d, %d] h: %d", x, y, h);
    }
}
