package org.alanflaherty.CT255.Assignment10;

import java.awt.Graphics;
import java.awt.Image;

import java.util.*;

public class BadGuy {
	
	Image myImage;
	int x=0,y=0;
	boolean hasPath=false;

    // next part of path
	static int pX = 0;
	static int pY =0;

    // final target
	static int tX = -1;
	static int tY = -1;

    static AStarNode path;

    LinkedList<AStarNode> pathStack;
	ArrayList<AStarNode> openList;
	ArrayList<AStarNode> closedList;

	public BadGuy( Image i ) {
		myImage=i;
		x = 30;
		y = 10;

		pathStack = new LinkedList<AStarNode>();
		openList = new ArrayList<AStarNode>();
		closedList = new ArrayList<AStarNode>();
	}
	
	public void reCalcPath(boolean map[][],int targx, int targy, boolean force) {
		// TO DO: calculate A* path to targx,targy, taking account of walls defined in map[][]

        // check if target has moved
		if(targx == tX && targy == tY && !force){
			return;
		}

		tX = targx;
		tY = targy;

        // recreate lists
        pathStack = new LinkedList<AStarNode>();

        openList = new ArrayList<AStarNode>();
        closedList = new ArrayList<AStarNode>();

        //
		AStarNode startNode = new AStarNode(
		        x,
                y,
                0,
                calculateH(x, y, targx, targy),
                null
        );

		addNode(startNode, targx, targy, map);

		while(!openList.isEmpty()){

            // Sort by 'h' value, should pass in a custom comparer
            // to keep the list 'h' value sorted
            openList.sort((o1, o2) -> Integer.compare(o1.getF(), o2.getF())); // lowest f value

			AStarNode bestNode = openList.remove(0);
			if(bestNode.getX() == targx && bestNode.getY() == targy) {

				// path = cycle trough the nodes, choose last for now
				AStarNode nextPosition = bestNode;
                pathStack.push(nextPosition);

                while(nextPosition.getParent() != null){
                    if(nextPosition.getParent().getParent() == null)
                        break;

					nextPosition = nextPosition.getParent();
                    pathStack.push(nextPosition);
				}

				hasPath = true;
                path = bestNode;
				pX = nextPosition.getX();
				pY = nextPosition.getY();

				break;
			}

			addNode(bestNode, targx, targy, map);
		}
	}

	private void addNode(AStarNode node, int targetX, int targetY, boolean map[][]){

		for (int i = -1; i <= 1; i++) {
			for (int j = -1; j <= 1; j++) {
				if( !(i == 0 && j == 0) ) {

					if( (node.getX() + i ) < 40 && (node.getY() + j) < 40
						&& (node.getX() + i ) > -1 && (node.getY() + j) > -1
							)
						 {

						if (!map[node.getX() + i][node.getY() + j]) {
                            // better to be able to check open ed closed by x and y here..

							AStarNode opened_node = new AStarNode(
                                    node.getX() + i,
                                    node.getY() + j,
                                    node.getG() + 1, // this should be updated on node distance from start...
                                    calculateH(node.getX() + i, node.getY() + j, targetX, targetY),
                                    node
                            );

                            // Using the AStarNode's Value object properties here...

							// check if node is on the closed list
							boolean isOpen = openList.contains(opened_node);
							boolean isClosed = closedList.contains(opened_node);

							if (!isOpen && !isClosed) {
								openList.add(opened_node);
							}
						}
					}
				}
			}
		}

		closedList.add(node);
	}

	// this is an as the crow flies cost heuristic
	private int calculateH(int pX, int pY, int targX, int targY){
		return Math.abs(targX - pX) + Math.abs(targY - pY);
	}
	
	public void move(boolean map[][],int targx, int targy) {
		if (hasPath) {
			int newx=x, newy=y;

			if (pX<x)
				newx--;
			else if (pX>x)
				newx++;
			if (pY<y)
				newy--;
			else if (pY>y)
				newy++;

            x=newx;
            y=newy;

			if(pY==y && pX == x){
                System.out.printf("Reached target node, %d %d\n", x, y );
                if(!pathStack.isEmpty()) {
                    AStarNode nextPosition = pathStack.pop();
                    pX = nextPosition.getX();
                    pY = nextPosition.getY();
                    System.out.printf(" new target node, %d %d\n", pX, pY);
                }
            }
		}
		else {
			// no path known, so just do a dumb 'run towards' behaviour
			int newx=x, newy=y;
			if (targx<x)
				newx--;
			else if (targx>x)
				newx++;
			if (targy<y)
				newy--;
			else if (targy>y)
				newy++;
			if (!map[newx][newy]) {
				x=newx;
				y=newy;
			}
		}
	}
	
	public void paint(Graphics g) {
		g.drawImage(myImage, x*20, y*20, null);
	}
	
}

