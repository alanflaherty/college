package org.alanflaherty.CT2109.Assignment7;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.image.BufferStrategy;

/**
 * Created by alan on 05/03/18.
 */
public class GameOfLifeApplication extends JFrame implements Runnable, KeyListener, MouseListener {
    private static int CELL_COUNT_ROWS = 40;
    private static int CELL_COUNT_COLUMNS = 40;
    private static int CELL_SIZE = 10;
    private static int TOOLBAR_HEIGHT = 33;
    private static int LEFT_MARGIN = 5;
    private static int BOTTOM_MARGIN = 4;

    protected static final Dimension WindowSize = new Dimension (CELL_SIZE * CELL_COUNT_COLUMNS, CELL_SIZE * CELL_COUNT_ROWS + TOOLBAR_HEIGHT );
    private BufferStrategy strategy;

    private boolean[][] cells;
    private boolean isGraphicsInitalised = false;

    public GameOfLifeApplication(){
        Dimension screensize = java.awt.Toolkit.getDefaultToolkit().getScreenSize();
        int x = screensize.width/2 - WindowSize.width/2;
        int y = screensize.height/2 - WindowSize.height/2;

        setBounds(x, y, WindowSize.width + LEFT_MARGIN, WindowSize.height + BOTTOM_MARGIN);
        setVisible(true);
        setResizable(false);

        this.setTitle("Game of Life");

        addKeyListener(this);
        addMouseListener(this);

        Thread t = new Thread(this);
        t.start();

        createBufferStrategy(2);
        strategy = getBufferStrategy();

        // initalise state
        cells = new boolean[40][40];   // extract cols and rows variables
        for (int i = 0; i < CELL_COUNT_COLUMNS; i++) { // TODO: unroll loop
            for (int j = 0; j < CELL_COUNT_ROWS; j++) {
                cells[i][j] = false;
            }
        }

        isGraphicsInitalised = true;
    }

    public void run() {
        while(true) {
            try {
                Thread.sleep(20);
            } catch (InterruptedException e) {
                System.out.printf("Thread sleep interrupted: ", e.getMessage());
            }

            cells[0][0] = !cells[0][0];
            cells[0][39] = !cells[0][39];
            cells[39][0] = !cells[39][0];
            cells[39][39] = !cells[39][39];

            repaint();
        }
    }

    public void paint(Graphics g){
        if(isGraphicsInitalised) {

            g = strategy.getDrawGraphics();

            g.setColor(Color.PINK);
            g.fillRect(0, 0, 800, 800);

            for (int i = 0; i < CELL_COUNT_COLUMNS; i++) { // TODO: unroll loop
                for (int j = 0; j < CELL_COUNT_ROWS; j++) {
                    if (cells[i][j]) {
                        g.setColor(Color.BLACK);
                    } else {
                        g.setColor(Color.WHITE);
                    }

                    g.fillRect( (i * CELL_SIZE) + LEFT_MARGIN, (j * CELL_SIZE) + TOOLBAR_HEIGHT, CELL_SIZE, CELL_SIZE);
                }
            }

            strategy.show();
        }
    }

    // Key Events
    public void keyTyped(KeyEvent e) {}

    public void keyPressed(KeyEvent e) {
        if(e.getKeyCode() == KeyEvent.VK_S){
            resetCells();

            // TODO: START/STOP cellular Automata
        }
    }

    private void resetCells() {
        for (int i = 0; i < CELL_COUNT_COLUMNS; i++) { // TODO: unroll loop
            for (int j = 0; j < CELL_COUNT_ROWS; j++) {
                cells[i][j] = false;
            }
        }
    }

    public void keyReleased(KeyEvent e) {}


    // Mouse Events
    public void mouseExited(MouseEvent e) {}

    public void mouseEntered(MouseEvent e) {}

    public void mouseReleased(MouseEvent e) {}

    public void mousePressed(MouseEvent e) {}

    public void mouseClicked(MouseEvent e) {
        System.out.printf("MouseClicked: x: %d y: %d\n", e.getX(), e.getY());

        int cellX = Math.floorDiv(e.getX() - LEFT_MARGIN, CELL_SIZE);
        int cellY = Math.floorDiv(e.getY() - TOOLBAR_HEIGHT, CELL_SIZE);

        cells[cellX][cellY] = !cells[cellX][cellY];

        System.out.printf("Cells x: %d y: %d, result: %b \n", cellX, cellY, cells[cellX][cellY]);

        // this.repaint();
    }
}
