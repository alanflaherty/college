package org.alanflaherty.CT255.Assignment5;

import java.awt.*;
/**
 * Created by alan on 05/02/18.
 */
public abstract class Sprite2D {
    // member data
    protected double x, y;
    protected double xSpeed = 0;
    protected Image myImage;
    protected Image myImage2;

    long framesDrawn = 0;

    protected boolean draw = true;

    // constructor
    public Sprite2D(Image i, Image i_2){
        // x = Math.random() * 800;
        // y = Math.random() * 600;
        myImage = i;
        myImage2 = i_2;
    }

    public abstract boolean move();

    public void setPosition(double xx, double yy) {
        x = xx;
        y = yy;
    }

    public void setXSpeed(double dx) {
        xSpeed =  dx;
    }

    public void paint(Graphics g) {
        if(!draw)
            return;

        if(myImage2 == null) {
            g.drawImage(myImage, (int) x, (int) y, null);
        }
        else {
            framesDrawn++;
            if ( framesDrawn%100<50 )
                g.drawImage(myImage, (int)x, (int)y, null);
            else
                g.drawImage(myImage2, (int)x, (int)y, null);
        }
    }
}
