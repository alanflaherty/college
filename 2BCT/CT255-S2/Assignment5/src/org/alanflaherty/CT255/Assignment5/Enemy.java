package org.alanflaherty.CT255.Assignment5;

import java.awt.*;

/**
 * Created by alan on 05/02/18.
 */
public class Enemy extends Sprite2D {
    public boolean isAlive = true;

    // constructor
    public Enemy(Image i, Image i2){
        super(i, i2);
    }

    private int direction = -1;
    public void moveDownSwitchDirection() {
        // y += myImage.getHeightr(null);
        y += 65;
        direction *= -1;
        x += (2 * direction);
    }

    @Override
    public boolean move()
    {
        x += (1 * direction);
        boolean ret = false;

        if(x < 1) {
            ret = true;
        }
        else if (x > (800 - (65 + 1))){
            ret = true;
        }

        return ret;
    }

    public boolean collide(Sprite2D sprite){
        if(!draw || !sprite.draw)
            return false;

        int w1 = this.myImage.getWidth(null);
        int h1 = this.myImage.getHeight(null);
        double x1 = this.x;
        double y1 = this.y;

        int w2 = sprite.myImage.getWidth(null);
        int h2 = sprite.myImage.getHeight(null);
        double x2 = sprite.x;
        double y2 = sprite.y;

        if (
                ( (x1<x2 && x1+w1>x2) || (x2<x1 && x2+w2>x1) ) &&
                ( (y1<y2 && y1+h1>y2) || (y2<y1 && y2+h2>y1) )
        )
        {
            draw = false;

            return true;
        }

        return false;
    }
}
