package org.alanflaherty.CT255.Assignment5;

import java.awt.*;

/**
 * Created by alan on 05/02/18.
 */
public class Player extends Sprite2D {

    private int windowWidth;

    // constructor
    public Player(Image i, int windowWidth) {
        super(i, null);
        this.windowWidth = windowWidth;
    }

    @Override
    public boolean move() {
        if( xSpeed  < 0 && x > 10
           || xSpeed > 0 && x < windowWidth - ( myImage.getWidth(null) + 10 ) ) {
            x += xSpeed;
        }

        return false;
    }
}
