package org.alanflaherty.CT255.Assignment5;

import java.awt.*;

/**
 * Created by alan on 19/02/18.
 */
public class PlayerBullet extends Sprite2D{
    private int dy = 2;
    private int windowWidth;

    public PlayerBullet(Image i, int windowWidth){
        super(i, null);
        this.windowWidth = windowWidth;

        // set position
    }

    public boolean move(){
        y -= dy;

        if(y < 50) { // -10
            return true;
        }

        return false;
    }
}
