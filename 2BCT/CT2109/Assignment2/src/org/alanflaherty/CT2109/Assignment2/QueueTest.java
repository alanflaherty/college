/*
 * M Madden, Nov 2005: Test the ArrayQueue
 */
package org.alanflaherty.CT2109.Assignment2;

import javax.swing.JOptionPane;

public class QueueTest {
	public static void main(String[] args) {
        testOne();
	}

	private static void testOne(){
        System.out.printf("Enqueue 10 items\n");
        Queue q = new LinkedListQueue();
        q.enqueue("This");
        q.enqueue("is");
        q.enqueue("a");
        q.enqueue("test");
        q.enqueue("of");
        q.enqueue("the");
        q.enqueue("capacity");
        q.enqueue("of");
        q.enqueue("the");
        q.enqueue("ArrayQueueCircular.");

        assert(q.isFull() == true);
        System.out.printf("1. IsFull %b\n", q.isFull() );
        System.out.printf("2. IsEmpty %b\n", q.isEmpty() );

        System.out.printf("dequeue 2 items\n", q.isFull() );
        System.out.printf("%s\n", q.dequeue());
        System.out.printf("%s\n", q.dequeue());

        System.out.printf("Enqueue 2 items\n");
        q.enqueue("One");
        q.enqueue("Two");

        System.out.printf("dequeue all remaining items\n");
        System.out.printf("%s\n", q.dequeue());
        System.out.printf("%s\n", q.dequeue());
        System.out.printf("%s\n", q.dequeue());
        System.out.printf("%s\n", q.dequeue());
        System.out.printf("%s\n", q.dequeue());
        System.out.printf("%s\n", q.dequeue());
        System.out.printf("%s\n", q.dequeue());
        System.out.printf("%s\n", q.dequeue());
        System.out.printf("%s\n", q.dequeue());
        System.out.printf("%s\n", q.dequeue());

        // This one should be null
        System.out.printf("%s\n", q.dequeue());

        System.out.printf("1. IsFull:%b\n", q.isFull() );
        System.out.printf("2. IsEmpty:%b\n", q.isEmpty() );

        // fill queue
        System.out.printf("Fill Queue\n");
        q.enqueue("One");
        q.enqueue("Two");
        q.enqueue("Three");
        q.enqueue("Four");
        q.enqueue("Five");
        q.enqueue("Six");
        q.enqueue("Seven");
        q.enqueue("Eight");
        q.enqueue("Nine");
        q.enqueue("Ten");

        System.out.printf("1. IsFull:%b\n", q.isFull() );
        System.out.printf("2. IsEmpty:%b\n", q.isEmpty() );

        q.enqueue("Eleven");

        System.out.printf("1. IsFull:%b\n", q.isFull() );
        System.out.printf("2. IsEmpty:%b\n", q.isEmpty() );

        System.out.printf("dequeue first item, should be one\n");
        System.out.printf("%s\n", q.dequeue());

        System.out.printf("1. IsFull:%b\n", q.isFull() );
        System.out.printf("2. IsEmpty:%b\n", q.isEmpty() );

        System.out.printf("Front should be 'Two'.\n");
        System.out.printf("%s\n", q.front());
    }

    private static void originalTest(){
        // Create a Queue
        Queue q = new ArrayQueueCircular();

        // Put some strings onto the queue
        JOptionPane.showMessageDialog(null, "About to enqueue words onto queue: \nThe end is nigh!");
        q.enqueue("The");
        q.enqueue("end");
        q.enqueue("is");
        q.enqueue("nigh!");

        // Now dequeue them from the queue
        JOptionPane.showMessageDialog(null, "About to dequeue the words ...");
        while(! q.isEmpty()) {
            String word = (String)q.dequeue(); // Note: have to cast Objects popped to String
            JOptionPane.showMessageDialog(null, "Word dequeued: " + word);
        }

        System.exit(0);
    }
}
