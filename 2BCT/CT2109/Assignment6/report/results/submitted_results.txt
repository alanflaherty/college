
Array Size: 100 Number of Iterations: 10000 
sort type			       time      cmp   swap 
quick				  131633.95μs   1072    159 
quick sorted		   43882.76μs   5247     99 
quick rsorted		   46939.72μs   5247     99 
quick2 pivot		  102956.01μs   1052    207 
quick2 sorted		   20338.68μs   1130    102 
quick2 rsorted		   10848.73μs   1277    151 

Array Size: 200 Number of Iterations: 10000 
sort type			       time      cmp   swap 
quick				   29734.18μs   2592    379 
quick sorted		   83424.30μs  20497    199 
quick rsorted		  105005.55μs  20497    199 
quick2 pivot		   45939.42μs   2511    480 
quick2 sorted		   27962.40μs   3925    202 
quick2 rsorted		   31191.89μs   4224    303 

Array Size: 300 Number of Iterations: 10000 
sort type			       time      cmp   swap 
quick				   35398.18μs   4796    563 
quick sorted		  182485.92μs  45747    299 
quick rsorted		  161405.14μs  45747    299 
quick2 pivot		   38605.53μs   3880    742 
quick2 sorted		   44786.64μs   8397    306 
quick2 rsorted		   49613.18μs   8849    455 

Array Size: 400 Number of Iterations: 10000 
sort type			       time      cmp   swap 
quick				   47107.00μs   6257    822 
quick sorted		  328340.57μs  80997    399 
quick rsorted		  278982.52μs  80997    399 
quick2 pivot		   56926.13μs   5808   1023 
quick2 sorted		   72356.26μs  14528    402 
quick2 rsorted		   77996.22μs  15104    605 

Array Size: 500 Number of Iterations: 10000 
sort type			       time      cmp   swap 
quick				   64899.09μs   7765   1093 
quick sorted		  481397.74μs 126247    499 
quick rsorted		  423223.53μs 126247    499 
quick2 pivot		   74928.01μs   7176   1313 
quick2 sorted		  104845.29μs  22297    504 
quick2 rsorted		  111977.39μs  23085    751 

Array Size: 600 Number of Iterations: 10000 
sort type			       time      cmp   swap 
quick				   84211.55μs   9849   1365 
quick sorted		  688419.68μs 181497    599 
quick rsorted		  600383.74μs 181497    599 
quick2 pivot		  138434.13μs   8986   1606 
quick2 sorted		  156936.08μs  31696    608 
quick2 rsorted		  153182.81μs  32689    903 

Array Size: 700 Number of Iterations: 10000 
sort type			       time      cmp   swap 
quick				  107506.05μs  11767   1585 
quick sorted		  946225.24μs 246747    699 
quick rsorted		  810363.08μs 246747    699 
quick2 pivot		  122978.39μs  10956   1918 
quick2 sorted		  187199.38μs  42806    708 
quick2 rsorted		  199203.08μs  43968   1057 

Array Size: 800 Number of Iterations: 10000 
sort type			       time      cmp   swap 
quick				  135024.09μs  13717   1892 
quick sorted		 1325548.53μs 321997    799 
quick rsorted		 1076289.30μs 321997    799 
quick2 pivot		  148469.17μs  13023   2188 
quick2 sorted		  238238.12μs  55618    808 
quick2 rsorted		  253252.88μs  56750   1205 

Array Size: 900 Number of Iterations: 10000 
sort type			       time      cmp   swap 
quick				  154288.59μs  15811   2137 
quick sorted		 1518907.82μs 407247    899 
quick rsorted		 1310682.48μs 407247    899 
quick2 pivot		  179253.78μs  15326   2538 
quick2 sorted		  296668.87μs  70181    904 
quick2 rsorted		  311059.07μs  71324   1353 

Array Size: 1000 Number of Iterations: 10000 
sort type			       time      cmp   swap 
quick				  190860.51μs  18772   2395 
quick sorted		 1874920.48μs 502497    999 
quick rsorted		 1606835.11μs 502497    999 
quick2 pivot		  207748.84μs  16955   2848 
quick2 sorted		  360732.98μs  86197   1004 
quick2 rsorted		  380918.48μs  87392   1505 

Process finished with exit code 0

