package org.alanflaherty.CT2109.Assignment6;

import java.util.Comparator;

/**
 * Created by alan on 15/03/18.
 */
public class Sort {
    /** Task: Sorts equally spaced elements of an array into ascending order.
     *  The paramater arr is an array of Comparable objects.
     */

    public static int move = 0;
    public static int compare = 0;

    /** QuickSort method:
     * Sorts the elements of array arr in nondecreasing order according
     * to comparator c, using the quick-sort algorithm. Most of the work
     * is done by the auxiliary recursive method quickSortStep.
     **/
    public static void quickSort (Object[] arr, Comparator c) {
        compare = 0;
        move = 0;

        if (arr.length < 2) return; // the array is already sorted in this case
        quickSortStep(arr, c, 0, arr.length-1); // call the recursive sort method
    }

    /** QuickSortStep method:
     * Method called by QuickSort(), which sorts the elements of array s between
     * indices leftBound and rightBound, using a recursive, in-place,
     * implementation of the quick-sort algorithm.
     **/
    private static void quickSortStep (Object[] s, Comparator c, int leftBound, int rightBound ) {
        if (leftBound >= rightBound) return; // the indices have crossed

        Object temp;  // temp object used for swapping
        // Set the pivot to be the last element
        Object pivotValue = s[rightBound];

        // Now partition the array
        int upIndex = leftBound;     // will scan rightward, 'up' the array
        int downIndex = rightBound-1; // will scan leftward, 'down' the array

        while (upIndex <= downIndex) {
            compare++;  //# (c.compare(s[upIndex], pivotValue)<=0)

            // scan right until larger than the pivot
            while ( (upIndex <= downIndex) && (c.compare(s[upIndex], pivotValue)<=0) ) {
                upIndex++;

                compare++;  //# (c.compare(s[upIndex], pivotValue)<=0)
            }

            compare++;  //# (c.compare(s[upIndex], pivotValue)<=0)
            // scan leftward to find an element smaller than the pivot
            while ( (downIndex >= upIndex) && (c.compare(s[downIndex], pivotValue)>=0)) {
                downIndex--;

                compare++;  //# (c.compare(s[upIndex], pivotValue)<=0)
            }

            if (upIndex < downIndex) { // both elements were found
                temp = s[downIndex];
                s[downIndex] = s[upIndex]; // swap these elements
                s[upIndex] = temp;

                move++;     //# this was all one swap..
            }

            // while
            compare++;  //# while upIndex <= downIndex
        } // the loop continues until the indices cross

        int pivotIndex = upIndex;
        temp = s[rightBound]; // swap pivot with the element at upIndex
        s[rightBound] = s[pivotIndex];
        s[pivotIndex] = temp;

        move++;     //# this was all one swap

        // the pivot is now at upIndex, so recursively quicksort each side
        quickSortStep(s, c, leftBound, pivotIndex-1);
        quickSortStep(s, c, pivotIndex+1, rightBound);
    }


    // UPDATED QUICK SORT
    /*
    updated quicksort algorithm choosing pivot!

    Can approximate it:
    mid value of Array[Left],[Middle],[Right])
     */
    /** QuickSort method:
     * Sorts the elements of array arr in nondecreasing order according
     * to comparator c, using the quick-sort algorithm. Most of the work
     * is done by the auxiliary recursive method quickSortStep.
     **/
    public static void quickSort2 (Object[] arr, Comparator c) {
        compare = 0;
        move = 0;

        if (arr.length < 2) return; // the array is already sorted in this case
        quickSortStep2(arr, c, 0, arr.length-1); // call the recursive sort method
    }

    /** QuickSortStep method:
     * Method called by QuickSort(), which sorts the elements of array s between
     * indices leftBound and rightBound, using a recursive, in-place,
     * implementation of the quick-sort algorithm.
     **/
    private static void quickSortStep2 (Object[] s, Comparator c, int leftBound, int rightBound ) {
        if (leftBound >= rightBound) return; // the indices have crossed

        // Bad error here somewhere... Choosing a non optimal random pivot
        // consistently works far better for worst case scenarios but
        // knocks out the average case completely


        // this is  where the error was, calculating mid on the entire array rather than just the portion
        // we were working with
        int mid;
        int diff = rightBound - leftBound;
        if(diff % 2 == 0) // exclude last item, calculate mid
        {
            mid = leftBound + (diff / 2) - 1;
        }
        else {
            mid = leftBound + ((diff - 1) / 2);
        }

        Object temp;  // temp object used for swapping
        int pivotInd = middleIndex3(c, s, leftBound, mid, rightBound);
        if(pivotInd != rightBound){
            temp = s[pivotInd];
            s[rightBound] = s[pivotInd]; // swap these elements
            s[pivotInd] = temp;

            move++;
        }

        // Set the pivot to be the last element
        Object pivotValue = s[rightBound];

        // Now partition the array
        int upIndex = leftBound;     // will scan rightward, 'up' the array
        int downIndex = rightBound-1; // will scan leftward, 'down' the array

        while (upIndex <= downIndex) {
            compare++;  //# (c.compare(s[upIndex], pivotValue)<=0)

            // scan right until larger than the pivot
            while ( (upIndex <= downIndex) && (c.compare(s[upIndex], pivotValue)<=0) ) {
                upIndex++;

                compare++;  //# (c.compare(s[upIndex], pivotValue)<=0)
            }

            compare++;  //# (c.compare(s[upIndex], pivotValue)<=0)
            // scan leftward to find an element smaller than the pivot
            while ( (downIndex >= upIndex) && (c.compare(s[downIndex], pivotValue)>=0)) {
                downIndex--;

                compare++;  //# (c.compare(s[upIndex], pivotValue)<=0)
            }

            if (upIndex < downIndex) { // both elements were found
                temp = s[downIndex];
                s[downIndex] = s[upIndex]; // swap these elements
                s[upIndex] = temp;

                move++;     //# this was all one swap..
            }

            // while
            compare++;  //# while upIndex <= downIndex
        } // the loop continues until the indices cross

        int pivotIndex = upIndex;
        temp = s[rightBound]; // swap pivot with the element at upIndex
        s[rightBound] = s[pivotIndex];
        s[pivotIndex] = temp;

        move++;     //# this was all one swap

        // the pivot is now at upIndex, so recursively quicksort each side
        quickSortStep2(s, c, leftBound, pivotIndex-1);
        quickSortStep2(s, c, pivotIndex+1, rightBound);
    }

    private static int middleIndex3(Comparator c, Object[] s, int left, int mid, int right) {
        if( c.compare(s[left], s[right]) == 0 )
            return left;

        if( c.compare(s[left], s[mid]) == 0 || c.compare(s[right], s[mid]) == 0)
            return mid;

        if( (c.compare(s[mid], s[left]) == 1 && c.compare(s[right], s[mid]) == 1)
                 || (c.compare(s[mid], s[right]) == 1 && c.compare(s[left], s[mid]) == 1)   )
            return mid;

// M  L  R
// R  L  M
        if( (c.compare(s[left], s[mid]) == 1 && c.compare(s[right], s[left]) == 1)
                || (c.compare(s[left], s[right]) == 1 && c.compare(s[mid], s[left]) == 1)   )
            return left;

// M R L
// L R M
        if( (c.compare(s[right], s[mid]) == 1 && c.compare(s[left], s[right]) == 1)
                || (c.compare(s[right], s[left]) == 1 && c.compare(s[mid], s[right]) == 1)   )
            return right;

        String params = String.format("left: %d, mid: %d, right: %d", left, mid, right);
        throw new IllegalArgumentException("Code should have returned before this: " + params);
    }

    private static int middleIndex2(Comparator c, Object[] s, int left, int mid, int right) {

        Object b1 = c.compare(s[right], s[left]) == 1 ? s[right] : s[left];
        Object biggest = c.compare(b1, s[mid]) == 1 ? b1 : s[mid];

        Object s1 = c.compare(s[right], s[left]) == -1 ? s[right] : s[left];
        Object smallest = c.compare(s1, s[mid]) == -1 ? s1 : s[mid];

        // TODO REWORK THIS if any two are the same it would return the wrong value......
        //return tte one thats not biggest or smallest
        if(!(c.compare(biggest, s[left]) == 0 || c.compare(smallest, s[left])==0)){
            return left;
        }
        if(!(c.compare(biggest, s[mid]) == 0 || c.compare(smallest, s[mid])==0)){
            return mid;
        }

        return right;
//        if(!(c.compare(biggest, s[right]) == 0 || c.compare(smallest, s[right])==0)){
//            return left;
//        }
    }

    // bubble sort, here...
    private static int middleIndex(Comparator c, Object[] s, int left, int mid, int right) {
        Object[] items = new Object[]{s[left], s[mid], s[right]};
        int[] indexes = new int[]{left, mid, right};

        while(true){
            boolean cleanPass = true;
            for (int i = 0; i < 2 ; i++) {
                if( c.compare(items[i], items[i+1]) == 1 ){
                    Object temp = items[i];
                    items[i + 1] = items[i];
                    items[i] = temp;

                    int tempi = indexes[i];
                    indexes[i + 1] = indexes[i];
                    indexes[i] = tempi;

                    cleanPass = false;
                }
            }

            if(cleanPass){
                break;
            }
        }

        return indexes[1];
    }
}
