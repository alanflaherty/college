package org.alanflaherty.CT2109.Assignment6;

import java.io.File;
import java.util.Arrays;
import java.util.ResourceBundle;

public class Main {
    private static int runCount = 100000;
    private static int maxIntValue = 65535;
    private static Integer[] sourceArray = new Integer[]{};

    private static int partitionSize = 100;
    private static int partitionEnd = 1000;

    public static void main(String[] args) {

        System.out.print("\033[H\033[2J");
        System.out.flush();

        System.out.println("\n\nCT2109 - Assignment 6: Pivot Analysis.\n");

        initaliseProperties();

        for (int arrayLength = partitionSize; arrayLength <= partitionEnd ; arrayLength += partitionSize) {
            System.out.printf("\nArray Size: %d Number of Iterations: %d \n", arrayLength, runCount);
            System.out.printf("sort type\t\t\t %10s   %6s %6s \n", "time", "cmp", "swap");

            Integer[] ints = Arrays.copyOfRange(sourceArray, 0, arrayLength);

            Integer[] sorted_ints = ints.clone();
            Arrays.sort(sorted_ints, (o1, o2) -> o1.compareTo(o2));

            Integer[] reverse_sorted_ints = ints.clone();
            Arrays.sort(reverse_sorted_ints, (o1, o2) -> o2.compareTo(o1));

            // quick sort
            Integer[] quickSortItems  = null;
            long start = System.nanoTime();
            for (int i = 0; i < runCount; i++) {
                quickSortItems = ints.clone();
                Sort.quickSort(quickSortItems, new MyIntegerComparator());
            }
            double total = (System.nanoTime() - start) / (double) runCount;
            System.out.printf("quick\t\t\t\t %10.2fμs %6d %6d \n", total, Sort.compare, Sort.move);

            // quick sort sorted
            Integer[] sortedQuickSortItems  = null;
            start = System.nanoTime();
            for (int i = 0; i < runCount; i++) {
                sortedQuickSortItems = sorted_ints.clone();
                Sort.quickSort(sortedQuickSortItems, new MyIntegerComparator());
            }
            total = (System.nanoTime() - start) / (double) runCount;
            System.out.printf("quick sorted\t\t %10.2fμs %6d %6d \n", total, Sort.compare, Sort.move);

            // quick sort reverse sorted
            Integer[] reverseSortedQuickSortItems = null;
            start = System.nanoTime();
            for (int i = 0; i < runCount; i++) {
                reverseSortedQuickSortItems = reverse_sorted_ints.clone();
                Sort.quickSort(reverseSortedQuickSortItems, new MyIntegerComparator());
            }
            total = (System.nanoTime() - start) / (double) runCount;
            System.out.printf("quick rsorted\t\t %10.2fμs %6d %6d \n", total, Sort.compare, Sort.move);

            // METHOD 2
            // ####################################
            // quick sort with mid pivot
            Integer[] quickSort2Items  = null;
            start = System.nanoTime();
            for (int i = 0; i < runCount; i++) {
                quickSort2Items = ints.clone();
                Sort.quickSort2(quickSort2Items, new MyIntegerComparator());
            }
            total = (System.nanoTime() - start) / (double) runCount;
            System.out.printf("quick2 pivot\t\t %10.2fμs %6d %6d \n", total, Sort.compare, Sort.move);

            // quick sort sorted
            Integer[] sortedQuickSort2Items = null;
            start = System.nanoTime();
            for (int i = 0; i < runCount; i++) {
                sortedQuickSort2Items = sorted_ints.clone();
                Sort.quickSort2(sortedQuickSort2Items, new MyIntegerComparator());
            }
            total = (System.nanoTime() - start) / (double) runCount;
            System.out.printf("quick2 sorted\t\t %10.2fμs %6d %6d \n", total, Sort.compare, Sort.move);

            // quick sort reverse sorted
            Integer[] reverseSortedQuickSort2Items = null;
            start = System.nanoTime();
            for (int i = 0; i < runCount; i++) {
                reverseSortedQuickSort2Items = reverse_sorted_ints.clone();
                Sort.quickSort2(reverseSortedQuickSort2Items, new MyIntegerComparator());
            }
            total = (System.nanoTime() - start) / (double) runCount;
            System.out.printf("quick2 rsorted\t\t %10.2fμs %6d %6d \n", total, Sort.compare, Sort.move);
        }
    }

    private static void initaliseProperties(){
        ResourceBundle b = ResourceBundle.getBundle("Assignment6", new XMLResourceBundleControl());

        sourceArray = StringArrToIntArr(b.getString("ints_to_sort").split(", "));
        runCount = Integer.parseInt(b.getString("run_count"));
        maxIntValue = Integer.parseInt(b.getString("max_int_value"));

        if ( b.containsKey("partition_size") ){
            partitionSize = Integer.parseInt(b.getString("partition_size"));
        }
        if ( b.containsKey("partition_end") ){
            partitionEnd = Integer.parseInt(b.getString("partition_end"));
        }

        // set up the automated output directory
        File jarPath = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        String propertiesPath = jarPath.getParentFile().getAbsolutePath();
    }

    public static Integer[] StringArrToIntArr(String[] s) {
        Integer[] result = new Integer[s.length];
        for (int i = 0; i < s.length; i++) {
            result[i] = Integer.parseInt(s[i]);
        }

        return result;
    }
}