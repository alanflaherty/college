package org.alanflaherty.CT2109.Assignment6;

// original : https://gist.github.com/asicfr/1b76ea60029264d7be15d019a866e1a4
import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.Collections;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;

/**
 * Xml resource bundle control
 * @author slabbe
 */
public class XMLResourceBundleControl extends ResourceBundle.Control {

    private static final String XML = "xml";
    private static final List<String> SINGLETON_LIST = Collections.singletonList(XML);

    @Override
    public ResourceBundle newBundle(String baseName, Locale locale, String format, ClassLoader loader, boolean reload)
            throws IllegalAccessException, InstantiationException, IOException {

        if ((baseName == null) || (locale == null) || (format == null) || (loader == null)) {
            throw new IllegalArgumentException("baseName, locale, format and loader cannot be null");
        }
        if (!format.equals(XML)) {
            throw new IllegalArgumentException("format must be xml");
        }

        final String bundleName = toBundleName(baseName, locale);
        final String resourceName = toResourceName(bundleName, format);
        URL url = loader.getResource(resourceName);

        System.out.printf("[newBundle] \n\t baseName: %s \n\t bundleName: %s \n\t url: %s\n", baseName, bundleName, url);

        File jarPath = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        String propertiesPath = jarPath.getParentFile().getAbsolutePath();

        System.out.printf("propertiesPath: %s \n", propertiesPath);

        File overrideXmlResource = new File(propertiesPath+"/"+bundleName+".xml");

        System.out.printf("override url: %s \n", overrideXmlResource.getAbsolutePath());

        if(overrideXmlResource.exists())
            url = new URL("file:"+overrideXmlResource.getAbsolutePath());


        System.out.printf("url: %s \n", url);

        if (url == null) {
            return null;
        }

        final URLConnection urlconnection = url.openConnection();
        if (urlconnection == null) {
            return null;
        }

        if (reload) {
            urlconnection.setUseCaches(false);        }

        try ( 	final InputStream stream = urlconnection.getInputStream();
                 final BufferedInputStream bis = new BufferedInputStream(stream);
        ) {
            final ResourceBundle bundle = new XMLResourceBundle(bis);
            return bundle;
        }
    }

    @Override
    public List<String> getFormats(String baseName) {
        return SINGLETON_LIST;
    }
}
