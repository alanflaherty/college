package org.alanflaherty.CT2109.Assignment7;

/** CT2109 NUI Galway:
 * Class to demonstrate the use of BinaryTree code.
 * Based on code by Carrano & Savitch.
 * @author Michael Madden.
 */

public class BinaryTreeDemo
{
    public static void main(String[] args)
    {
        // Create a tree
        System.out.println("Constructing a test tree ...");
        BinaryTree<String> testTree = new BinaryTree<String>();
        createTreeOfHeight4(testTree);

        // Display some statistics about it
        System.out.println("\nSome statistics about the test tree ...");
        displayStats(testTree);

        // Perform in-order traversal
        System.out.println("\nIn-order traversal of the test tree, printing each node when visiting it ...");
        testTree.inorderTraverse();

        // Perform post-order traversal
        System.out.println("\nPost-order traversal of the test tree, printing each node when visiting it ...");
        testTree.postorderTraverse();

        // Perform post-order traversal
        System.out.println("\nPre-order traversal of the test tree, printing each node when visiting it ...");
        testTree.preorderTraverse();

        // Perform post-order traversal
        System.out.println("\nBreadth first traversal of the test tree, printing each node when visiting it ...");
        testTree.breadthfirstTraverse();

    } // end of main


    public static void createTree1(BinaryTree<String> tree)
    {
        // To create a tree, build it up from the bottom:
        // create subtree for each leaf, then create subtrees linking them,
        // until we reach the root.

        System.out.println("\nCreating a treee that looks like this:\n");
        System.out.println("     A      ");
        System.out.println("   /   \\   "); // '\\' is the escape character for backslash
        System.out.println("  B     C   ");
        System.out.println(" / \\   / \\");
        System.out.println("D   E  F  G ");
        System.out.println();

        // First the leaves
        BinaryTree<String> dTree = new BinaryTree<String>();
        dTree.setTree("D");
        // neater to use the constructor the initialisation ...
        BinaryTree<String> eTree = new BinaryTree<String>("E");
        BinaryTree<String> fTree = new BinaryTree<String>("F");
        BinaryTree<String> gTree = new BinaryTree<String>("G");

        // Now the subtrees joining leaves:
        BinaryTree<String> bTree = new BinaryTree<String>("B", dTree, eTree);
        BinaryTree<String> cTree = new BinaryTree<String>("C", fTree, gTree);

        // Now the root
        tree.setTree("A", bTree, cTree);
    } // end createTree1

    public static void createTreeOfHeight4(BinaryTree<String> tree)
    {
        System.out.println("\nCreating a tree of height 4 that looks like this:\n");
        System.out.println("          A");
        System.out.println("        /   \\");
        System.out.println("      B       C");
        System.out.println("    /   \\      \\");
        System.out.println("  D      E      F");
        System.out.println(" /      / \\    / \\");
        System.out.println("G      H   I  J   K");
        System.out.println();

        // First the leaves
        BinaryTree<String> gTree = new BinaryTree<String>("G");
        BinaryTree<String> hTree = new BinaryTree<String>("H");
        BinaryTree<String> iTree = new BinaryTree<String>("I");
        BinaryTree<String> jTree = new BinaryTree<String>("J");
        BinaryTree<String> kTree = new BinaryTree<String>("K");

        // Now the subtrees joining leaves:
        BinaryTree<String> dTree = new BinaryTree<String>("D", gTree, null);
        BinaryTree<String> eTree = new BinaryTree<String>("E", hTree, iTree);
        BinaryTree<String> fTree = new BinaryTree<String>("F", jTree, kTree);

        // level 1
        BinaryTree<String> bTree = new BinaryTree<String>("B", dTree, eTree);
        BinaryTree<String> cTree = new BinaryTree<String>("C", null, fTree);

        // Now the root
        tree.setTree("A", bTree, cTree);
    }

    public static void displayStats(BinaryTree<String> tree)
    {
        if (tree.isEmpty())
            System.out.println("The tree is empty");
        else
            System.out.println("The tree is not empty");

        System.out.println("Root of tree is " + tree.getRootData());
        System.out.println("Height of tree is " + tree.getHeight());
        System.out.println("No. of nodes in tree is " + tree.getNumberOfNodes());
    } // end displayStats
}
