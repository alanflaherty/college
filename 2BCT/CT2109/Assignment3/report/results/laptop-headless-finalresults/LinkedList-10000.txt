Queue: LinkedList Elements 10000
 [+] it:       1000	 avg enqueue:      48.48 avg dequeue:      8.08
 [+] it:       2000	 avg enqueue:      54.24 avg dequeue:      9.06
 [+] it:       3000	 avg enqueue:      55.23 avg dequeue:      9.29
 [+] it:       4000	 avg enqueue:      59.65 avg dequeue:     10.12
 [+] it:       5000	 avg enqueue:      49.07 avg dequeue:      8.14
 [+] it:       6000	 avg enqueue:      49.66 avg dequeue:      8.21
 [+] it:       7000	 avg enqueue:      49.70 avg dequeue:      8.30
 [+] it:       8000	 avg enqueue:      48.93 avg dequeue:      8.16
 [+] it:       9000	 avg enqueue:      49.63 avg dequeue:      8.25
 [+] it:      10000	 avg enqueue:      49.61 avg dequeue:      8.28
