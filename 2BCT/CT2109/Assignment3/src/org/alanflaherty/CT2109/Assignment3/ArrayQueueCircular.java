package org.alanflaherty.CT2109.Assignment3;

import javax.swing.JOptionPane;

/**
 * A Flaherty: Circular Buffer implementation of Queue ADT
 */

public class ArrayQueueCircular implements Queue
{
    protected int head = -1;			// index for the head of the queue
    protected int tail = -1;			// index for the tail of the queue

	 protected Object Q[];				// array used to implement the queue
	 protected int capacity; 			// The actual capacity of the queue array
	 public static final int CAPACITY = 1000;	// default array capacity
	   
	 public ArrayQueueCircular() {
		   // default constructor: creates queue with default capacity
		   this(CAPACITY);
	 }

	 public ArrayQueueCircular(int cap) {
		  // this constructor allows you to specify capacity
		  capacity = (cap > 0) ? cap : CAPACITY;
		  Q = new Object[capacity]; 
	 }
	 
	 public void enqueue(Object n) {
		 if (isFull()) {
			 JOptionPane.showMessageDialog(null, "Cannot enqueue object; queue is full.");
			 return;
		 }

         head++;
		 Q[head % capacity] = n;

         if(tail == -1)
         {
             tail = 0;
         }
	 }
	 
	 public Object dequeue() {
		 // Can't do anything if it's empty
		 if (isEmpty())
			 return null;
		 
		 Object toReturn = Q[tail%10];
         tail++;
		 
		 return toReturn;
	 }
	 
	 public boolean isEmpty() {
	     return ( (head - tail ) == -1);
	 }
	 
	 public boolean isFull() {
		 return (head - tail) == capacity - 1;
	 }
	 
	 public Object front() {
		 if (isEmpty())
			 return null;
		 
		 return Q[tail % capacity];
	 }

    @Override
    public String toString() {
        return String.format("[head:%d,tail:%d]", head, tail);
    }
}
