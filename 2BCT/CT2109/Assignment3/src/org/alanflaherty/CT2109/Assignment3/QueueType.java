package org.alanflaherty.CT2109.Assignment3;

/**
 * Created by alan on 28/02/18.
 */
public enum QueueType {
    CircularArray,
    LinkedList
}
