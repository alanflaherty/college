package org.alanflaherty.CT2109.Assignment3;

/**
 * Created by alan on 08/02/18.
 */
public class LinkedListQueue implements Queue {

    protected SLinkedList list;

    public LinkedListQueue() {
        list = new SLinkedList();
    }

    public void enqueue(Object n) {
        list.gotoTail();
        list.insertNext(n);
    }

    public Object dequeue() {
        list.gotoHead();
        Object ret = list.getCurr();
        list.deleteHead();
        return ret;
    }

    public boolean isEmpty(){
        return list.head == null;
    }

    public boolean isFull(){
        return false;
    }

    public Object front() {
        return list.head.getElement();
    }
}
