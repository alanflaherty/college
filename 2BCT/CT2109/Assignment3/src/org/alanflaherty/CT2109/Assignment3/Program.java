package org.alanflaherty.CT2109.Assignment3;

import org.knowm.xchart.BitmapEncoder;
import org.knowm.xchart.QuickChart;

import java.io.Console;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class Program {

    private static QueueType queueType = QueueType.CircularArray;
    private static Console console;

    private static int warmup = 10; // TODO: implement...

    private static int[] times = {1000, 2000, 3000, 4000, 5000, 6000, 7000, 8000, 9000, 10000};

    // automated variables
    private static int[] automatedInputElementCount = new int[]{1000, 10000, 100000, 1000000};
    private static String automatedSaveDirectory = "/home/alan/git/2BCT/CT2109/Assignment3/automatedOutput/";

    // TODO: Comment code and document,
    //       with an md file in the root of the application.
    //      settings in jar file and override with file in jar directory
    public static void main(String[] args) {

        console = System.console();
        if (console == null) {
            System.out.println("No console: exiting!\n\nPleases run from the command line.");
            System.exit(0);
        }

        initaliseProperties();

        // clear screen.
        System.out.print("\033[H\033[2J");
        System.out.flush();

        System.out.println("\n\nCT2109 - Assignment 3: Algorithm Analysis of Queue Implementations.\n");

        System.out.println("Run Automated? (y/n)");

        boolean automated = false;
        String item = console.readLine(": ");
        automated = (item.toLowerCase().charAt(0) == 'y');
        if(automated){
            System.out.println("Are you sure? (y/n)");
            item = console.readLine(": ");
            automated = (item.toLowerCase().charAt(0) == 'y');
        }

        if(automated) {
            runAutomated();
        }
        else{
            runManual();
        }
    }

    public static int[] StringArrToIntArr(String[] s) {
        int[] result = new int[s.length];
        for (int i = 0; i < s.length; i++) {
            result[i] = Integer.parseInt(s[i]);
        }
        return result;
    }

    private static void initaliseProperties(){
        ResourceBundle b = ResourceBundle.getBundle("Assignment3", new XMLResourceBundleControl());

        automatedInputElementCount = StringArrToIntArr(b.getString("elementCounts").split(", "));
        times = StringArrToIntArr(b.getString("iterations").split(", "));

        // set up the automated output directory
        File jarPath = new File(Program.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        String propertiesPath=jarPath.getParentFile().getAbsolutePath();

        File outputPath = new File(propertiesPath + "/automatedOutput");

        if(!outputPath.exists()) {
            outputPath.mkdir();
        }

        automatedSaveDirectory = outputPath.getAbsolutePath();
    }

    private static void runAutomated(){
        System.out.print("\033[H\033[2J");
        System.out.flush();

        // run tests for ArrayList
        queueType = QueueType.CircularArray;
        System.out.printf("Queue - Array Implementation Tests \n");

        for (int elementCount: automatedInputElementCount) {
            System.out.printf("[%d elements]\n", elementCount);

            // run warmup
            runTests(warmup, elementCount);

            ArrayList<Result> result = new ArrayList<Result>();
            for(int iterations : times){
                Result res = runTests(iterations, elementCount);
                result.add(res);
            }

            // todo: save output, save bitmap
            saveResult(result, elementCount);
        }

        // run tests for LinkedList
        queueType = QueueType.LinkedList;
        System.out.printf("Queue - Linked List Implementation Tests \n");

        for (int elementCount: automatedInputElementCount) {

            System.out.printf("[%d elements]\n", elementCount);

            // run warmup
            runTests(warmup, elementCount);

            ArrayList<Result> result = new ArrayList<Result>();
            for(int iterations : times){
                Result res = runTests(iterations, elementCount);
                result.add(res);
            }

            saveResult(result, elementCount);
        }
    }

    private static void saveResult(ArrayList<Result> results, int elementCount){
        String filenameTxt = String.format("%s/%s-%d.txt", automatedSaveDirectory, queueType, elementCount);
        String filenamePng = String.format("%s/%s-%d.png", automatedSaveDirectory, queueType, elementCount);

        // results messages
        List<String> lines = new ArrayList<String>();
        lines.add( String.format("Queue: %s Elements %d", queueType, elementCount) );
        for (Result result: results) {
            lines.add( String.format(" [+] it: %10d\t avg enqueue: %10.2f avg dequeue:%10.2f", result.iterations, result.averageEnqueue, result.averageDequeue) );
        }

        Path file = Paths.get(filenameTxt);
        try {
            Files.write(file, lines, Charset.forName("UTF-8"));
        }
        catch(IOException ex){
            System.out.printf(" [error] file io: '%s'\n", ex.getMessage());
        }

        // Write PNG chart
        org.knowm.xchart.XYChart chart = generateChart(results);
        try {
            BitmapEncoder.saveBitmap(chart, filenamePng, BitmapEncoder.BitmapFormat.PNG);
        }
        catch(IOException ex){
            System.out.printf(" [error] file io: '%s'\n", ex.getMessage());
        }
    }

    private static void runManual(){
        System.out.printf(" [-] What type of Queue (A)rray/(L)inkedList?");

        while(true) {
            String item = console.readLine("\n:");
            char input = item.toLowerCase().charAt(0);

            if(input == 'a' || input == 'l') {
                if(input == 'a'){
                    queueType = QueueType.CircularArray;
                }
                else{
                    queueType = QueueType.LinkedList;
                }

                break; // exit loop
            }
            else {
                System.out.printf(" [-] invalid input '%s'", item);
            }
        }

        System.out.printf(" [-] How many items should I add to the list?");
        int count;
        while(true) {
            try{
                String item = console.readLine("\n:");
                count = Integer.parseInt(item);
                break;
            } catch (NumberFormatException ex) {
                System.out.println("Not a number!, try again...");
            }
        }
        System.out.printf(" [+] Add %d items for each test\n", count);

        // todo: Add Warmup routine...

        ArrayList<Result> result = new ArrayList<Result>();
        for(int iterations : times){
            Result res = runTests(iterations, count);
            result.add(res);
        }

        saveResult(result, count);

    }

    private static org.knowm.xchart.XYChart generateChart(ArrayList<Result> result){
        // convert back to arrays for chart
        double[] xData = new double[result.size()]; // iterations
        double[] yData = new double[result.size()]; // enqueues / Integer.toString()
        double[] yData2 = new double[result.size()];// dequeues

        int i =0;
        for (Result r1 : result) {
            xData[i] = (double) r1.iterations;
            yData[i] = r1.averageEnqueue;
            yData2[i] = r1.averageDequeue;
            i++;
        }

        String chartTitle = String.format("Enqueue/Dequeue times for the %s", queueType);
        org.knowm.xchart.XYChart chart = QuickChart.getChart(chartTitle, "iterations", "ns", "ns per enqueue", xData, yData);
        chart.addSeries("ns per dequeue", xData, yData2);

        return chart;
    }

    private static Result runTests(int iterations, int elementCount){
        Queue queue;
        ArrayList<Long> enqueues = new ArrayList<Long>();
        ArrayList<Long> dequeues = new ArrayList<Long>();

        for (int i = 0; i < iterations; i++) {

/*
            // code to test Integer.toString()
            long queueStart = System.nanoTime();
            for (int j = 0; j < count; j++) {
                Integer.toString(j);
            }
            long enqueueTotal = (System.nanoTime() - queueStart);
            enqueues.add(enqueueTotal/count);

            double averageEnqueue = getAverage(enqueues);

            System.out.printf(" [+] it: %10d\t avg Integer.toString: %10.2f \n", iterations, averageEnqueue);
*/

            queue = getQueue(elementCount);

            long queueStart = System.nanoTime();
            for (int j = 0; j < elementCount; j++) {
                queue.enqueue(Integer.toString(j)); // ""
            }
            long enqueueTotal = (System.nanoTime() - queueStart);
            enqueues.add(enqueueTotal/elementCount);

            long dequeueStart = System.nanoTime();
            while(true) {
                if(queue.dequeue() == null){
                    break;
                }
            }
            long dequeueTotal = (System.nanoTime() - dequeueStart);
            dequeues.add(dequeueTotal/elementCount);

            System.out.printf(String.format("\r [t] it: %10d\t avg enqueue: %10d avg dequeue:%10d", i, enqueueTotal/elementCount, dequeueTotal/elementCount) );
        }

        double averageEnqueue = getAverage(enqueues);
        double averageDequeue = getAverage(dequeues);

        System.out.printf("\r [+] it: %10d\t avg enqueue: %10.2f avg dequeue:%10.2f \n", iterations, averageEnqueue, averageDequeue);

        return new Result(iterations, elementCount, averageEnqueue, averageDequeue);
    }

    private static Queue getQueue(int elementCount){
        if(queueType == QueueType.CircularArray) {
            return new ArrayQueueCircular(elementCount);
        }
        else{
            return new LinkedListQueue();
        }
    }

    private static double getAverage(ArrayList<Long> nums) {
        long total = 0;
        for(long num : nums){
            total += num;
        }

        double result = (double)total/(double)nums.size();
        return result;
    }
}
