package org.alanflaherty.CT2109.Assignment3;

/**
 * Created by alan on 22/02/18.
 */
public class Result {

    public int iterations;
    public int elementCount;
    public double averageEnqueue;
    public double averageDequeue;

    public Result(int iterations, int elementCount, double averageEnqueue, double averageDequeue){
        this.iterations = iterations;
        this.elementCount = elementCount;
        this.averageEnqueue = averageEnqueue;
        this.averageDequeue = averageDequeue;
    }
}
