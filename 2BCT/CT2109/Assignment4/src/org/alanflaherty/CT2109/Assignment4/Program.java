package org.alanflaherty.CT2109.Assignment4;

import java.io.Console;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

public class Program {
    private static Console console;

    public static void main(String[] args) {
        // check if we have a console if not exit
        console = System.console();
        if (console == null) {
            System.out.println("No console: exiting!\n\nPleases run from the command line.");
            System.exit(0);
        }

        console.printf("\nCT2109 Assignment 4: Palindrome Using Stacks & Queues\n\n");

        if(runTimings()) {
            return;
        }

        // ask the user to enter a string to test
        console.printf("Please enter a palindrome.\n");
        String input = console.readLine(": ");

        printResultsToScreen(input);
    }

    private static void printResultsToScreen(String input) {
        if(isPalindrome(input)){
            console.printf("\n \033[1;32m✔\033[0m '%s' is a palindrome.\n\n", input);
        }
        else{
            console.printf("\n \033[0;31m✘\033[0m '%s' is not a palindrome.\n\n", input);
        }
    }

    public static boolean isPalindrome(String input){
        Queue queue = new ArrayQueue();
        Stack stack = new ArrayStack();

        // Add the string to a stack and a queue one char at a time.
        for(int i = 0; i< input.length(); i++){
            char currentChar = input.charAt(i);
            currentChar = Character.toUpperCase(currentChar);

            queue.enqueue(currentChar);
            stack.push(currentChar);
        }

        // pop from the stack and check its the same as the char we dequeue..
        int i = 0;
        int limit = (int)Math.ceil(input.length()/2.0d) ;
        while(!stack.isEmpty() &&  i++ < limit){
            char res = (char)stack.pop();

            // check queue char is alphanumeric
            if(Character.isAlphabetic(res)) {
                char dequeueChar = (char)queue.dequeue();

                // check queue char is alphanumeric
                while(!Character.isAlphabetic(dequeueChar)) {
                    dequeueChar = (char)queue.dequeue();
                }

                // fail early
                if( res != dequeueChar){
                    return false;
                }
            }
        }

        return true;
    }

    private static boolean confirm(String message) {
        System.out.printf("%s (y/n)\n", message);
        String item = console.readLine(": ");
        return (item.toLowerCase().charAt(0) == 'y');
    }

    private static boolean runTimings(){
        boolean timings = confirm("Run timings?");

        if(!timings) {
            return false;
        }

        boolean runAutomated = confirm("Run automated timings?");
        if(runAutomated) {
            runAutomated = confirm("Are you sure?");
        }

        if(runAutomated) {
            runAutomatedTimings();
        }
        else{
            runManualTimings();
        }

        return true;
    }

    private static void runAutomatedTimings() {
/*
        String[] palindromes =
        {
            "t",
            "Ada",
            "Abba",
            "Malam",
            "Marram",
            "RaceCar",
            "RaceECar", // HAD TO MAKE UP
            "Rotavator",
            // "Kanakanak",
            "Kanakkanak", // HAD TO MAKE UP
            "Detartrated",
            "Marge let a moody baby doom a telegram.",
            "Red Roses run no risk, sir, on nurses order.",
            "Nella's simple hymn: \"I attain my help, Miss Allen.\"",
            "On a clover, if alive, erupts a vast pure evil; a fire volcano.",
            "Name no side in Eden, I’m mad! A maid I am, Adam mine; denied is one man.",
            "Dennis, Nell, Edna, Leon, Nedra, Anita, Rolf, Nora, Alice, Carol, Leo, Jane, Reed, Dena, Dale, Basil, Rae, Penny, Lana, Dave, Denny, Lena, Ida, Bernadette, Ben, Ray, Lila, Nina, Jo, Ira, Mara, Sara, Mario, Jan, Ina, Lily, Arne, Bette, Dan, Reba, Diane, Lynn, Ed, Eva, Dana, Lynne, Pearl, Isabel, Ada, Ned, Dee, Rena, Joel, Lora, Cecil, Aaron, Flora, Tina, Arden, Noel, and Ellen sinned."

        };
  */

        String[] palindromes =
        {
                "",
                "I need a string that is 55 si taht gnirts a deen I",
                "I need a string that is 50/100/200/300/400 chars ll srahc 004/003/002/001/05 si taht gnirts a deen I",
                "I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chhc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I",
                "I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/44/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I",
                "I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200//002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I",
                "I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I",
                "I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100001/05 si taht gnirts a deen I ,gnol srahc 004/003300/400 chars long, I need a string that is 50/100001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I",
                "I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 0000 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I",
                "I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol sraars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I",
                "I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnoong, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I",
                "I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen II need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100/200/300/400 chars long, I need a string that is 50/100001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I ,gnol srahc 004/003/002/001/05 si taht gnirts a deen I"
        };

        for (String palindrome :palindromes) {
            printResultsToScreen(palindrome);
            runTimings(palindrome);
        }
    }

    private static int[] iterations = {100000, 200000, 300000, 400000, 500000, 600000, 700000, 800000, 900000, 1000000};
    // private static int[] iterations = {1000000, 2000000, 3000000, 4000000, 5000000, 6000000, 7000000, 8000000, 9000000, 10000000};

    private static void runManualTimings() {
        console.printf("Please enter a palindrome to run timings on.\n");
        String input = console.readLine(": ");
        printResultsToScreen(input);

        runTimings(input);
    }

    private static Result[] runTimings(String input) {
        console.printf("Timings for validating '%s'.\n", input);

        ArrayList<Result> results = new ArrayList<Result>();
        for(int count : iterations){
            long start = System.nanoTime();
            for (int i = 0; i < count ; i++) {
                isPalindrome(input);
            }
            long total = (System.nanoTime() - start);

            double average = total/(double)count;
            results.add(new Result(average, count));

            System.out.printf(String.format(" [t] it: %10d\t avg run: %10.2fμs \n", count, average));
        }

        Result[] res = results.toArray(new Result[results.size()]);
        //
        saveLogFile(input, res);
        return res;
    }

    private static void saveLogFile(String input, Result[] results){
        String saveDirectory = getSaveDirectory();

        String filenameTxt = String.format( "%s/%d-length-palindrome.txt", saveDirectory, input.length() );

        // results messages
        List<String> lines = new ArrayList<String>();
        lines.add( String.format("input: %s", input) );
        lines.add( String.format("isPalindrome: %s", isPalindrome(input)));

        for (Result result: results) {
            lines.add( String.format(" [t] it: %10d\t avg run: %10.2fμs ", result.iterations, result.timeNanoSec ));
        }

        Path file = Paths.get(filenameTxt);
        try {
            Files.write(file, lines, Charset.forName("UTF-8"));
        }
        catch(IOException ex){
            System.out.printf(" [error] file io: '%s'\n", ex.getMessage());
        }
    }

    // has to be jar...
    public static String getSaveDirectory() {
        // set up the automated output directory
        File jarPath = new File(Program.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        String propertiesPath=jarPath.getParentFile().getAbsolutePath();

        // should throw here if its not a jar..

        File outputPath = new File(propertiesPath + "/output");

        if(!outputPath.exists()) {
            outputPath.mkdir();
        }

        return outputPath.getAbsolutePath();
    }
}
