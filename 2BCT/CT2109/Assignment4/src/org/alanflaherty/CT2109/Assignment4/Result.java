package org.alanflaherty.CT2109.Assignment4;

/**
 * Created by alan on 09/03/18.
 */
public class Result {
    public double timeNanoSec = 0;
    public int iterations = 0;

    public Result(double time, int iterations){
        this.timeNanoSec = time;
        this.iterations = iterations;
    }
}
