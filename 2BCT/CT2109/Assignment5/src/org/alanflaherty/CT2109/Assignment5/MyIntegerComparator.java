package org.alanflaherty.CT2109.Assignment5;

import java.util.Comparator;

class MyIntegerComparator implements Comparator<Integer> {
    public int compare(Integer a, Integer b) {
        return a.compareTo(b);
    }
}