package org.alanflaherty.CT2109.Assignment5;

import java.util.Comparator;
import java.util.Hashtable;

/**
 * Created by alan on 15/03/18.
 */
public class Sort {
    /** Task: Sorts equally spaced elements of an array into ascending order.
     *  The paramater arr is an array of Comparable objects.
     */

    public static int move = 0;
    public static int compare = 0;

    public static void shellSort (Comparable[] arr) {
        compare = 0;
        move = 0;

        int last = arr.length-1;
        // Begin with gap = half length of array; reduce by half each time.
        for (int gap = arr.length/2; gap > 0; gap = gap/2) {
            if (gap % 2 == 0) {
                gap++; // if gap is even, move to next largest odd number
            }

            // Apply Insertion Sort to the subarrays defined by the gap distance
            for (int first = 0; first < gap; first++) {
                insertionSort (arr, first, last, gap);
            }
        } // end for
    } // end shellSort


    /** Helper function for shellSort method, that sorts a subarray
     * that goes from the first to last index, in steps specified by gap.
     */
    private static void insertionSort(Comparable[] a, int first, int last, int gap) {
        int index;     // general index for keeping track of a position in array
        int toSort;  // stores the index of an out-of-place element when sorting.

        // NOTE: Instead of considering a full array of adjacent elements, we are considering
        // a sub-list of elements from 'first' to 'last, separated by 'gap'. All others are ignored.
        //
        // Work forward through the list, starting at 2nd element,
        // and sort each element relative to the ones before it.

        for (toSort = first+gap; toSort <= last; toSort += gap) {
            Comparable toSortElement = a[toSort];

            // Go back through the list to see how far back (if at all)
            // this element should be moved.
            // Note: we assume all elements before this are sorted relative to each other.
            boolean moveMade = false;
            index = toSort - gap;

            compare++;  //# toSortElement.compareTo(a[index]) < 0)
            while ((index >= first) && (toSortElement.compareTo(a[index]) < 0)) {
                // Shuffle elements over to the right, put firstUnsorted before them
                a[index+gap] = a[index];
                move++;     //# a[index+gap] = a[index];

                index = index - gap;
                moveMade = true;
                compare++;  //# toSortElement.compareTo(a[index]) < 0)
            }
            if (moveMade) {
                //System.out.println("Inserting " + toSortElement + " at pos " + (index+1));
                a[index+gap] = toSortElement;
                move++;     //# a[index+gap] = toSortElement;
            }
        }
    }

    /** Version of insertionSort method that uses the correct settings to sort an entire array
     *  from start to end in steps of 1.
     *
     *  This version uses the 'helper function' version, but is a direct substitute for the
     *  quickSort() method call.
     */
    public static void insertionSort(Comparable arr[]) {
        compare = 0;
        move = 0;
        insertionSort(arr, 0, arr.length-1, 1);
    }

    /** QuickSort method:
     * Sorts the elements of array arr in nondecreasing order according
     * to comparator c, using the quick-sort algorithm. Most of the work
     * is done by the auxiliary recursive method quickSortStep.
     **/
    public static void quickSort (Object[] arr, Comparator c) {
        compare = 0;
        move = 0;

        if (arr.length < 2) return; // the array is already sorted in this case
        quickSortStep(arr, c, 0, arr.length-1); // call the recursive sort method
    }

    /** QuickSortStep method:
     * Method called by QuickSort(), which sorts the elements of array s between
     * indices leftBound and rightBound, using a recursive, in-place,
     * implementation of the quick-sort algorithm.
     **/
    private static void quickSortStep (Object[] s, Comparator c, int leftBound, int rightBound ) {
        if (leftBound >= rightBound) return; // the indices have crossed

        Object temp;  // temp object used for swapping
        // Set the pivot to be the last element
        Object pivotValue = s[rightBound];

        // Now partition the array
        int upIndex = leftBound;     // will scan rightward, 'up' the array
        int downIndex = rightBound-1; // will scan leftward, 'down' the array

        while (upIndex <= downIndex) {
            compare++;  //# (c.compare(s[upIndex], pivotValue)<=0)

            // scan right until larger than the pivot
            while ( (upIndex <= downIndex) && (c.compare(s[upIndex], pivotValue)<=0) ) {
                upIndex++;

                compare++;  //# (c.compare(s[upIndex], pivotValue)<=0)
            }

            compare++;  //# (c.compare(s[upIndex], pivotValue)<=0)
            // scan leftward to find an element smaller than the pivot
            while ( (downIndex >= upIndex) && (c.compare(s[downIndex], pivotValue)>=0)) {
                downIndex--;

                compare++;  //# (c.compare(s[upIndex], pivotValue)<=0)
            }

            if (upIndex < downIndex) { // both elements were found
                temp = s[downIndex];
                s[downIndex] = s[upIndex]; // swap these elements
                s[upIndex] = temp;

                move++;     //# this was all one swap..
            }

            // while
            compare++;  //# while upIndex <= downIndex
        } // the loop continues until the indices cross

        int pivotIndex = upIndex;
        temp = s[rightBound]; // swap pivot with the element at upIndex
        s[rightBound] = s[pivotIndex];
        s[pivotIndex] = temp;

        move++;     //# this was all one swap

        // the pivot is now at upIndex, so recursively quicksort each side
        quickSortStep(s, c, leftBound, pivotIndex-1);
        quickSortStep(s, c, pivotIndex+1, rightBound);
    }

    public static void radixSort(Integer[] a, int upperLimit) {
        compare = 0;
        move = 0;

        int[] radix = new int[upperLimit];
        for (int i = 0; i < upperLimit; i++) {
            radix[i] = 0;
        }

        for (Integer item : a) {
            radix[item.hashCode()]++;
        }

        int radixCount = 0;
        int i = 0;
        while (radixCount < upperLimit) {
            while (radix[radixCount] != 0) {
                a[i++] = radixCount;
                radix[radixCount]--;
            }
            radixCount++;
        }
    }
}
