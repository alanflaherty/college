package org.alanflaherty.CT2109.Assignment5;

import org.apache.commons.cli.*;

import java.io.File;
import java.util.Arrays;
import java.util.Comparator;
import java.util.ResourceBundle;

public class Main {
    private static int runCount = 100000;
    private static int maxIntValue = 65535;
    private static Integer[] sourceArray = new Integer[]{};

    private static int partitionSize = 100;
    private static int partitionEnd = 1000;


    public static void main(String[] args) {
        CommandLine cmd = parseParameters(args);

        System.out.printf("Number if args: %d\n", cmd.getArgList().size());

        for (String argument: cmd.getArgList()) {
            System.out.printf(" arg: %s\n", argument);
        }

        // generate integers
        if (cmd.hasOption('g')){
            generateIntegersToOutput(cmd);
            System.exit(1);
            return;
        }

        System.out.print("\033[H\033[2J");
        System.out.flush();

        System.out.println("\n\nCT2109 - Assignment 5: Sorting Algorithm Analysis.\n");

        initaliseProperties();

        for (int arrayLength = partitionSize; arrayLength <= partitionEnd ; arrayLength += partitionSize) {
            System.out.printf("\nArray Size: %d Number of Iterations: %d \n", arrayLength, runCount);
            System.out.printf("sort type\t %10s   %6s %6s \n", "time", "cmp", "swap");
            Integer[] ints = Arrays.copyOfRange(sourceArray, 0, arrayLength);

            Integer[] insertionSortItems = null;
            long start = System.nanoTime();
            for (int i = 0; i < runCount; i++) {
                insertionSortItems = ints.clone();      // timings should not include copying the int[]
                Sort.insertionSort(insertionSortItems);
            }
            double total = (System.nanoTime() - start) / (double) runCount;
            System.out.printf("insertion\t %10.2fμs %6d %6d \n", total, Sort.compare, Sort.move);

            Integer[] quickSortItems  = null;
            start = System.nanoTime();
            for (int i = 0; i < runCount; i++) {
                quickSortItems = ints.clone();
                Sort.quickSort(quickSortItems, new MyIntegerComparator());
            }
            total = (System.nanoTime() - start) / (double) runCount;

            System.out.printf("quick\t\t %10.2fμs %6d %6d \n", total, Sort.compare, Sort.move);

            Integer[] shellSortItems  = null;
            start = System.nanoTime();
            for (int i = 0; i < runCount; i++) {
                shellSortItems = ints.clone();
                Sort.shellSort(shellSortItems);
            }
            total = (System.nanoTime() - start) / (double) runCount;
            System.out.printf("shell\t\t %10.2fμs %6d %6d \n", total, Sort.compare, Sort.move);

            // radix sort
            Integer[] radixSortItems = null;
            start = System.nanoTime();
            for (int i = 0; i < runCount; i++) {
                radixSortItems = ints.clone();
                Sort.radixSort(radixSortItems, maxIntValue);
            }
            total = (System.nanoTime() - start) / (double) runCount;

            System.out.printf("radix\t\t %10.2fμs %6d %6d \n", total, Sort.compare, Sort.move);
        }
    }

    private static void generateIntegersToOutput(CommandLine cmd) {
        int optNumberOfInts =  Integer.parseInt(cmd.getOptionValue('g', "1000"));
        int optMaxIntValue =  Integer.parseInt(cmd.getOptionValue('l', "1000"));

        System.out.printf("Generating %d ints; 0-%d \n",optNumberOfInts, optMaxIntValue);

        Integer[] items = getArray(optNumberOfInts, optMaxIntValue);

        // Write int[]
        writeInts("values", items);

        // Write Sorted int[]
        Sort.radixSort(items, optMaxIntValue);
        writeInts("sorted_values", items);

        // Write Reverse Sorted int[]
        Sort.quickSort(items, new Comparator<Integer>() {
            @Override
            public int compare(Integer o1, Integer  o2) {
                return (o2.compareTo(o1) );
            }
        });
        writeInts("reverse_sorted_values", items);
    }

    private static void writeInts(String variableName, Integer[] items) {
        String res  ="";
        for (int i = 0; i < items.length; i++) {
            res += String.format(", %d", items[i]);
        }

        System.out.printf("\n\nInteger[] %s = new Integer[]{%s}; \n",variableName, res.substring(2));
    }

    private static void initaliseProperties(){
        ResourceBundle b = ResourceBundle.getBundle("Assignment5", new XMLResourceBundleControl());

        sourceArray = StringArrToIntArr(b.getString("ints_to_sort").split(", "));
        runCount = Integer.parseInt(b.getString("run_count"));
        maxIntValue = Integer.parseInt(b.getString("max_int_value"));

        if ( b.containsKey("partition_size") ){
            partitionSize = Integer.parseInt(b.getString("partition_size"));
        }
        if ( b.containsKey("partition_end") ){
            partitionEnd = Integer.parseInt(b.getString("partition_end"));
        }

        // set up the automated output directory
        File jarPath = new File(Main.class.getProtectionDomain().getCodeSource().getLocation().getPath());
        String propertiesPath = jarPath.getParentFile().getAbsolutePath();
    }

    public static Integer[] StringArrToIntArr(String[] s) {
        Integer[] result = new Integer[s.length];
        for (int i = 0; i < s.length; i++) {
            result[i] = Integer.parseInt(s[i]);
        }

        return result;
    }

    private static CommandLine parseParameters(String[] args) {
        Options options = new Options();

        Option optGenerate = new Option("g", "generate-integers", true, "");
        optGenerate.setRequired(false);
        options.addOption(optGenerate);

        Option optLimit = new Option("l", "limit", true, "");
        optLimit.setRequired(false);
        options.addOption(optLimit);

        CommandLineParser parser = new DefaultParser();
        HelpFormatter formatter = new HelpFormatter();
        CommandLine cmd;

        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            System.out.println(e.getMessage());
            formatter.printHelp("Assignment5", options);

            System.exit(1);
            return null;
        }

        return cmd;
    }

    private static Integer[] getArray(int length) {
        return getArray(length,Integer.MAX_VALUE);
    }

    private static Integer[] getArray(int length, int maxValue) {
        Integer[] ints = new Integer[length];
        for (int i = 0; i < length; i++) {
            ints[i] = Integer.valueOf((int) (Math.random() * maxValue));
        }

        return ints;
    }
}
