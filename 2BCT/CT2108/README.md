# CT2108 - Networks and Data Communications
This module provides students with theoretical and practical knowledge in the area of computer networks and data communications. It provides the student with a good introduction to communication networks including design principles, existing technologies and future technologies.

