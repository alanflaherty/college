
/**
 * Write a description of class Seed here.
 *
 * @author (conor hayes)
 * @version (October 25th 2017)
 */
public class Seed extends Vegetable
{
    // instance variables - replace the example below with your own
    
    

    /**
     * Constructor for objects of class Seed
     */
    public Seed()
    {
        super();
        calories = 10;// overriding calories from Animal
    }
    
    
    @Override
    public int getCalories(){
        return calories;
    }
    
    /**
     * returns the current value for Calories
     * and then sets the calory value to zero
     * i.e. the energy has been extracted from Seed
     */
    @Override
    public int extractEnergy(){
       int cal = calories; // temp variable cal holds value of calories
       calories = 0; // now set calories to be zero
       return cal; // return value assigned to cal
    }
    
}
