
/**
 * Write a description of class Food here.
 *
 * @author (conor Hayes)
 * @version (a version number or a date)
 */
public abstract class Food
{
    // instance variables - replace the example below with your own
    int calories;

    /**
     * Constructor for objects of class Food
     */
    public Food()
    {
        // we have
        
    }

    public abstract int getCalories();
    
    public abstract int extractEnergy();
    
}
