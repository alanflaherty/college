
/**
 * Abstract class Feline - write a description of the class here
 *
 * @author (your name here)
 * @version (version number or date here)
 */
public abstract class Feline extends Animal
{
    private boolean hasFur = true;
    
    public Feline(){
    }
    
    public boolean hasFur(){
        return this.hasFur;
    }
    
    /**
     * move method overrides the move method
     * inherited from superclass Animal
     */
    @Override // good programming practice to use @Override to denote overridden methods
    public void move(int distance){
        System.out.printf("I move %d metres \n", distance);
    }
}
