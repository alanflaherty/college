
/**
 * Write a description of class Canary here.
 *
 * @author (Conor Hayes)
 * @version (October 5th 2017)
 */
public class Canary extends Bird
{
    // instance variables - replace the example below with your own
    String name; // the name of this Canary

    /**
     * Constructor for objects of class Canary
     */
    public Canary(String name)
    {
        super(); // call the constructor of the superclass Bird
        this.name = name;
        colour = "yellow"; // this overrides the value inherited from Bird
        
    }
    
    /**
     * Sing method overrides the sing method
     * inherited from superclass Bird
     */
    @Override // good programming practice to use @Override to denote overridden methods
    public void sing(){
        System.out.println("tweet tweet tweet");
    }
    
    /**
     * toString method returns a String representation of the bird
     * What superclass has Canary inherited this method from? 
     */
    @Override
    public String toString(){
        String strng ="";
        strng+= "Canary; ";
        strng+= "name: ";
        strng+= name;
        strng+= "; ";
        strng+= "colour: ";
        strng+= colour;
        strng+= "\n";
        // TOD0 Your job is to include the fields and attributes inherited 
        //from Bird and Animal in the String representation
        return strng;
    }

    
    /**
     * equals method defines how equality is defined between 
     * the instances of the Canary class
     * param Object
     * return true or false depending on whether the input object is 
     * equal to this Canary object
     */
    
    @Override
    public boolean equals(java.lang.Object object){
        //TODO : You have to define an equals method for this class
        return false; //default equals
    }
    
    /**
     * eat method :  if the Canary can eat this type of Food
     * then it extracts the Food's energy and sings
     * @param Food food - a reference to an object of type Food
     */
    
    @Override
    public boolean eat(Food food){
        if(food ==null){ // if the reference points to null
            return false; // immediately return.  Method execution goes no further
        }
        
        if(food instanceof Seed){ // is food pointing to a Seed object?
            Seed seed = (Seed) food; // cast reference to a Seed type
            energy+=seed.extractEnergy(); // extract the Seeds energy
            sing(); // sing 
            return true; // return. Method execution goes no further
        }else{
          System.out.println("I cannot eat this type of food");  
          return false;
        }
    }
}
