/**
 * Abstract Animal class.
 *
 * @author Alan Flaherty
 * @version 23/10/2017
 */
public abstract class Animal
{
    // instance variables - replace the example below with your own
    protected boolean hasSkin;
    protected boolean breathes;
    protected String colour;

    private String name;
    /**
     * Constructor for objects of class Animal
     */
    public Animal()
    {
        breathes = true; //all the subclasses of Animal inherit this property and value
        hasSkin = true; // all the subclasses of Animal inherit this property and value
        colour = "grey"; //all the subclasses of Animal inherit this property and value
    }
    
    /**
     * Constructor with name for objects of class Animal
     */
    public Animal(String name) 
    {
        this();
        
        this.name = name;
    }
    
    public String getName()
    {
        return name;
    }
    
    public void setName(String value)
    {
        this.name = value;
    }
    
    /**
     * move method
     * param int distance - the distance the Animal should move
     * All subclasses inherit this method
     */
    public abstract void move(int distance);
    
    /**
     * eat method
     * param Food food - food the Animal should eat
     * We haven't defined this yet
     * All subclasses inherit this method
     */
    public void eat(Food food)
    {
    }
    
    /**
     * getter method for colour field
     * All subclasses inherit this method
     */
    public String getColour(){
        return colour;
    }
    
     /**
     * 'getter' method for haSkin field
     * All subclasses inherit this method
     */
    public boolean hasSkin(){
        return hasSkin;
    }
    
    public boolean breathes(){
        return this.breathes;
    }
    
    @Override
    public String toString()
    {
        String message = "[" + this.getClass().toString().replace("class ", "") + "]\n";
        message += "name: " + name + "\n";
        message += "colour: " + colour + "\n";
        message += "hasSkin: " + hasSkin + "\n";
        message += "breathes: " + breathes + "\n";
        return message;
    }
    
    @Override
    public boolean equals(Object animal)
    {
        // check if same object reference 
        if(this == animal)
            return true;

        if(!(getClass() == animal.getClass()))
            return false;

        return ((Animal)animal).getName() == this.getName();
    }
}
