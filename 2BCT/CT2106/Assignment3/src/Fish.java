/**
 * Fish class.
 *
 * @author Alan Flaherty
 * @version 23/10/2017
 */
public abstract class Fish extends Animal
{
    protected boolean hasScales;
    protected boolean swims;

    public Fish( String name )
    {
        super(name);
        
        hasScales = true;
        swims = true;
    }
       
    public boolean hasScales(){
        return this.hasScales;
    }
    
    public boolean swims(){
        return this.swims;
    }
    
    @Override
    public String toString()
    {
        String message = super.toString();
        message += "hasScales: " + hasScales + "\n";
        message += "swims: " + swims + "\n";
        return message;
    }

    @Override
    public boolean equals(Object animal)
    {
        if(!super.equals(animal))
            return false;
        
        Fish fish = (Fish)animal;
        if(this.hasScales() != fish.hasScales())
            return false;

        if(this.swims() != fish.swims())
            return false;
            
        return true;
    }
}
