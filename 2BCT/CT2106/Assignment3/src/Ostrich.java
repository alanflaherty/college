/**
 * Ostrich class.
 *
 * @author Alan Flaherty
 * @version 23/10/2017
 */
public class Ostrich  extends Bird
{
    private boolean isTall = true;
    private boolean hasThinLongLegs = true;

    public Ostrich(String name)
    {
        super(name);
        this.flies = false;
    }
    
    public boolean isTall(){
        return this.isTall;
    }
    
    public boolean hasThinLongLegs(){
        return this.hasThinLongLegs;
    }
    
    @Override
    public void sing()
    {
        System.out.println("Chirp Chirp, ROAR!!!");
    }
    
    @Override
    public void move(int distance)
    {
        System.out.format("I am a bird but cannot fly..\n\tI walk %d meters\n", distance);
    }
       
    @Override
    public String toString()
    {
        String message = super.toString();
        message += "isTall: " + isTall + "\n";
        message += "hasThinLongLegs: " + hasThinLongLegs + "\n";
        return message;
    }
    
    @Override
    public boolean equals(Object other)
    {
        if(!super.equals(other))
            return false;
        
        Ostrich ostrich = (Ostrich)other;
        if(this.isTall() != ostrich.isTall())
            return false;
        
        if(this.hasThinLongLegs() != ostrich.hasThinLongLegs())
            return false;
            
        return true;
    }
}
