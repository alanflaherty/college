/**
 * Test Cases class to exercise the library.
 *
 * @author Alan Flaherty
 * @version 23/10/2017
 */
public class TestCases
{
    public void test1()
    {
        Animal[] animals = new Animal[4];
        animals[0] = new Canary("Bluey");
        animals[1] = new Ostrich("Leggy");;
        animals[2] = new Shark("Jaws");
        animals[3] = new Salmon("Pinky");
        
        for(Animal animal: animals)
        {
            System.out.println(animal);
        }
    }
    
    public void test2()
    {
        Animal[] animals = new Animal[8];
        animals[0] = new Canary("Bluey");
        animals[1] = new Ostrich("Leggy");
        animals[2] = new Shark("Jaws");
        animals[3] = new Salmon("Pinky");

        animals[4] = new Canary("Bluey");
        animals[5] = new Ostrich("Leggy");
        animals[6] = new Shark("Jaws");
        animals[7] = new Salmon("Pinky");

        // References should be equal
        assertMessage(animals[0].equals(animals[0]), "A Canary should equal the same Reference");
        assertMessage(animals[1].equals(animals[1]), "An Ostrich should equal the same Reference");
        assertMessage(animals[2].equals(animals[2]), "A Shark should equal the same Reference");
        assertMessage(animals[3].equals(animals[3]), "A Salmon should equal the same Reference");

        // Same types with the same properties should be equal
        assertMessage(animals[0].equals(animals[4]), "A Canary should equal another Canary with the same properties");
        assertMessage(animals[1].equals(animals[5]), "An Ostrich should equal another Ostrich with the same properties");
        assertMessage(animals[2].equals(animals[6]), "A Shark should equal another Shark with the same properties");
        assertMessage(animals[3].equals(animals[7]), "A Salmon should equal another Salmon with the same properties");

        // Different types should not be equal
        assertMessage(!animals[0].equals(animals[1]), "A Canary should not equal an Ostrich");
        assertMessage(!animals[1].equals(animals[2]), "An Ostrich should not equal a Shark");
        assertMessage(!animals[2].equals(animals[3]), "A Shark should not equal a Salmon");
        assertMessage(!animals[3].equals(animals[4]), "A Salmon should not equal a Canary");

        Animal canary = new Canary("Bluey");
        Animal shark = new Shark("Bluey");
        assertMessage(!canary.equals(shark), "A Canary called bluey should not equal a Shark called bluey.");

        Shark greatWhiteBluey = new Shark("Bluey");
        Shark baskingBluey = new Shark("Bluey", false);
        assertMessage(!greatWhiteBluey.equals(baskingBluey), "A Shark called bluey should not equal a non dangerous Shark called bluey.");

        System.out.println();
    }
    
    public void assertMessage(boolean condition, String message)
    {
        assert(condition);
        System.out.println((condition ? "✔ " : "✗ ") + message);
    }

    public void test3()
    {
        Animal[] animals = new Animal[4];
        animals[0] = new Canary("Bluey");
        animals[1] = new Ostrich("Leggy");
        animals[2] = new Shark("Jaws");
        animals[3] = new Salmon("Pinky");
        
        for(Animal animal: animals)
        {
            int distance = (int)Math.floor(Math.random() * 1000);
            animal.move(distance);
            
            if(animal instanceof Bird)
            {
                ((Bird)animal).sing();
            }
        }
        
        System.out.println();
    }
}
