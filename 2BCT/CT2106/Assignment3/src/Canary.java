/**
 * Canary class.
 *
 * @author Alan Flaherty
 * @version 23/10/2017
 */
public class Canary extends Bird
{
    private boolean canSing = true;
    private boolean isYellow = true;

    public Canary(String name)
    {
        super(name); // call the constructor of the superclass Bird
        colour = "yellow"; // this overrides the value inherited from Bird
    }
    
    public boolean canSing(){
        return this.canSing;
    }
    
    public boolean isYellow(){
        return this.isYellow;
    }
    
    /**
     * Sing method overrides the sing method
     * inherited from superclass Bird
     */
    @Override // good programming practice to use @Override to denote overridden methods
    public void sing()
    {
        System.out.println("tweet tweet tweet");
    }
    
    @Override
    public void move(int distance)
    {
        System.out.format("I am a Canary I  fly %d meters\n", distance);
    }
    
    @Override
    public String toString()
    {
        String message = super.toString();
        message += "isYellow: " + isYellow + "\n";
        message += "canSing: " + canSing + "\n";
        return message;
    }

    @Override
    public boolean equals(Object other)
    {
        if(!super.equals(other))
            return false;

        Canary canary = (Canary)other;
        if(this.canSing() != canary.canSing())
            return false;

        if(this.isYellow() != canary.isYellow())
            return false;

        return true;
    }
}
