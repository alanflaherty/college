
/**
 * Abstract Bird class.
 *
 * @author Alan Flaherty
 * @version 23/10/2017
 */
public abstract class Bird extends Animal
{
    //instance variables (fields)
    protected boolean hasFeathers;
    protected boolean hasWings;
    protected boolean flies;

    /**
     * Constructor for objects of class Bird
     */
    public Bird( String name )
    {
        super(name); //calls the constructor of its superclass  - Animal
        colour = "black"; //overrides the value of colour inherited from Animal
        hasFeathers = true; //all the subclasses of Bird inherit this property and value
        hasWings = true; //all the subclasses of Bird inherit this property and value
        flies = true; //all the subclasses of Bird inherit this property and value
    }

    
    /**
     * sing method that all birds have
     */
    public void sing()
    {
        System.out.println("tra la la");
    }
    
    /**
     * 'getter' method for the hasWings field
     */
    public boolean hasWings(){
        return hasWings;
    }
    
    /**
     * 'getter' method for the hasFeathers field
     */
    public boolean hasFeathers(){
        return hasFeathers;
    }
    
    public boolean flies(){
        return flies;
    }
    
    @Override 
    public String toString()
    {   
        String message = super.toString();
        message += "hasWings: " + hasWings + "\n";
        message += "hasFeathers: " + hasFeathers + "\n";
        message += "flies: " + flies + "\n";
        return message;
    }
    
    @Override
    public boolean equals(Object animal)
    {
        if(!super.equals(animal))
            return false;
        
        Bird bird = (Bird)animal;
        if(this.hasFeathers() != bird.hasFeathers())
            return false;

        if(this.hasWings() != bird.hasWings())
            return false;

        if(this.flies() != bird.flies())
            return false;
            
        return true;
    }
}
