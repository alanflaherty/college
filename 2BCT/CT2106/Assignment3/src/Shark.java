/**
 * Shark class.
 *
 * @author Alan Flaherty
 * @version 23/10/2017
 */
public class Shark extends Fish
{
    private boolean canBite = true;
    private boolean isDangerous = true;

    public Shark(String name)
    {
        super(name);
    }
    
    public Shark(String name, boolean isDangerous)
    {
        this(name);
        
        this.isDangerous = isDangerous;
    }

    public Shark(String name, boolean isDangerous, boolean canBite)
    {
        this(name, isDangerous);
        
        this.canBite = canBite;
    }

    public boolean canBite(){
        return this.canBite;
    }
    
    public boolean isDangerous(){
        return this.isDangerous;
    }

    @Override
    public void move(int distance)
    {
        System.out.format("I am a Shark I swim %d meters.\n", distance);
    }
    
    @Override
    public String toString()
    {
        String message = super.toString().replace("class ", "");
	message += "canBite: " + canBite + "\n";
        message += "isDangerous: " + isDangerous + "\n";
        return message;
    }
    
    @Override
    public boolean equals(Object other)
    {
        if(!super.equals(other))
            return false;

        Shark shark = (Shark)other;
        if(this.canBite() != shark.canBite())
            return false;

        if(this.isDangerous() != shark.isDangerous())
            return false;

        return true;
    }
}
