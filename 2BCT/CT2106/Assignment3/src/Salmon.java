/**
 * Salmon class.
 *
 * @author Alan Flaherty
 * @version 23/10/2017
 */
public class Salmon extends Fish
{
    private boolean isPink = true;
    private boolean isEdible = true;
    private boolean swimsUpriverToLayEggs = true;

    public Salmon(String name)
    {
        super(name);
        this.colour = "Blue/Grey/Pink";
    }
    
    public boolean isPink(){
         return isPink;
    }
   
    public boolean isEdible(){
         return isEdible;
    }
    
    public boolean swimsUpriverToLayEggs(){
         return swimsUpriverToLayEggs;
    }
    
    @Override
    public void move(int distance)
    {
        System.out.format("I am a Salmon I swim %d meters.\n", distance);
    }
    
    @Override
    public String toString()
    {
        String message = super.toString();
	message += "isEdible: " + isEdible + "\n";
        message += "swimsUpriverToLayEggs: " + swimsUpriverToLayEggs + "\n";
        return message;
    }
    
    @Override
    public boolean equals(Object other)
    {
        if(!super.equals(other))
            return false;

        Salmon salmon = (Salmon)other;
        if(this.isPink() != salmon.isPink())
            return false;
        
        if(this.isEdible() != salmon.isEdible())
            return false;

        if(this.swimsUpriverToLayEggs() != salmon.swimsUpriverToLayEggs())
            return false;

        return true;
    }
}
