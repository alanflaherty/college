 

public class Euro implements Comparable {

    public final static String ABBR = "EUR";
    private final double amount;
    
    public  Euro(double amt ){
        amount = amt;
        
        if(amt < 0)
        {
            throw new IllegalArgumentException("Cannot instantiate currency as a negative ");
        }
    }

    public double getAmount(){
        return amount;
    }
    
    public Euro add(Euro euro)
    {
        Euro eur = new Euro(this.amount + euro.getAmount());
        return eur;
    }

    @Override
    public String toString()
    {
        return String.format("%s%.2f",ABBR, amount );
    }
    
    public int compareTo(Object obj)
    {
        Euro euro = (Euro)obj;
        return this.amount > euro.getAmount() ? 1 : this.amount == euro.getAmount() ? 0 : -1 ;
    }
}