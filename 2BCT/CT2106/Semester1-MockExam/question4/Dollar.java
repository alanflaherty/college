 


public class Dollar {
    
    public final static String ABBR = "USD";
    private final double amount;
    
    public  Dollar(double amt ){
        amount = amt;        
        
        if(amt < 0)
        {
            throw new IllegalArgumentException("Cannot instantiate currency as a negative ");
        }
    }

    public double getAmount(){
        return amount;
    }
    
    public Dollar add(Dollar dollar)
    {
        Dollar d = new Dollar(this.amount + dollar.getAmount());
        return d;
    }
    
    @Override
    public String toString()
    {
        return String.format("%s%.2f",ABBR, amount );
    }
}

