
/**
 * Abstract class Money - write a description of the class here
 *
 * @author (your name here)
 * @version (version number or date here)
 */
public abstract class Money {
    private final double amount;

    public abstract String getABBR(); //= return "EUR";

    public Money(double amt ){
        amount = amt;
        
        if(amt < 0)
        {
            throw new IllegalArgumentException("Cannot instantiate currency as a negative amount.");
        }
    }

    public double getAmount(){
        return amount;
    }
    
    public abstract Money add(Money money);
    //{
    //    Money mon = new Money(this.amount + money.getAmount());
    //    return mon;
    //}

    @Override
    public String toString()
    {
        return String.format("%s%.2f",getABBR(), amount );
    }
}
