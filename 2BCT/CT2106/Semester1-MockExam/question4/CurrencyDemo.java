 
import java.util.*;

public class CurrencyDemo {
	
	public static void main (String [] args){
		Dollar dol = new Dollar(100.50);
		Euro  eur = new Euro(34.00);
		
		dol = dol.add(dol);
		eur = eur.add(eur);
	
		System.out.println(dol); //prints out 'USD201.00'
		System.out.println(eur); //prints out 'EUR68.00'

		// Dollar negativeCurrency = new Dollar(-100.50);
		
		ArrayList<Euro> euros = new ArrayList<Euro>();
		euros.add(new Euro(5));
		euros.add(new Euro(100));
		euros.add(new Euro(20));
		euros.add(new Euro(300));
		
		Collections.sort(euros);
	}

}
