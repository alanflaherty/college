import java.lang.reflect.*;


public class CodecFactory
{
    public CodecFactory()
    {
    }
    
    public IPlayMediaCodec getCodec(String extension)
    {
        IPlayMediaCodec  codec = null;
        
        try
        {
            Object createdCodec = Class.forName("Play"+extension.toUpperCase()+"MediaCodec").getConstructor() .newInstance();
            codec = (IPlayMediaCodec)createdCodec;
        }
        catch(ClassNotFoundException cnfex)
        {
        }
        catch(NoSuchMethodException nsmex)
        {
        }
        catch(InstantiationException iex)
        {
        }
        catch(IllegalAccessException ilaex)
        {
        }
        catch(InvocationTargetException itex)
        {
        }
        
        if(codec == null)
        {
            throw new UnsupportedMediaTypeException(extension.toUpperCase() + " is not a supported media type.");
        }
        
        // put your code here
        return codec;
    }
}
