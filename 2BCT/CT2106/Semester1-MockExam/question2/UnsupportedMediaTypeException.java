
/**
 * Write a description of class UnsupportedMediaTypeException here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class UnsupportedMediaTypeException extends IllegalArgumentException
{
    public UnsupportedMediaTypeException(String message)
    {
        super(message);
    }
}
