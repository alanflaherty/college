 

public class OpenVideoPlayer {
    
    
    public String play(MediaFile in){
        String filename = in.getFile();
        String ext = filename.substring(filename.length() - 3);
        
        System.out.println("ext: "+ ext);
        
        // find codec
        CodecFactory codecFactory = new CodecFactory();
        IPlayMediaCodec codec = codecFactory.getCodec(ext);
        
        // pass file to play
        codec.play(in);
        
        return null;
    }
    

}
