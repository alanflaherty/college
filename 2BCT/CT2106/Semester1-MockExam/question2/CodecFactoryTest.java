

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class CodecFactoryTest.
 *
 * @author  (your name)
 * @version (a version number or a date)
 */
public class CodecFactoryTest
{
    CodecFactory factory = null;

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
        factory = new CodecFactory();
    }
    
    @Test
    public void testInvalidCodec()
    {
        IPlayMediaCodec ogvCodec = factory.getCodec("non");
        assertNotNull(ogvCodec);
        assertTrue(ogvCodec instanceof UnsupportedMediaTypeCodec);
    }
    
    @Test
    public void testOGV()
    {
        IPlayMediaCodec ogvCodec = factory.getCodec("ogv");
        assertNotNull(ogvCodec);
        assertTrue(ogvCodec instanceof PlayOGVMediaCodec);
    }
    
    @Test
    public void testMP4()
    {
        IPlayMediaCodec mp4Codec = factory.getCodec("mp4");
        assertNotNull(mp4Codec);
        assertTrue(mp4Codec instanceof PlayMP4MediaCodec);
    }
    
    @Test
    public void testAVI()
    {
        IPlayMediaCodec aviCodec = factory.getCodec("avi");
        assertNotNull(aviCodec);        
        assertTrue(aviCodec instanceof PlayAVIMediaCodec);
    }
}
