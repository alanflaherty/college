public class UnsupportedMediaTypeCodec implements IPlayMediaCodec
{
    public void play(MediaFile media)
    {
        System.out.println("Unsupported Media Type " + media.getFile() + "...");
    }
}
