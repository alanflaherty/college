
/**
 * Write a description of class Cat here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Cat extends Feline implements GridObject
{ 
    
    /**
     * Constructor for objects of class Cat
     */
    public Cat(String name)
    {
        super(name);
        colour = "black";
    }
    
    @Override
    public char getAvatarCharacter()
    {
        return 'б';
    }
    
    @Override
    public int getEnergyPerTurn()
    {
        return 2;
    }   
    
    public boolean eats(Food food){
        return (food instanceof Canary);
    }

    public boolean eat(Food food) {
       
        if (food instanceof Canary)
        {
            Canary canary = (Canary)food;
            
            int canaryEnergy = canary.extractEnergy();
            this.energy += canaryEnergy;

            return true;
        }
        
        return false;
    }
    
    @Override
    public String move(double distance){
      
        if(energy==0){ // refactor ouyt 
            return String.format("%s is out of energy and cannot move.", this);
        }
        
        String message = "";
        energy -= this.getEnergyPerTurn();
        if(energy<0){
            energy = 0; // prevents energy having a negative number  
        }
        
        message += String.format("%s runs %f metres. ", this, distance);
        message += String.format("%s now has Energy level %d. \n", this, energy);
        
        return message;
    }
}
