
/**
 * Write a description of class Canary here.
 *
 * @author (Conor Hayes)
 * @version (October 5th 2017)
 */
public class Canary extends Bird implements Food, GridObject 
{
    // instance variables - replace the example below with your own
    

    /**
     * Constructor for objects of class Canary
     */
    public Canary(String name)
    {
        super(name); // call the constructor of the superclass Bird
        colour = "yellow"; // this overrides the value inherited from Bird
    }
    
    @Override
    public char getAvatarCharacter()
    {
        return 'ѱ';
    }
    
    @Override
    public int getEnergyPerTurn()
    {
        return 1;
    }    
    /**
     * Sing method overrides the sing method
     * inherited from superclass Bird
     */
    @Override // good programming practice to use @Override to denote overridden methods
    public void sing(){
        System.out.println("tweet tweet tweet");
    }

    @Override
    public boolean eat(Food food){
        if (food instanceof Seed)
        {
            Seed seed = (Seed)food;
            int seedEnergy = seed.extractEnergy();
            this.energy += seedEnergy;
            
            return true;
        }
        
        return false;
    }
    
    @Override
    public boolean eats(Food food){
        return (food instanceof Seed);
    }
    
    @Override // good programming practice to use @Override to denote overridden methods
    public String move(double distance)
    {
        String message = "";
        energy -= this.getEnergyPerTurn();
        if(energy<0){
            energy = 0; // prevents energy having a negative number  
        }
        
        message += String.format("%s flies %f metres. ", this, distance);
        message += String.format("%s now has Energy level %d. \n", this, energy);
        
        return message;
    }
    
    @Override
    public int getCalories(){
        return energy;
    }
    
    @Override
    public int extractEnergy(){
        
        int cal = energy;
        energy = 0;
        return cal;
    }
}
