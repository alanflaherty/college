
/**
 * Abstract class Vegetable - write a description of the class here
 *
 * @author (your name here)
 * @version (version number or date here)
 */
public abstract class Vegetable implements Food
{
    // instance variables - replace the example below with your own
    protected int calories;
    protected String name;    
    protected Location location;
    
    public Vegetable(String name){
        this.name = name;
    }

    public String getName(){
        return name;
    }

    public Location getLocation(){
        return location;
    }

    public void setLocation(Location location){
        this.location = location;
    }
   
    @Override
    public int getCalories(){
        return calories;
    }
    
    @Override
    public int extractEnergy(){
        return calories;
    }
    
    @Override
    public String toString(){
        return String.format("%s", name);
    }
}
