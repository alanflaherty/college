
/**
 * Write a description of class Seed here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Seed extends Vegetable implements Food, GridObject
{
    /**
     * Constructor for objects of class Seed
     */
    public Seed(String name)
    {
        super(name);
        calories = 10;      
    }
    
    @Override
    public char getAvatarCharacter()
    {
        return 'ŝ';
    }
    
    // possibly need to create a movable interface
    // that is implemented by animals and move this method there
    public int getEnergyPerTurn()
    {
        return 0;
    }   
}
