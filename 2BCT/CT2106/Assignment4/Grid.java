
/**
 * Central class for handling managing the grid and the
 *
 * @author (your name)
 * @version (a version number or a date)
 */

import java.util.ArrayList;
import java.util.Comparator;

public class Grid
{
    public static String MESSAGES[] = new String[5 * 5];

    private int size;
    private ArrayList<Location> locations;
    private ArrayList<GridObject> gridObjects;
    
    private ArrayList<GridObject> gameObjects;
    
    private String header = "";
    private String rowval = "";
    private String rowsep = "";
    private String footer = "";
    
    public Grid(int size)
    {
        this.size = size;

        locations = new ArrayList(size * size);
        gridObjects = new ArrayList();
       
        for(int i = 0; i < size; i++){
            for(int j = 0; j < size; j++){
                locations.add(new Location(i,j));
            }
        }
    }

    public void addAvatar(GridObject gridObject, int x, int y){
        int index = (size * x) + y;
        Location location = locations.get(index);
        location.setGridObject(gridObject);
        
        gridObjects.add(gridObject);
        gridObject.setLocation(location);
    }
    
    public static void main(String[] args){
        Grid grid = new Grid(5);
        
        Seed seed = new Seed("Poppy Seed");
        Seed seed2 = new Seed("Hay Seed");
        Seed seed3 = new Seed("Wheat Seed");
        Seed seed4 = new Seed("Oat Seed");
        
        grid.addAvatar(seed, 0, 0);
        grid.addAvatar(seed2, 0, 1);
        grid.addAvatar(seed3, 2, 2);
        grid.addAvatar(seed4, 4, 2);
        
        Canary topsie = new Canary("Topsie");
        Canary tim = new Canary("Tim");
     
        grid.addAvatar(topsie,4,4);
        grid.addAvatar(tim,0,4);
        
        Cat felix = new Cat("Felix");
        grid.addAvatar(felix,1,3);
        
        grid.start(10);
    }
    
    public void start(int rounds){
        
        // setup messages
        MESSAGES = new String[rounds];
        for(int i=0; i<MESSAGES.length; i++){
            MESSAGES[i] = "";
        }

        // 
        initaliseGridLayout();
        gameObjects = (ArrayList<GridObject>)gridObjects.clone();
        for(GridObject gameObject: gameObjects)
        {
            addMessage(0, "%s is at %s\n", new Object[]{gameObject, gameObject.getLocation()});
        }
        
        // Checking for winner
        int lastRound = 0;                
        boolean winner = false;
        int countAnimals = 0;
        int countSeeds = 0;                
        ArrayList<Animal> remainingAnimals = new ArrayList<Animal>();
        ArrayList<Vegetable> remainingVegetables = new ArrayList<Vegetable>();
        
        for(GridObject obj: gameObjects)
        {
            if(obj instanceof Animal)
            {
                countAnimals++;
                remainingAnimals.add((Animal)obj);
            }
            if(obj instanceof Vegetable)
            {
                countSeeds++;
                remainingVegetables.add((Vegetable)obj);
            }                    
        }
        
        // dodgy Label to break from inner for loop
        Outer:
        for(int i = 0; i< rounds; i++){
            // for each object, choose a new location and try move it there.
            // If the location is occupied see who eats who.
            // Possible error where object is discarded before its turn.
            lastRound = i;
            
            // Round header
            addMessage(i, "\n*** round %d ***\n", i);

            // Check for win . . . .  too many loops these should be tracked properly
            ArrayList<GridObject> removedObjects = new ArrayList<GridObject>();
            for(GridObject gridObject: gameObjects)
            {
                if(removedObjects.contains(gridObject))
                {
                    addMessage(i, "removedObjects.contains(gridObject): " + gridObject.getClass().getSimpleName() );
                }
                
                if(gridObject instanceof Vegetable)
                {
                    addMessage(i, "%s at %s with %d calories and and cannot move.\n", new Object[]{gridObject, gridObject.getLocation(), ((Vegetable)gridObject).getCalories() });
                }

                if(gridObject instanceof Animal)
                {
                    Location newLocation = getRandomLocation();
                    Animal movingAnimal = (Animal)gridObject;
                    
                    // If we are moving to our current pick another. smaller size grids...
                    while(newLocation == gridObject.getLocation())
                    {
                        newLocation = getRandomLocation();
                    }
                    
                    if(newLocation.isOccupied())
                    {
                        // TODO: check if moving to location with same type..
                        
                        if(newLocation.getGridObject() instanceof Food)
                        {
                            // Animal travels to food
                            Animal animal = (Animal)gridObject;
                            GridObject food = newLocation.getGridObject();
                            if(animal.eats((Food) newLocation.getGridObject()))
                            {
                                double distance = animal.getLocation().distanceTo(newLocation);
                                String info = animal.move(distance);
                                
                                // animal moves to ,location
                                addMessage(i, info);
                                addMessage(i, "%s is now at %s\n", animal, newLocation);
                                
                                // eats existing resident
                                animal.eat((Food) newLocation.getGridObject());
                                addMessage(i, "%s has now eaten %s. Energy level at %d\n", animal, newLocation.getGridObject(), animal.getEnergy());
                                
                                food.getLocation().setGridObject(null);
                                food.setLocation(null);
                                removedObjects.add(food);
                                
                                // actual move
                                gridObject.getLocation().setGridObject(null);
                                gridObject.setLocation(newLocation);

                                printGrid(i, gameObjects, 100 );
                            }
                        }
                        
                        if(gridObject instanceof Food)
                        {
                            // Food travels to predator
                            if(newLocation.getGridObject() instanceof Animal)
                            {
                                Animal animal = (Animal)newLocation.getGridObject();
                                // newLocation
                                if( animal.eats( (Food)gridObject ) )
                                {
                                    double distance = gridObject.getLocation().distanceTo(newLocation);
                                    
                                    // animal is food
                                    Animal a = (Animal)gridObject;
                                    String info = a.move(distance);
                                    
                                    // animal moves to ,location
                                    gridObject.setLocation(newLocation);
                                    
                                    addMessage(i, info);
                                    addMessage(i, "%s is now at %s\n", a, newLocation);
                                    
                                    // animal is eaten by existing resident
                                    animal.eat((Food) gridObject);
                                    addMessage(i, "%s has now eaten %s. Energy level at %d\n", newLocation.getGridObject(), animal, ((Animal)newLocation.getGridObject()).getEnergy() );
  
                                    // remove gridObject from grid 
                                    gridObject.getLocation().setGridObject(null);
                                    gridObject.setLocation(null);
                                    removedObjects.add(gridObject);
                                    
                                    printGrid(i, gameObjects, 100 );
                                }
                            }
                        }
                    }
                    else
                    {
                        // move to location, no existing resident.
                        double distance = gridObject.getLocation().distanceTo(newLocation);
                        newLocation.setGridObject(gridObject);
                        
                        Animal a = (Animal)gridObject;
                        String info = a.move(distance);
                        
                        //gridObject.getLocation().setGridObject(null);
                        gridObject.setLocation(newLocation);
                        
                        addMessage(i, info);
                        addMessage(i, "%s is now at %s\n", a, a.getLocation());
                        
                        if(a.getEnergy() == 0)
                        {
                            gridObject.getLocation().setGridObject(null);
                            gridObject.setLocation(null);
                            
                            removedObjects.add(gridObject);
                        }
                        
                        printGrid(i, gameObjects, 100 );
                    }
                }
            }
            
            // Remove inactive objects from collections
            gameObjects.removeAll(removedObjects);
            
            // recalculate winning conditions
            remainingAnimals.removeAll(removedObjects);
            countAnimals = remainingAnimals.size();
            remainingVegetables.removeAll(removedObjects);
            countSeeds = remainingVegetables.size();

            for(GridObject removedObject: removedObjects)
            {
                addMessage(i, "%s has been removed from the Grid\n", removedObject);
            }
            
            removedObjects.clear();
            
            // Check for winner
            winner = false;

            if(countAnimals == 1)
            {
                winner = true;
                Animal animal = remainingAnimals.get(0);
                addMessage(i, "\n\n********* GAME OVER ***********\n");
                addMessage(i, "Winner is %s with energy %s\n", animal, animal.getEnergy());
                break Outer;
            }
            else if(countAnimals > 1 && lastRound == rounds - 1  )
            {
                // 
                winner = true;
                addMessage(i, "\n\n********* GAME OVER ***********\nWinner:\n");
                
                int maxenergy = -1;
                
                remainingAnimals.sort(new Comparator<Animal>() {
                    @Override
                    public int compare(Animal a, Animal b) 
                    {
                        return a.energy > b.energy ? -1 : a.energy == b.energy ? 0 : 1 ;
                    }
                });
                
                for(Animal animal: remainingAnimals)
                {
                    if(maxenergy == -1 )
                    {
                        maxenergy = animal.getEnergy();
                        addMessage(i, "\t%s with energy %s\n", animal, animal.getEnergy());
                        // write out winner
                    }
                    else
                    {
                        if(animal.getEnergy() == maxenergy)
                        {
                            // write out winner
                            addMessage(i, "\t%s with energy %s\n", animal, animal.getEnergy());
                        }
                        else
                        {
                            break;
                        }
                    }
                }
                
                break Outer;
            }
            else if(countAnimals == 0 && countSeeds > 0)
            {
                winner = true;
                addMessage(i, "\n\n********* GAME OVER ***********\nWinners are:\n");
                
                for(Vegetable seed: remainingVegetables)
                {   
                    addMessage(i, "\t%s\n", seed);
                }
                break Outer;
            }
            
            // print grid
            printGrid(i, gameObjects);
            
        }
/*
        if(!winner && countAnimals > 0) // no clear winner
        {
            addMessage(lastRound, "********* GAME OVER ***********\nWinner is:");
            Animal roundWinner = remainingAnimals.get(0);    
            for(Animal animal: remainingAnimals)
            {   
                if(animal.getEnergy() > roundWinner.getEnergy())
                {
                    roundWinner = animal;
                }
            }
            addMessage(lastRound, "\t%s with %s Energy", roundWinner, roundWinner.getEnergy());
        }
 */       
        // print last round without messages
        printGrid( lastRound, gameObjects, 0, false);
        
        // print messages for all rounds
        for(String message: MESSAGES)
        {
           System.out.println(message);
        }
    }
    
    private void addMessage(int round, String message){
        MESSAGES[round] += message;
        printGrid( round, this.gameObjects, 0);
    }
    
    private void addMessage(int round, String messageFormat, Object... items){
        addMessage( false, round, messageFormat, items );
    }
    
    private void addMessage(  boolean printGrid, int round, String messageFormat, Object... items ){
        MESSAGES[round] += String.format(messageFormat, items);
        if(printGrid)
        {
            printGrid( round, this.gameObjects, 0);
        }
    }

    public Location getRandomLocation(){
        int x = (int)Math.round( Math.random() * (size - 1) );
        int y = (int)Math.round( Math.random() * (size - 1) );

        Location randomLocation = locations.get( (x * size) + y );
        return randomLocation;
    }
    
    private void initaliseGridLayout() {
        header  = "╭──";
        rowval  = "│ %s";
        rowsep  = "├──";
        footer  = "╰──";
        
        for(int i=0; i < (size-1); i++)
        {
            header  += "┬──";
            rowval  += "│ %s";
            rowsep  += "┼──";
            footer  += "┴──";
        }
        
        header  += "╮";
        rowval  += "│\n";
        rowsep  += "┤";
        footer  += "╯";
    }
    
    // Use string instead of char, value type boxing to Object[] for System.out.format(format, []) issue, ...... this is shit..
    private String[] getRow(int rowNum)
    {
        String row[] = new String[size];
        
        for(int i=0; i < size; i++ )
        {
            row[i] = " ";
            GridObject gridObject = locations.get( i + (rowNum*size)).getGridObject();
            
            if(gridObject != null)
            {
                // should just be char... this is  bad
                row[i] = String.valueOf(gridObject.getAvatarCharacter());
            }
        }
        return row;
    }
    
    private int defaultTimeoutMillisec = 500;
    private void printGrid( int round, ArrayList<GridObject> objects)
    {
        printGrid(round, objects, defaultTimeoutMillisec );
    }
    
    private void printGrid( int round, ArrayList<GridObject> objects, int timeoutMillisec)
    {
        printGrid(round, objects, timeoutMillisec, true );
    }
    
    private void printGrid(int round, ArrayList<GridObject> objects, int timeoutMillisec, boolean printRound)
    {
        // debug code check for object in multiple locations
        validateObjects(round, objects);

        System.out.print('\u000C');
        System.out.println("*** round " + round + " ***");
        System.out.println(header);
        for(int j =0; j < size; j++)
        {
            String[] row = getRow(j);
            System.out.format(rowval, (Object[]) row);
            
            if(j != (size-1))
            {
                System.out.println(rowsep);
            }   
        }        
        System.out.println(footer);
        
        System.out.println("");
        System.out.println("Number of Game Objects:" + objects.size());
        System.out.println("");

        if(printRound)
        {
            System.out.println("Messages:");

            System.out.println(MESSAGES[round]);
        }
    
        try        
        {
            Thread.sleep(200);
        } 
        catch(InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
    }

    
    // debug code
    private void validateObjects(int round, ArrayList<GridObject> objects)
    {
        for(GridObject gridObject:objects)
        {
            for( Location location: locations )
            {
                if(location.getGridObject() == gridObject && gridObject.getLocation() != location)
                {
                    addMessage(false, round, "%s in multiple locations %s and additionally %s\n", gridObject, gridObject.getLocation(), location);
                }
            }
        }
    }
}
