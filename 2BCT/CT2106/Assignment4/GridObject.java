
/**
 * Write a description of interface GridObject here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public interface GridObject
{
     public Location getLocation();
     public void setLocation(Location location);
     public char getAvatarCharacter();
     public int getEnergyPerTurn();
}
