
/**
 * Write a description of class Animal here.
 *
 * @author (conor hayes)
 * @version (October 5th 2017)
 */
public abstract class Animal
{
    // instance variables - replace the example below with your own
    protected boolean hasSkin;
    protected boolean breathes;
    protected String colour;
    protected int energy = 5;
    protected String name; // each Animal has a name
    
    protected Location location; 
    
    /**
     * Constructor for objects of class Animal
     */
    public Animal(String name)
    {
        this.name = name;
        breathes = true; //all the subclasses of Animal inherit this property and value
        hasSkin = true; // all the subclasses of Animal inherit this property and value
        colour = "grey"; //all the subclasses of Animal inherit this property and value
    }

    /**
     * move method
     * param int distance - the distance the Animal should move
     * All subclasses inherit this method
     */
    public abstract String move(double distance);
    
    public abstract boolean eats(Food food);
    /**
     * eat method
     * param Food food - food the Animal should eat
     * We haven't defined this yet
     * All subclasses inherit this method
     */
    public abstract boolean eat(Food food);
    
    /**
     * getter method for colour field
     * All subclasses inherit this method
     */
    public String getColour(){
        return colour;
    }
    
    /**
     * getter method for extract field
     * All subclasses inherit this method
     */
    public int getEnergy(){
        return energy;
    }
    
     /**
     * 'getter' method for haSkin field
     * All subclasses inherit this method
     */
    public boolean hasSkin(){
        return hasSkin;
    }
    
    public Location getLocation(){
        return location;
    }

    public String getName(){
        return name;
    }
    
    public void setName(String name){
        this.name = name;
    }

    public void setLocation(Location location){
        if(location == null)
        {
        }

        if(this.location != null && this.location.getGridObject() != null)
        {
            if(this.location.getGridObject() != this)
            {
                this.location.getGridObject().setLocation(null);
            }
        }

        this.location = location;
    }
    
    @Override
    public String toString(){
        return String.format("%s the %s", name, this.getClass().getSimpleName());
    }
}
