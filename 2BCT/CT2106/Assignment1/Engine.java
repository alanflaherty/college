
/**
 * Write a description of class Engine here.
 *
 * @author (Alan Flaherty)
 * @version (0.0.1)
 */
public class Engine
{
    private String name;
    private double fuel;
    private double wheelTurnsPerLitre;
    private Wheel wheel;

    /**
     * Constructor for objects of class Engine
     */
    public Engine(String name, double wheelTurnsPerLitre)
    {
        this.name = name;
        this.wheelTurnsPerLitre = wheelTurnsPerLitre;
        this.fuel =0;
    }

    public void setFuel(double value){
        this.fuel = value;
    }
    
    public double getFuel(){
         return this.fuel;
    }
    
    public void setWheelTurnsPerLitre(double value){
        this.wheelTurnsPerLitre = value;
    }
    
    public double getWheelTurnsPerLitre(){
         return this.wheelTurnsPerLitre;
    }

    public void addWheel(Wheel wheel){
        this.wheel = wheel;
    }
    
    public Wheel getWheel(){
        return this.wheel;
    }
}
