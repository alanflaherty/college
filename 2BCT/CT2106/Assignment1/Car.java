
import java.lang.Math;
/**
 * Write a description of class Car here.
 *
 * @author (Alan Flaherty)
 * @version (0.0.1)
 */
public class Car
{
    private Engine engine;
    private String name;
    private double distanceKm;
 
    /**
     * Constructor for objects of class Car
     */
    public Car(String name)
    {
        this.name = name;
        this.distanceKm = 0;
    }
    
    public void add(Engine engine){
        this.engine = engine;
    }
    
    public void add(Wheel wheel){
        engine.addWheel(wheel);
    }
    
    public double getFuel() {
        return engine.getFuel();
    }    

    public void setFuel(int fuel){
        if(fuel == (Integer.MAX_VALUE + 1))
            fuel = 0;
        
        engine.setFuel(fuel);
    }
    
    public void run()
    {
        double distancePerLitreCm =  this.engine.getWheelTurnsPerLitre() * this.engine.getWheel().getCircumference();
        distanceKm = ( distancePerLitreCm * getFuel() ) / 100 /1000 ;
        // result is in cm so divide by 100 to get meters and then divide by 1000 to get km
    }
    
    public double getDistanceKm()
    {
        return distanceKm;
    }
}
