import java.lang.Math;

/**
 * Write a description of class Wheel here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Wheel
{
    private double radius;
    private String name;
    private double circumference;

    public Wheel(String name, double raduisCm)
    {
    this.name = name;
    this.radius = raduisCm;
    
    this.circumference = (2 * Math.PI * raduisCm);
    }
    
    public double getCircumference(){
    return this.circumference;
    }
    
    public String getName(){
    return this.name;
    }
    
    public double getRadius(){
    return radius;
    }
}