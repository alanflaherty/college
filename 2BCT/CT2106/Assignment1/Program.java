
/**
 * Write a description of class Program here.
 *
 * @author (your name)
 * @version (a version number or a date)
 */
public class Program
{
    
    public void main(String[] args)
    {
        // 20 inch tyres 50.8cm
        runConfiguration( "X7", "DR9", 3732, "Wichelin20", 50.8, 100);
        
        // 18 inch tyres 45.8cm
        runConfiguration( "X7", "DR9", 3732, "Wichelin18", 45.72, 100);
        
        // 15 inch tyres 
        runConfiguration( "X5", "DRX", 3900, "Wichelin15", 38.1, 100);
        // 15 inch tyres 
        runConfiguration( "X5", "DRX-1", 3500, "Woyo RS1 Slick Race ", 38.1, 100);
    }
    
    private void runConfiguration( String carName, String engineName, int rotationsPerLitre, String tyreName, double sizeCm, int fuelInLitres)
    {
        Car car = new Car(carName);
        System.out.println("Creating an "+carName+" with");

        Engine engine = new Engine(engineName, rotationsPerLitre);
        car.add(engine);
        System.out.println(" * A " + engineName + " engine");

        Wheel wheel = new Wheel (tyreName, sizeCm);
        car.add(wheel);        
        System.out.println(" * " + tyreName + " tyres");

        car.setFuel(fuelInLitres);
        System.out.print("\n100l of fuel goes: ");

        car.run();

        double dist = car.getDistanceKm();
        System.out.format("%fkm\n\n", dist);
    }
}
