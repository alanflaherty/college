package org.alanflaherty.CT2106.CardApplication;

public class Main {

    public static void main(String[] args) {
        System.out.println("\n# main ###########################\n\nTesting Card class:\n");

        Card card = new Card(2, 4);
        printCard(card);

        System.out.println("\n# /main ##########################\n\n");
    }

    private static void printCard(Card card)
    {
        System.out.format("Card: %s\n", card);
        System.out.format("Symbol: %s\n", card.toUnicodeCard());
    }
}
