package org.alanflaherty.CT2106.CardApplication;

/**
 * Created by alan on 16/11/17.
 */

public class Card {

    private int suit, rank;

//    private String[] suits = {"Clubs", "Diamonds", "Hearts", "Spades"};
    public static final String[] SUITS = {"♧", "♢", "♡", "♤"};
    public static final String[] RANKS = {  null, "Ace", "Two", "Three", "Four", "Five",
                                            "Six", "Seven", "Eight", "Nine", "Ten",
                                            "Jack", "Queen", "King"
    };

    public static final String[][] CARDS = {
                                {null, "🃑", "🃒", "🃓", "🃔", "🃕", "🃖", "🃗", "🃘", "🃙", "🃚", "🃛", "🃜", "🃝", "🃞"},
                                {null, "🃁", "🃂", "🃃", "🃄", "🃅", "🃆", "🃇", "🃈", "🃉", "🃊", "🃋", "🃌", "🃍", "🃎"},
                                {null, "🂱", "🂲", "🂳", "🂴", "🂵", "🂶", "🂷", "🂸", "🂹", "🂺", "🂻", "🂼", "🂽", "🂾"},
                                {null, "🂡", "🂢", "🂣", "🂤", "🂥", "🂦", "🂧", "🂨", "🂩", "🂪", "🂫", "🂬", "🂭", "🂮"}
    };

/*
🂡	🂱	🃁	🃑
🂢	🂲	🃂	🃒
🂣	🂳	🃃	🃓
🂤	🂴	🃄	🃔
🂥	🂵	🃅	🃕
🂦	🂶	🃆	🃖
🂧	🂷	🃇	🃗
🂨	🂸	🃈	🃘
🂩	🂹	🃉	🃙
🂪	🂺	🃊	🃚
🂫	🂻	🃋	🃛
🂬	🂼	🃌	🃜
🂭	🂽	🃍	🃝
🂮	🂾	🃎	🃞

🂠

🃏 🃟

🂠

    	🃏
    	🃟
*/
    public Card (int suit, int rank) throws IllegalArgumentException {

        if(suit<0 || suit> Card.SUITS.length-1){
            throw new IllegalArgumentException("Incorrect suit value " + suit);
        }

        if(rank<1 || rank> Card.RANKS.length-1){
            throw new IllegalArgumentException("Incorrect rank value " + rank);
        }

        this.suit = suit;
        this.rank = rank;
    }

    public int getSuit(){
        return suit;
    }

    public int getRank(){
        return rank;
    }

    @Override
    public String toString(){
        return Card.RANKS[rank] + " of " + Card.SUITS[suit]; //returns rank of suit
    }

    public String toUnicodeCard() {
        return CARDS[suit][rank];
    }
}
