/**
 * Created by alan on 16/11/17.
 */
package org.alanflaherty.CT2106.CardApplication.test;

import org.alanflaherty.CT2106.CardApplication.*;

import org.junit.*;
import org.junit.rules.ExpectedException;

import static org.hamcrest.core.StringStartsWith.startsWith;

public class CardTest {
    private Card card = null;

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Before
    public void setup(){
        card = new Card(2,10);
    }

    @Test
    public void unicodeCardTest(){
        Assert.assertEquals("🂺", card.toUnicodeCard());
    }

    @Test
    public void testExceptionSuit(){
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage(startsWith("Incorrect suit value 100"));

        card = new Card (100, 1);
    }

    @Test
    public void testExceptionRank(){
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage(startsWith("Incorrect rank value 100"));

        card = new Card (1, 100);
    }

//    @Test
//    public void testTemplate(){
//        System.out.format("%d\n", card);
//        Assert.assertFalse("Intentional Failure", true);
//    }

//    @Test
//    public void testExceptionRank(){
//        thrown.expect(IllegalArgumentException.class);
//        thrown.expectMessage(startsWith("Incorrect rank value 100"));
//
//        card = new Card (1, 100);
//    }

}