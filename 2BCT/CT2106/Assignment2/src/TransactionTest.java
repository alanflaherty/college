
/**
 * TransactionTest is a class that does one thing only
 * It provides methods for testing out different test scenarios 
 * for our Shopping Cart Transaction classes
 * @author Alan Flaherty
 * @version 13 October 2017
 */
public class TransactionTest
{
    /**
     * main method to execute the TransactionTest methods
     */
    public static void main(String[] args) throws Exception
    {
       TransactionTest test = new TransactionTest();
       test.transaction1();
    }
    
    public void transaction1() throws Exception
    {  
        Customer customer = new Customer("Niamh", "O'Leary", "niamhol@zmail.com");
        
        ShoppingCart cart = new ShoppingCart();
        Item item1 = new Item("Product 1", 100, 10.00);
        Item item2 = new Item("Product 2", 101, 5.00);
        Item item3 = new Item("Product 3", 102, 2.00);
        
        cart.addItem(item1, 2);
        cart.addItem(item2, 1);
        cart.addItem(item3, 7);
        
        Address deliveryAddress = new Address();
        deliveryAddress.setLine1("Line 1");
        deliveryAddress.setLine2("Line 2");
        deliveryAddress.setLine3("Line 3");
        deliveryAddress.setCounty("County");
        deliveryAddress.setCountry("Country");

        Payment payment = new Payment();
        payment.setCardNumber("4485340592263186");
        payment.setCCV("924");
        payment.setExpiryMonth(12);
        payment.setExpiryYear(2017);

        Address billingAddress = new Address();
        billingAddress.setLine1("Line 1");
        billingAddress.setLine2("Line 2");
        billingAddress.setLine3("Line 3");
        billingAddress.setCounty("County");
        billingAddress.setCountry("Country");
        payment.setBillingAddress(billingAddress);
        
        cart.show();

        process(cart, customer, deliveryAddress, payment);
    }
    
    public void transaction2() throws Exception
    {
        Customer customer = new Customer("Niamh", "O'Leary", "niamhol@zmail.com");
        
        ShoppingCart cart = new ShoppingCart();
        Item item1 = new Item("Product 1", 100, 10.99);
        Item item2 = new Item("Product 2", 101, 1.50);
        Item item3 = new Item("Product 3", 102, 2.60);
        
        cart.addItem(item1,2);
        cart.addItem(item2,1);
        cart.addItem(item3,7);
        
        cart.show();           // < print out the cart as it is
        cart.remove(item1);
        
        cart.show();           // < print out the cart as it is
        
        Address deliveryAddress = new Address();
        deliveryAddress.setLine1("Line 1");
        deliveryAddress.setLine2("Line 2");
        deliveryAddress.setLine3("Line 3");
        deliveryAddress.setCounty("County");
        deliveryAddress.setCountry("Country");
        
        Payment payment = new Payment();
        payment.setCardNumber("4485340592264186");
        payment.setCCV("924");
        payment.setExpiryMonth(12);
        payment.setExpiryYear(2017);        

        Address billingAddress = new Address();
        billingAddress.setLine1("Line 1");
        billingAddress.setLine2("Line 2");
        billingAddress.setLine3("Line 3");
        billingAddress.setCounty("County");
        billingAddress.setCountry("Country");
        payment.setBillingAddress(billingAddress);

        process(cart, customer, deliveryAddress, payment);
    }
    
    private void process(ShoppingCart cart, Customer customer, Address deliveryAddress, Payment payment) throws Exception
    {
        Order order = cart.confirm();

        order.setCustomer(customer);
        order.addDeliveryAddress(deliveryAddress);
        order.addPayment(payment);
        
        OrderFailureReason reason = payment.validate();
        if(reason != OrderFailureReason.None)
        {
            Email email = new OrderFailedEmail(order, reason);
            email.send();

            return;
        }

        Email email = new OrderSuccessfulEmail(order);
        email.send();
    }
}