
import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class OrderTest.
 *
 * @author Alan Flaherty
 * @version 13 October 2017
 */
public class OrderTest
{
    Order order;
    Address address;
    Payment payment;

    /**
     * Default constructor for test class OrderTest
     */
    public OrderTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
        ShoppingCart cart = new ShoppingCart();
        
        Item product1 = new Item("Product 1", 101, 10.00);
        Item product2 = new Item("Product 2", 102, 5.00);
        Item product3 = new Item("Product 3", 103, 2.00);
        
        cart.addItem(product1, 1);
        cart.addItem(product2, 2);
        cart.addItem(product3, 3);
        
        order = cart.confirm();
        
        //
        address = new Address();
        address.setLine1("Line 1");
        address.setLine2("Line 2");
        address.setLine3("Line 3");
        address.setCounty("County");
        address.setCountry("Country");
        
        payment = new Payment();
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
    
    @Test(expected = Exception.class)
    public void adding_a_second_delivery_address_should_throw_exception() throws Exception
    {
        order.addDeliveryAddress(address);
        order.addDeliveryAddress(address);
    }
    
    @Test(expected = Exception.class)
    public void adding_a_second_payment_should_throw_exception() throws Exception
    {
        order.addPayment(payment);
        order.addPayment(payment);
    }
    
    @Test
    public void get_total_should_calculate_the_order_value()
    {
	    // act
        double total = order.getTotal();
        
        // assert
        assertEquals("Invalid total", 26.00, total, 0.000001);
    }
}
