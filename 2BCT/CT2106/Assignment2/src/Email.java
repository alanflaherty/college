import java.util.HashMap;
import java.util.Map;

/**
 * Base class for implementing client emails.
 *
 * @author Alan Flaherty
 * @version 13 October 2017
 */
public abstract class Email
{
    Order customerOrder;
    public Email(Order order)
    {
        this.customerOrder = order;
    }
    
    protected Order getOrder() {
        return this.customerOrder;
    }
    
    protected String getOrderContents()
    {
        String cartContents = "";
        for( Map.Entry<Item, Integer> entry: this.getOrder().getItems().entrySet() )
        {
            double linePrice = entry.getValue() * entry.getKey().getPrice();
            cartContents += String.format("%s x %d \t €%.2f \n", entry.getKey().getName(), entry.getValue(), linePrice);
        }
        
        cartContents += String.format("Total Cost: €%.2f\n\n",  this.getOrder().getTotal());

        return cartContents;
    }

    public abstract void send();
}
