/**
 * Outputs an order succesful client email to the console.
 *
 * @author Alan Flaherty
 * @version 13 October 2017
 */
public class OrderSuccessfulEmail extends Email
{
    public OrderSuccessfulEmail (Order order)
    {
        super(order);
    }
    
    @Override
    public void send()
    {        
        Customer customer = this.getOrder().getCustomer();
        String message = "Dear " + customer.getFullName() + ", \n\n";
        message += "Your successful order: \n\n";
        message += getOrderContents();
        
        message += "Delivery Address:\n";
        Address deliveryAddress = this.getOrder().getDeliveryAddress();
        message += deliveryAddress.getLine1() + "\n";
        message += deliveryAddress.getLine2() + "\n";
        message += deliveryAddress.getLine3() + "\n";
        message += deliveryAddress.getCounty() + "\n";
        message += deliveryAddress.getCountry() + "\n\n";

        Payment payment = this.getOrder().getPayment();
        message += "Billed Credit Card:\n";
        message += "CardNumber: " + maskCC(payment.getCardNumber()) + "\n";
        message += "CCV:        " + payment.getCCV() + "\n";
        message += "Expiry:     " + payment.getExpiryMonth() + "/" + payment.getExpiryYear() + "\n";
        
        System.out.println("Order Recieved: " + customer.getFullName());
        
        System.out.println("Subject: Your Order");
        System.out.println(message);
    }
    
    private String maskCC(String creditCard)
    {
        String lastFourDigits = creditCard.substring(creditCard.length() - 4);
	String mask = new String(new char[(creditCard.length() - 4)]).replace("\0", "*");
	return  mask + lastFourDigits;
    }
}

