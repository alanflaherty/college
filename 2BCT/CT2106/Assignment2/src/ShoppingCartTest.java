
import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

/**
 * The test class ShoppingCartTest.
 *
 * @author Alan Flaherty
 * @version 13 October 2017
 */
public class ShoppingCartTest
{
    // ShoppingCart 
    ShoppingCart cart;
    
    // Products
    Item product1;
    Item product2;
    Item product3;

    /**
     * Default constructor for test class ShoppingCartTest
     */
    public ShoppingCartTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {
        cart = new ShoppingCart();

        product1 = new Item("Product 1", 101, 10.00);
        product2 = new Item("Product 2", 102, 2.00);        
        product3 = new Item("Product 3", 103, 5.00);
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }
    
    @Test
    public void adding_a_product_to_the_cart_should_add_product_to_internal_store()
    {
        // arrange
        cart.addItem(product1, 1);
        
        // act/assert
        assertEquals("Items not added to cart ", cart.getItems().size(), 1);
    }
    
    @Test
    public void adding_an_existing_product_to_the_cart_should_not_add_to_internal_store()
    {
        // arrange
        cart.addItem(product1, 1);
        cart.addItem(product1, 1);
        
        // this should not be possible due to keying on maps
        assertEquals("2 items  ", cart.getItems().size(), 1);
        
    }
    
    @Test
    public void adding_an_existing_product_to_the_cart_should_increment_the_key_value_count()
    {
        // arrange
        cart.addItem(product1, 1);
        cart.addItem(product1, 1);

        // act
        Item firstItem = cart.getItems().keySet().iterator().next();
        int count = cart.getItems().get(firstItem);
        
        // assert
        assertEquals("Item not incremented correctly", 2, count);
    }
    
    @Test
    public void total_after_adding_three_products_is_correct()
    {
        // arrange
        cart.addItem(product1, 1);
        cart.addItem(product2, 1);
        cart.addItem(product3, 2);
        
        // act
        double total = cart.getTotal();
        
        // assert
        assertEquals("Invalid total", 22.00, total, 0.000001);
    }
    
    @Test
    public void confirming_the_shopping_cart_should_create_an_order()
    {
        // arrange 
        cart.addItem(product1, 1);
        cart.addItem(product2, 2);
        
        // act
        Order order = cart.confirm();

        // assert        
        assertNotNull(order);
    }
    
    @Test
    public void close_method_should_prevent_addition_of_further_products()
    {
        boolean addedFirst = cart.addItem(product1, 1);
        assertTrue("Addded first item to cart.", addedFirst);
        
        cart.close();
        
        boolean addedAfterClose = cart.addItem(product2, 1);
        assertFalse("Added item after the cart was closed. ", addedAfterClose);
    }
    
    /*
    @Test
    public void test_method_shell()
    {   
        assertTrue("Not Yet Implemented", false);
    }
    */
}
