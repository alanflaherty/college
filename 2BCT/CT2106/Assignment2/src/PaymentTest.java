
import static org.junit.Assert.*;
import static org.assertj.core.api.Assertions.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


/**
 * Tests the Payment class.
 *
 * @author Alan Flaherty
 * @version 13 October 2017
 */
public class PaymentTest
{
    private Payment payment;
    private static final String VALID_CREDITCARD = "4485340592263186";
    private static final String VALID_CCV = "924";

    public PaymentTest()
    {
    }

    /**
     * Sets up the test fixture.
     *
     * Called before every test case method.
     */
    @Before
    public void setUp()
    {        
        payment = new Payment();
        payment.setExpiryMonth(currentMonth());
        payment.setExpiryYear(currentYear() + 1);

        payment.setCardNumber(VALID_CREDITCARD);
        payment.setCCV(VALID_CCV);
    }

    /**
     * Tears down the test fixture.
     *
     * Called after every test case method.
     */
    @After
    public void tearDown()
    {
    }

    private int currentMonth()
    {
        return 10;
    }
    
    private int currentYear()
    {
        return 2017;
    }

    
    @Test
    public void setting_the_expiry_month_to_previous_month_should_fail_validation()
    {
        // arrange
        payment.setExpiryMonth(currentMonth() - 1);
        payment.setExpiryYear(currentYear());

        // act / assert
        assertThat(payment.validate()).isEqualTo(OrderFailureReason.CardExpired);
    }
    
    @Test
    public void setting_the_expiry_year_to_previous_year_should_fail_validation()
    {
        // arrange
        payment.setExpiryMonth(currentMonth());
        payment.setExpiryYear(currentYear()-1);

        // act / assert
        assertThat(payment.validate()).isEqualTo(OrderFailureReason.CardExpired);
    }
    
    @Test
    public void setting_the_expiry_to_the_end_of_current_month_should_pass_validation()
    {
        // arrange 
        payment.setExpiryMonth(currentMonth());
        payment.setExpiryYear(currentYear());

        // act / assert
        assertThat(payment.validate()).isEqualTo(OrderFailureReason.None);
    }

    @Test
    public void setting_the_card_number_to_a_card_number_with_an_invalid_number_of_digits_should_fail_validation()
    {
        // arange
        payment.setCardNumber("47928829256269011");

        // act / assert
        assertThat(payment.validate()).isEqualTo(OrderFailureReason.CardNumberInvalid);
    }

    @Test
    public void setting_the_card_number_to_an_invalid_card_number_should_fail_validation()
    {
        // arrange
        payment.setCardNumber("4792882925626901");

        // act / assert
        assertThat(payment.validate()).isEqualTo(OrderFailureReason.CardNumberInvalid);
    }
    
    @Test
    public void setting_the_card_number_to_a_valid_card_number_should_pass_validation()
    {
        // arrange
        payment.setCardNumber(this.VALID_CREDITCARD);

        // act / assert
        assertThat(payment.validate()).isEqualTo(OrderFailureReason.None);
    }
    
    @Test
    public void setting_the_ccv_number_to_invalid_content_should_fail_validation()
    {
        // arrange 
        payment.setCCV("abc");

        // act / assert
        assertThat(payment.validate()).isEqualTo(OrderFailureReason.InvalidCCV);
    }
    
    @Test
    public void setting_the_ccv_number_to_an_invalid_number_should_fail_validation()
    {
        // arrange
        payment.setCCV("25");

        // act / assert
        assertThat(payment.validate()).isEqualTo(OrderFailureReason.InvalidCCV);
    }
    
    @Test
    public void setting_the_ccv_number_to_an_valid_number_should_pass_validation()
    {
        // arrange
        payment.setCCV("257");

        // act / assert
        assertThat(payment.validate()).isEqualTo(OrderFailureReason.None);
    }
}
