/**
 * Holds information on a Sales Item.
 *
 * @author Alan Flaherty
 * @version 13 October 2017
 */ 
public class Item 
{
    private String name;
    private double price;
    private long itemId;
    
    public Item(String itemName, long id, double price) {
    	this.name = itemName;
    	this.itemId = id;
    	this.price = price;
    }
    
    public void setPrice(double price){
    	this.price = price;
    }
    
    public double getPrice() {
    	return price;
    }
    
    public void setName(String name){
    	this.name = name;
    }
    
    public String getName() {
    	return name;
    }
    
    @Override
    public String toString(){
    	String out = "Item Id: " + itemId + "\t" + name +"\tPrice: " + price;
    	return out;
    }
}
