import java.util.HashMap;
import java.util.Map;

/**
 * Writes out a order failed email to the console.
 *
 * @author Alan Flaherty
 * @version 13 October 2017
 */ 
public class OrderFailedEmail extends Email
{
    private OrderFailureReason orderFailureReason;
    public void setOrderFailedreason(OrderFailureReason orderFailureReason) {
         this.orderFailureReason = orderFailureReason;
    }
    
    public OrderFailureReason getOrderFailedReason() {
        return this.orderFailureReason;
    }
    
    public OrderFailedEmail (Order order, OrderFailureReason orderFailureReason)
    {
        super(order);
        
        this.orderFailureReason = orderFailureReason;
    }
    
    @Override
    public void send()
    {
        Customer customer = this.getOrder().getCustomer();
        String message = "\n\nDear " + customer.getFullName() + "\n\n";
        message += "Your Recent order, as detailed: \n\n";
        message += getOrderContents();
        message += "\nFailed due to: " + getMessage(orderFailureReason);
        
        System.out.println("Order Failed Email to: " + customer.getFullName());
        System.out.println("Subject: Order Failed due to - " + getMessage(orderFailureReason) );
        System.out.println(message);
    }

    public String getMessage(OrderFailureReason reason)
    {
        switch(reason)
        {
            case CardNumberInvalid:
                return "Invalid Card Number";

            case CardExpired:
                return "Card Expired";

            case InvalidCCV:
                return "CCV is Invalid";

            default:
                return null;
        }
    }
 }
