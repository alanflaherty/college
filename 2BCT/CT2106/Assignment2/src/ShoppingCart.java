import java.util.HashMap;
import java.util.Map;
import java.util.Iterator;

public class ShoppingCart
{
    // Dictionary
    private Map<Item,Integer> items;
    private boolean isClosed = false;
    
    public ShoppingCart()
    {
        this.items = new HashMap<Item,Integer>();
    }
    
    public boolean addItem(Item item, int numberOfItems)
    {
        if(isClosed)
            return false;

        if( items.keySet().contains(item) )
        {
            int existingNumberOfItems = items.get(item);
            items.put(item, existingNumberOfItems + numberOfItems);
        }
        else
        {
            items.put(item, numberOfItems);
        }

        return true;
    }
    
    public Map<Item,Integer> getItems()
    {
        return items;
    }
    
    public void removeAll()
    {
        this.items.clear();
    }
    
    public void remove(Item item)
    {
        this.items.remove(item);
    }
    
    public boolean isValid()
    {
        // TODO: Validation code
        return false;
    }
    
    public double getTotal()
    {
        double total = 0.0;
        for( Map.Entry<Item, Integer> entry: items.entrySet() )
        {
            total += (entry.getKey().getPrice() * entry.getValue());
        }
        
        return total;
    }
    
    //
    public String show()
    {
        String message = "Shopping Cart Contents:\n";

        // TODO: Tidy up alignment
        for( Map.Entry<Item, Integer> entry: items.entrySet() )
        {
            double linePrice = entry.getValue() * entry.getKey().getPrice();
            message += String.format("%s x %d \t €%.2f \n", entry.getKey().getName(), entry.getValue(), linePrice);
        }

        message += String.format("Total: €%.2f \n", getTotal());
        System.out.println(message);
        return message;
    }

    public void close()
    {
        this.isClosed = true;
    }

    public Order confirm()
    {
        this.close();
        Order order = new Order (this);        
        return order;
    }
}