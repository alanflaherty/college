import java.util.HashMap;
import java.util.Map;

/**
 * Order class is used once the process of adding items is complete and 
 * the user is ready to complete their order by adding delivery and payment 
 * details. 
 *
 * @author Alan Flaherty
 * @version 13 October 2017
 */ 
public class Order
{
    private Map<Item,Integer> items;
    private Address deliveryAddress;
    private Customer customer;    
    private Payment payment;
    
    private double total;

    public Order(ShoppingCart cart)
    {
        this.items = new HashMap<Item,Integer>();
        moveItemsIntoOrder(cart);
        cart.removeAll();
    }
    
    private void moveItemsIntoOrder(ShoppingCart cart)
    {
        for( Map.Entry<Item, Integer> entry: cart.getItems().entrySet() )
        {
            this.items.put(entry.getKey(), entry.getValue());
        }
    }

    public Customer getCustomer()
    {
        return customer;
    }

    public void setCustomer(Customer customer)
    {
        this.customer = customer;
    }

    public void addDeliveryAddress(Address address) throws Exception
    {
        if( this.deliveryAddress!= null )
            throw new Exception("Existing Delivery Address");

        deliveryAddress = address;
    }

    public Address getDeliveryAddress()
    {
        return this.deliveryAddress;
    }

    public void addPayment(Payment payment) throws Exception
    {
       if( this.payment!= null )
           throw new Exception("Existing Payment Address");
           
       this.payment = payment;
    }

    public Map<Item,Integer> getItems()
    {
        return items;
    }

    public Payment getPayment(){
        return this.payment;
    }
    
    public double getTotal()
    {
        if(total == 0)
        {
            for( Map.Entry<Item, Integer> entry: items.entrySet() )
            {
                total += (entry.getKey().getPrice() * entry.getValue());
            }
        }
        
        return total;
    }
}
