import java.util.Date;
import java.util.Calendar;

/**
 * Holds details on the client payment.
 *
 * @author Alan Flaherty
 * @version 13 October 2017
 */
public class Payment
{
    // instance variables - replace the example below with your own
    private Address billingAddress;
    
    private String cardNumber = "";
    private String ccv;
    private int expiryMonth;
    private int expiryYear;
    
    public void setBillingAddress(Address address) {
        this.billingAddress = address;
    }

    public Address getBillingAddress() {
        return this.billingAddress;
    }
    
    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
    
    public String getCardNumber() {
        return this.cardNumber;
    }
    
    // private String ccv;
    public void setCCV(String ccv) {
        this.ccv = ccv;
    }

    public String getCCV() {
        return this.ccv;
    }
    
    // expiryMonth
    public void setExpiryMonth(int expiryMonth) {
        this.expiryMonth = expiryMonth;
    }
    
    public int getExpiryMonth() {
        return this.expiryMonth;
    }

    // expiryYear
    public void setExpiryYear(int expiryYear) {
        this.expiryYear = expiryYear;
    }
    
    public int getExpiryYear() {
        return this.expiryYear;
    }    
    
    public Payment()
    {
    }

    public OrderFailureReason validate()
    {
        // check card number
        if(this.cardNumber.length() != 13 && this.cardNumber.length() != 16)
            return OrderFailureReason.CardNumberInvalid;

        if(!Luhn.Check(this.cardNumber))
            return OrderFailureReason.CardNumberInvalid;
            
        //check ccv
        if(ccv.length() != 3)
            return OrderFailureReason.InvalidCCV;
        
            
        if(!ccv.matches("^[0-9]{3}$"))
            return OrderFailureReason.InvalidCCV;
 
        Date date = new Date(); // today
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH) + 1; // Month here is 0 based, our cc is 1 based
        
        if(this.expiryYear < year)
        {
            return OrderFailureReason.CardExpired;
        }
        else if(this.expiryYear == year)
        {
            if(this.expiryMonth < month)
            {
                return OrderFailureReason.CardExpired;
            }
        }
        
        return OrderFailureReason.None;
    }
}
