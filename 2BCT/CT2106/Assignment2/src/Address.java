
/**
 * Holds address details
 *
 * @author Alan Flaherty
 * @version 13 October 2017
 */
public class Address
{
    private String line1;
    private String line2;
    private String line3;
    private String county;
    private String country;
    
    public Address()
    {
    }
    
    public void setLine1(String val)
    {
        this.line1 = val;
    }
    
    public String getLine1()
    {
        return this.line1;
    }
    
    public void setLine2(String val)
    {
        this.line2 = val;
    }
    
    public String getLine2()
    {
        return this.line2;
    }
    
    public void setLine3(String val)
    {
        this.line3 = val;
    }
    
    public String getLine3()
    {
        return this.line3;
    }

    public void setCounty(String val)
    {
        this.county = val;
    }
    
    public String getCounty()
    {
        return this.county;
    }
    
    public void setCountry(String val)
    {
        this.country = val;
    }
    
    public String getCountry()
    {
        return this.country;
    }
}
