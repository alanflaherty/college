
/**
 * Enumeration class OrderFailureReason - Reasons that an order failed.
 *
 * @author Alan Flaherty
 * @version 13 October 2017
 */
public enum OrderFailureReason
{
    None, CardNumberInvalid, CardExpired, InvalidCCV
}
