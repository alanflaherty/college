# CT2106 Object-Oriented Programming
Object-orientation is an underlying paradigm that shapes our whole way of thinking about how to map a problem onto an algorithmic model. It determines in fundamental ways the structure of even the most simple programs. It cannot be “added on” to other language constructs; rather it replaces the fundamental structure of procedural programming (which you learned with the C language).

