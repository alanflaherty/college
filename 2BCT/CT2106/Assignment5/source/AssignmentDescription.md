The Java code below illustrates how a Drawing object may be composed of another Drawing object and Shape objects such as a Circle.

a) Using the Composite design pattern as your guide, write the remaining code that would be required to make this code run to produce the output shown below. 

b) Write the setColour method so that it outputs an error message if it receives a parameter value other than “Green”, “White” or “Gold”. The method should not be case-sensitive either, so “green”, “white” or “gold” should be acceptable. 

c) After this Thursday/Friday's lecture you will be in position to revise the setColour method so that it throws an Exception rather than printing out an error message. Revise your first version of the setColour method so that it throws an Exception rather than printing out an error message.

d) Revise the code shown below to show how you would handle the exception thrown by the setColour method

Parts a) and b) will require the most work in this assignment and can be tackled immediately.

Parts c) and d) require  a refactoring of any code you produce for a) and b) - based on techniques introduced in this Thursday and Friday's lectures. 

How to approach this assignment

Approach this assignment using the incremental code and compile approach I have illustrated in the Folder/File example (and in the Shopping Cart example);

Create a test class in which the main method will run. You can create a method for the code below and call that method from main, or you can run the code from main
Add a line of code - write the classes/methods to get this line of code to compile. Continue doing this until you reach the final line of code below. Your code should compile. You now have the skeleton to your solution
Refactor the code to achieve the full functionality of the assignment. The assignment suggests using the composite design pattern. Does your code observe this pattern? If not, refactor it so that it does. If you are  not sure what the pattern entails, go back over the lecture notes. The pattern was used to solve the Assembly/Part and Folder/File examples
Does your code produce the output required? If not, use the debugger to step through the program execution to see why not. Diagnose the problem - and refactor your code so that the correct output is produced.
Finally, if another shape is added, to any of the Drawing objects, will the code still work. It should do, though the output will be different.
Question 3 code snippet

Output: 

q3 code output

Submission:

Please note that submission that do not follow the submission guidelines exactly will not be graded.

A jar file created from your BlueJ project. The jar file must contain the source code. Anything other than a jar file not be graded.
A Word (not odf or rtf or text or any other format)  or a PDF document
It should show the output of your program
It should describe the working of your program. It it doesn't work or doesn't work properly, you should describe the issues here
