# Online SVG Editor
https://www.w3schools.com/graphics/tryit.asp?filename=trysvg_circle

# Sample
```
<!DOCTYPE html>
<html>
<body>

<svg height="100" width="100">
  <circle cx="50" cy="50" r="40" stroke="black" stroke-width="3" fill="red" />
  <polygon points="5,77 50,0 95,77" stroke="black" stroke-width="3" fill="red" />


  Sorry, your browser does not support inline SVG.  
</svg> 
 
</body>
</html>
```

# Test
<svg height="100" width="100">
  <circle cx="50" cy="50" r="40" stroke="black" stroke-width="3" fill="red" />
  <polygon points="5,77 50,0 95,77" stroke="black" stroke-width="3" fill="red" />
</svg>

data:image/svg+xml;utf8,<svg height="100" width="100"><circle cx="50" cy="50" r="40" stroke="black" stroke-width="3" fill="red" /><polygon points="5,77 50,0 95,77" stroke="black" stroke-width="3" fill="red" /></svg>

# Sample with viewbox, 
data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 100'%3E%3Ccircle cx='50' cy='50' r='40' stroke='black' stroke-width='3' fill='red' /%3E%3Cpolygon points='5,77 50,0 95,77' stroke='black' stroke-width='3' fill='red' /%3E%3C/svg%3E


data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 110 110'%3E%3Ccircle cx='60' cy='50' r='40' stroke='black' stroke-width='3' fill='red' /%3E%3Cpolygon points='15,77 60,0 105,77' stroke='black' stroke-width='3' fill='red' /%3E%3C/svg%3E

# best but using negative indexes, rework
data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' viewBox='-5 -5 100 100'%3E%3Ccircle cx='50' cy='50' r='40' stroke='black' stroke-width='3' fill='red' /%3E%3Cpolygon points='5,77 50,0 95,77' stroke='black' stroke-width='3' fill='red' /%3E%3C/svg%3E

# Expected Result from assignment
data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' viewBox='-5 -5 100 100' style='background: green'><circle cx='50' cy='50' r='40' stroke='black' stroke-width='1' fill='white' /><polygon points='5,77 50,0 95,77' stroke='black' stroke-width='1' fill='gold' /></svg> 

# base	64
data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHZpZXdCb3g9Jy01IC01IDEwMCAxMDAnIHN0eWxlPSdiYWNrZ3JvdW5kOiBncmVlbic+PGNpcmNsZSBjeD0nNTAnIGN5PSc1MCcgcj0nNDAnIHN0cm9rZT0nYmxhY2snIHN0cm9rZS13aWR0aD0nMScgZmlsbD0nd2hpdGUnIC8+PHBvbHlnb24gcG9pbnRzPSc1LDc3IDUwLDAgOTUsNzcnIHN0cm9rZT0nYmxhY2snIHN0cm9rZS13aWR0aD0nMScgZmlsbD0nZ29sZCcgLz48L3N2Zz4=


# TODO
Use trig to place items.. 

https://codepen.io/tigt/pen/wavYWE
https://codepen.io/tigt/post/optimizing-svgs-in-data-uris


using: https://codepen.io/jakob-e/pen/doMoML
base64: https://dopiaza.org/tools/datauri/index.php


Got:

data:image/svg+xml,%3Csvg xmlns='http://www.w3.org/2000/svg' height='100' width='100'%3E%3Ccircle cx='50' cy='50' r='40' stroke='black' stroke-width='3' fill='red' /%3E%3Cpolygon points='5,77 50,0 95,77' stroke='black' stroke-width='3' fill='red' /%3E%3C/svg%3E

actual:
data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' height='100' width='100'><circle cx='50' cy='50' r='40' stroke='black' stroke-width='3' fill='red' /><polygon points='5,77 50,0 95,77' stroke='black' stroke-width='3' fill='red' /></svg>

Find way to avoid using specific widths and heights 

Sample:
data:image/svg+xml,%3csvg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 100'%3e%3cpath d='M10,10 H90 L50,70'/%3e%3ctext y='90'%3e' %26apos; %23 %25 %26amp; %c2%bf %f0%9f%94%a3%3c/text%3e%3c/svg%3e

actual:
data:image/svg+xml,<svg xmlns='http://www.w3.org/2000/svg' viewBox='0 0 100 100'><path d='M10,10 H90 L50,70'/><text y='90'>' %26apos; %23 %25 %26amp; %C2%BF %F0%9F%94%A3</text></svg>






data:image/svg+xml;utf8,<svg%20height%3D"100"%20width%3D"100"><circle%20cx%3D"50"%20cy%3D"50"%20r%3D"40"%20stroke%3D"black"%20stroke-width%3D"3"%20fill%3D"red"%20/><polygon%20points%3D"5%2C77%2050%2C0%2095%2C77"%20stroke%3D"black"%20stroke-width%3D"3"%20fill%3D"red"%20/></svg>


# Url format
data:image/svg+xml;utf8,<svg ... > ... </svg>
data:image/svg+xml;base64,<svg ... > ... </svg>



sample:

data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHZpZXdCb3g9IjAgMCA1MTIgNTEyIj48cGF0aCBkPSJNMjI0IDM4Ny44MTRWNTEyTDMyIDMyMGwxOTItMTkydjEyNi45MTJDNDQ3LjM3NSAyNjAuMTUyIDQzNy43OTQgMTAzLjAxNiAzODAuOTMgMCA1MjEuMjg3IDE1MS43MDcgNDkxLjQ4IDM5NC43ODUgMjI0IDM4Ny44MTR6Ii8+PC9zdmc+

