
| Module     |CT2106       |
|------------|-------------|
| Student    |Alan Flaherty|
| Student ID |14104205     |
| Assignment |Assignment 5 |
| Due Date   |21/11/2017   |

# Assignment 5
This is the jar file submission for CT2106 assignment5. Included are the instructions to run the jar file itself and
separate instructions to run the integrated JUnit UnitTests. There is an 'AllTests' TestSuite that includes each test
class separately. This jar file contains original source java files and
compiled java bytecode.

## Run the jar file
```
$ java -jar Assignment5.jar
```

## Run included UnitTests
```
$ java -cp ./Assignment5.jar org.junit.runner.JUnitCore org.alanflaherty.CT2106.assignment5.test.AllTests
```