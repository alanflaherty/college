package org.alanflaherty.CT2106.assignment5.test;

import org.alanflaherty.CT2106.assignment5.InvalidColorException;
import org.alanflaherty.CT2106.assignment5.Shape;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import static org.hamcrest.core.StringStartsWith.startsWith;

/**
 * @author Alan Flaherty
 * @since  20/11/17
 *
 * The ShapeTest class tests setting the color in the Shape class and the use of
 * exceptions when an invalid color parameter is supplied.
 */
public class ShapeTest {

    private class TestShape extends Shape {
        public TestShape()
        {

        }

        @Override
        public String toSVG() {
            return null;
        }
    }

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    TestShape shape = null;
    @Before
    public void setup(){
        shape = new TestShape();
    }

    @Test
    public void test_setcolor_correctly_sets_color_when_supplied_a_lowercase_color_string(){
        shape.setColor("green");
        assertEquals("green", shape.getColor());
    }

    @Test
    public void test_setcolor_correctly_sets_color_when_supplied_an_uppercase_color_string(){
        shape.setColor("GREEN");
        assertEquals("GREEN", shape.getColor());
    }

    @Test
    public void invalidColorException_thrown_when_a_color_other_than_green_white_or_gold_is_used_as_a_parameter() {
        thrown.expect(InvalidColorException.class);
        thrown.expectMessage(startsWith("Pink is not a valid color."));

        shape.setColor("Pink");
    }
}
