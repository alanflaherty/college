package org.alanflaherty.CT2106.assignment5.test;

import org.alanflaherty.CT2106.assignment5.Circle;
import org.alanflaherty.CT2106.assignment5.Drawing;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import org.junit.rules.ExpectedException;


/**
 * @author Alan Flaherty
 * @since  20/11/17
 *
 * The DrawingTest class tests the interaction between the @{@link Drawing} class
 * and its interaction with sub components
 */
public class DrawingTest {
    @Rule
    public ExpectedException thrown = ExpectedException.none();

    private Drawing drawing1 = null;
    private Drawing drawing2 = null;
    private Drawing drawing3 = null;
    private Circle circle = null;

    @Before
    public void setup(){
        drawing1 = new Drawing();
        drawing2 = new Drawing();
        drawing3 = new Drawing();
        circle = new Circle();

        drawing3.add(circle);

        drawing1.add(drawing2);
        drawing1.add(drawing3);
    }

    @Test
    public void test_setting_drawing_color_sets_all_child_components_color(){
        drawing1.setColor("Green");

        assertEquals("Green", drawing2.getColor());
        assertEquals("Green", drawing3.getColor());
        assertEquals("Green", circle.getColor());
    }

    @Test
    public void setting_subcomponent_color_sets_child_component_color(){
        drawing1.setColor("Green");
        drawing3.setColor("White");

        assertEquals("Green", drawing2.getColor());
        assertEquals("White", drawing3.getColor());
        assertEquals("White", circle.getColor());
    }
}
