package org.alanflaherty.CT2106.assignment5.test;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

/**
 * This is the TestSuite class to allow running all test classes from the
 * command line.
 */

@RunWith(Suite.class)
@SuiteClasses({
        DrawingTest.class,
        ShapeTest.class,
        SVGTests.class
})public class AllTests
{
 /* empty class */
}