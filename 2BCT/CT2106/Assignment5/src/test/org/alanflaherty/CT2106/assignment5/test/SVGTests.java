package org.alanflaherty.CT2106.assignment5.test;

import org.alanflaherty.CT2106.assignment5.Circle;
import org.alanflaherty.CT2106.assignment5.Drawing;
import org.alanflaherty.CT2106.assignment5.Triangle;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

/**
 * @author Alan Flaherty
 * @since  20/11/17
 *
 * The SVGTests class tests the inherited toSVG method from the shape class in
 * the @{@link Drawing} class and the @{@link Circle} and  @{@link Triangle}
 * classes. This method creates an SVG {@link <a href="https://www.w3.org/TR/SVG11/">HTTP/1.1 documentation</a>}.
 * document using the given drawing class as a root node for the document.
 */
public class SVGTests {

    private Drawing drawing = null;
    private Circle circle = null;
    private Triangle triangle = null;

    @Before
    public void setup() {
        drawing = new Drawing();
        drawing.setColor("Green");
    }

    @Test
    public void test_drawing_outputs_svg_element() {
        String svg = drawing.toSVG();

        //  style='background: Green' ,looks better with sample code without setting background color on document
        assertEquals("<svg xmlns='http://www.w3.org/2000/svg' viewBox='-5 -5 100 100'></svg>", svg);
    }

    @Test
    public void test_circle_outputs_circle_svg_element() {
        circle = new Circle();
        circle.setColor("White");
        drawing.add(circle);

        String svg = circle.toSVG();
        assertEquals("<circle cx='50' cy='50' r='40' stroke='black' stroke-width='1' fill='White' />", svg);
    }

    @Test
    public void test_triangle_outputs_correct_polygon_element() {
        triangle = new Triangle();
        triangle.setColor("Gold");

        drawing.add(triangle);

        String svg = triangle.toSVG();
        assertEquals("<polygon points='5,77 50,0 95,77' stroke='black' stroke-width='1' fill='Gold' />", svg);
    }

    @Test
    public void test_svg_element_outputs_browser_svg_string() {
        // add circle
        circle = new Circle();
        circle.setColor("Green");
        drawing.add(circle);

        //add triangle on top of circle
        triangle = new Triangle();
        triangle.setColor("Gold");
        drawing.add(triangle);

        // get data:image string for browser
        String base64ImageString = drawing.toBrowserImageString();

        assertEquals("data:image/svg+xml;base64,PHN2ZyB4bWxucz0naHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmcnIHZpZXdCb3g9Jy01IC01IDEwMCAxMDAnPjxjaXJjbGUgY3g9JzUwJyBjeT0nNTAnIHI9JzQwJyBzdHJva2U9J2JsYWNrJyBzdHJva2Utd2lkdGg9JzEnIGZpbGw9J0dyZWVuJyAvPjxwb2x5Z29uIHBvaW50cz0nNSw3NyA1MCwwIDk1LDc3JyBzdHJva2U9J2JsYWNrJyBzdHJva2Utd2lkdGg9JzEnIGZpbGw9J0dvbGQnIC8+PC9zdmc+", base64ImageString);
    }
}
