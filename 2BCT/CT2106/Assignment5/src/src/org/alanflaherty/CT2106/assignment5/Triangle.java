package org.alanflaherty.CT2106.assignment5;

/**
 * @author Alan Flaherty
 * @since  20/11/17
 *
 * The Triangle class represents a simple triangle in a drawing. It generates the
 * SVG for a circle using the Circles color as a background color.
 */
public class Triangle extends Shape {
    @Override
    public String toSVG() {
        return String.format("<polygon points='5,77 50,0 95,77' stroke='black' stroke-width='1' fill='%s' />", getColor());
    }
}
