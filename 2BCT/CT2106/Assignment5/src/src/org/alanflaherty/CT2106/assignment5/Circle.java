package org.alanflaherty.CT2106.assignment5;

/**
 * @author Alan Flaherty
 * @since  20/11/17
 *
 * The Circle class represents a simple circle in a drawing. It generates the
 * SVG for a circle using the Circles color as a background color.
 */
public class Circle extends Shape {
    @Override
    public String toSVG() {
        return String.format("<circle cx='50' cy='50' r='40' stroke='black' stroke-width='1' fill='%s' />", getColor());
    }
}
