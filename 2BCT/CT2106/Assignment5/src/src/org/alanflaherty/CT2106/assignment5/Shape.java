package org.alanflaherty.CT2106.assignment5;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author Alan Flaherty
 * @since  20/11/17
 *
 * The Shape class is an abstract base class for all the Drawing related classes to derive from.
 * It implements most of the functionality required for the application, managing the color for
 * the derived shape and any sub-components. Derived classes implement the toSVG() method.
 */
public abstract class Shape {

    public static final List VALID_COLORS = Collections.unmodifiableList(
            Arrays.asList(new String[] {"green", "white", "gold"}));

    protected ArrayList<Shape> shapes;
    private String color;

    public Shape() {
        shapes = new ArrayList<Shape>();
    }

    public void add(Shape shape) {
        shapes.add(shape);
    }

    public void setColor(String color) {
        if(VALID_COLORS.indexOf(color.toLowerCase()) == -1 )
        {
            throw new InvalidColorException(
                String.format("%s is not a valid color. Expected one of %s.", color, String.join(", ",VALID_COLORS))
                    );
        }
        for(Shape shape: shapes) {
            shape.setColor(color);
        }
        this.color = color;
    }

    public String getColor() {
        return this.color;
    }

    public void print() {
        for(Shape shape: shapes) {
            shape.print();
        }

        if(!(this instanceof Drawing)) {
            System.out.format("Drawing %s with color %s\n", this.getClass().getSimpleName(), this.color);
        }
    }

    public abstract String toSVG();
}
