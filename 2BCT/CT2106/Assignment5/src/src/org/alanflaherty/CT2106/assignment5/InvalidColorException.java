package org.alanflaherty.CT2106.assignment5;

/**
 * @author Alan Flaherty
 * @since  20/11/17
 *
 * The InvalidColorException inherits the IllegalArgumentException and is used when a color parameter
 * supplied is outside the range of values permitted.
 */
public class InvalidColorException extends IllegalArgumentException {
    public InvalidColorException(String message)
    {
        super(message);
    }
}
