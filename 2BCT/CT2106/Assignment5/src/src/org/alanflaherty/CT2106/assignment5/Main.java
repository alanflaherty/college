package org.alanflaherty.CT2106.assignment5;

/**
 * @author Alan Flaherty
 * @since  20/11/17
 *
 * The main class is the entry point for the application and it creates the Drawing components
 * as specified in the Assignment brief, it also outputs the base64 encoded SVG data URI that
 * will display in all modern browsers.
 */
public class Main {

    public static void main(String[] args) {
	    // write your code here
        Shape tri1 = new Triangle();
        Shape tri2 = new Triangle();
        Shape cir1 = new Circle();

        Drawing drawing1 = new Drawing();
        Drawing drawing2 = new Drawing();
        Drawing drawing3 = new Drawing();

        drawing3.add(tri2);
        drawing2.add(tri1);
        drawing2.add(drawing3);

        drawing1.add(cir1);
        drawing1.add(drawing2);

        System.out.format("\n[Main]\n");
        // Catch exception when using invalid Color.
        try {
            drawing1.setColor("Red");
        }
        catch(InvalidColorException ex){
            System.out.format("Caught exception while trying to set the color of drawing1 to Red\n    Exception Message: '%s'\n", ex.getMessage());
        }

        drawing1.setColor("Green");
        drawing2.setColor("White");
        drawing3.setColor("Gold");

        drawing1.print();

        System.out.format("\n[SVG data URI]\n");
        System.out.format("Put this string into a browser to view image\n\n%s\n\n", drawing1.toBrowserImageString());
    }

    /*
    [Original Output]
    Drawing Circle with color Green
    Drawing Triangle with color White
    Drawing Triangle with color Gold

    [Updated Output]
    Caught exception while trying to set the color of drawing1 to Red
        Exception Message: 'Red is not a valid color. Expected one of green, white, gold.'
    Drawing Circle with color Green
    Drawing Triangle with color White
    Drawing Triangle with color Gold
     */
}
