package org.alanflaherty.CT2106.assignment5;

import java.util.Base64;

/**
 * @author Alan Flaherty
 * @since  20/11/17
 *
 * The Drawing class manages adding the svg for the drawing sub-components into the svg markup.
 * It also creates the data:image base64 encoded string that represents the drawing that will
 * render in the browser.
 */
public class Drawing extends Shape{

    private String toSVG(Boolean sourceElement) {
        String svg = "";

        // if this is the root drawing add the svg document start element
        if(sourceElement) {
            // not setting the background color on the document looks better with the sample code provided, so it is
            // always white
//            svg+= String.format("<svg xmlns='http://www.w3.org/2000/svg' viewBox='-5 -5 100 100' style='background: %s'>", getColor());
            svg+= "<svg xmlns='http://www.w3.org/2000/svg' viewBox='-5 -5 100 100'>";
        }
        // should build string
        for(Shape shape: this.shapes) {
            if(shape instanceof Drawing)
            {
                svg += ((Drawing)shape).toSVG(false);
            }
            else
            {
                svg += shape.toSVG();
            }
        }

        // if this is the root drawing add the svg document end element
        if(sourceElement) {
            svg+= "</svg>";
        }
        return svg ;
    }

    @Override
    public String toSVG() {
        return this.toSVG(true);
    }

    public String toBrowserImageString()
    {
        String svg = this.toSVG();
        byte[] bytesEncoded = Base64.getEncoder().encode(svg.getBytes());

        return String.format("data:image/svg+xml;base64,%s", new String(bytesEncoded) );
    }
}
