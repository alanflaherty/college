# CT230 Database Systems I

Indexing Techniques: Primary, Secondary, Clustering, B Trees, Hashing (Extendible, Dynamic, Linear). Database Architectures and Data Models: Network, Hierarchical, Relational, Object-Oriented. Relational Model: Relations, Relational operators, Integrity constraints. Relational Algebra and SQL: Relational operators, Query Optimisation, DDL, DML, DCL. Extended Relational Model.


