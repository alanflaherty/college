CREATE TABLE staff(
	ID integer NOT NULL PRIMARY KEY,
	fname varchar(20),
	lname varchar(20),
	role varchar(50)
);

INSERT INTO staff VALUES
(1,'Owen',	'Smyth',	'Dentist'),
(2,'Alastair',	'Wood',		'Dentist'),
(3,'Yolande',	'Keane',	'Hygienist'),
(4,'Aoife',	'Burke',	'Hygienist'),
(5,'Linda',	'Hughes',	'Dental Nurse'),
(6,'Sharon',	'Casey',	'Dental Nurse'),
(7,'Gillian',	'Forde',	'Dental Nurse'),
(8,'Eugenie',	'McCarthy',	'Dental Nurse'),
(9,'Siobhan',	'Gavin',	'Reception'),
(10,'Orla',	'Connolly',	'Manager');

CREATE TABLE patient(
	pID integer NOT NULL PRIMARY KEY,
	pFname varchar(20),
	pLname varchar(20),
	bDate date,
	address varchar(60),
	phone integer
);

INSERT INTO patient VALUES
(123,	'Sorcha',	'Doyle',	'1971/01/09',	'13 Sea Road, Galway', 867297291),
(124,	'Maya',		'Madi',		'1999/03/03',	'45 College Road, Galway',		864545454),
(125,	'Cliona',	'Finnerty',	'2000/01/20',	'Oranmore Maree, Galway',		872323232),
(126,	'James',	'O''Toole',	'1985/11/11',	'22 Highfield, Newcastle, Galway',	869871230),
(127,	'Tanya',	'Ratkai',	'2006/05/03',	'3 Ocean View, Salthill, Galway',	864532123),
(128,	'Sana',		'Hussain',	'2004/03/31',	'34 Newcastle Park, Galway',		867654321),
(321,	'Jennifer',	'Walsh',	'1985/06/24',	'42 Sea View, Spiddal, Galway',		861231231),
(322,	'Sadhbh',	'Walsh',	'2006/10/14',	'42 Sea View, Spiddal, Galway',		861231231),
(323,	'Tadhgh',	'Walsh',	'2010/11/21',	'42 Sea View, Spiddal, Galway',		861231231),
(444,	'Owen',		'Smyth',	'1990/02/27',	'31 Newcastle Gardens, Newcastle, Galway',	876665556),
(541,	'Sean',		'Bohan',	'1985/08/31',	'33 Rockbarton Road, Galway',	876665551);


CREATE TABLE procedures(
	procNum integer NOT NULL PRIMARY KEY,
	pName varchar(60),
	price decimal(10,2)
);

INSERT INTO procedures VALUES
(25, 'Local Anesthesia', 20.00),
(26, 'General Anesthesia', 100.00),
(27, 'X-Ray', 70.00),
(32, 'Check-Up', 50.00),
(33, 'Clean', 40.00),
(34, 'Filling', 55.00),
(35, 'Extraction', 100.00),
(36, 'Repair', 55.00),
(37, 'Root canal', 200.00),
(38, 'Dental Crowns', 300.00),
(39, 'Whitening', 20.00),
(40, 'Veneer', 100.00),
(41, 'Dentures', 500.00),
(42, 'Bridge', 150.00);


CREATE TABLE appointment(
	aptID integer NOT NULL,
	patientID integer,
	staffID integer,
	aptDate date,
	aptTime time,
	CONSTRAINT pk_appointment PRIMARY KEY(aptID),
	CONSTRAINT fk_patientID_pID  FOREIGN KEY (patientID) REFERENCES patient (pID),
	CONSTRAINT fk_staffID_ID FOREIGN KEY (staffID) REFERENCES staff(ID)
);

INSERT INTO appointment VALUES
(1, 123, 1, '2017/12/04', '09:30:00'),
(2, 321, 3, '2017/12/01', '10:00:00'),
(3, 322, 3, '2017/12/01', '10:15:00'),
(4, 323, 4, '2017/12/01', '10:15:00'),
(5, 444, 2, '2017/12/05', '17:00:00'),
(6, 124, 3, '2017/12/06', '09:30:00'),
(7, 128, 1, '2017/12/04', '10:00:00'),
(8, 125, 1, '2017/12/04', '10:30:00'),
(9, 123, 1, '2017/12/11', '09:30:00');


