CREATE TABLE aptDetails(
	aptID integer NOT NULL,
	procNum integer NOT NULL,

	CONSTRAINT pk_aptDetail PRIMARY KEY(aptID,procNum),
	CONSTRAINT fk_appointment_aptID FOREIGN KEY (aptID) REFERENCES appointment (aptID),
	CONSTRAINT fk_procedure_procNum FOREIGN KEY (procNum) REFERENCES procedures (procNum)
);

INSERT INTO aptDetails VALUES
(1,32),
(1,39),
(2,33),
(2,39),
(3,33),
(4,33),
(5,25),
(5,33),
(5,34),
(5,36),
(7, 32),
(7, 33),
(8, 32),
(9, 34);

