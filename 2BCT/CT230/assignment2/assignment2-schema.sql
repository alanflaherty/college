/*
    Looks like additional fk constraints could be added:
        1. timetable modulecode to module code
        2. studentReg sID to student id

    Check:
        1. How to prevent adding collaction, sets latin1_sweedish_ci as
           the collation with the current schema. 

	Use: 
	    ALTER TABLE tablename CHARACTER SET utf8 COLLATE utf8_general_ci;
        To set the table to use utf8 and to perform sorting and ordering in 
        that format.

	Remove??
*/

DROP TABLE IF EXISTS `timetable`;
DROP TABLE IF EXISTS `studentReg`;
DROP TABLE IF EXISTS `modLect`;

DROP TABLE IF EXISTS `venue`;
DROP TABLE IF EXISTS `student`;
DROP TABLE IF EXISTS `module`;

-- venue(vCode, vName, building, capacity, disAccess)
CREATE TABLE `venue` (
  `vCode` varchar(10) NOT NULL,
  `vName` varchar(200) NOT NULL,
  `building` varchar(200) NOT NULL,
  `capacity` int(10) NOT NULL,
  `disAccess` bit NOT NULL,

  PRIMARY KEY (`vCode`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- student(id, fname, surname, email, courseCode, currYear)
CREATE TABLE `student` (
  `id` bigint(20) NOT NULL,
  `fname` varchar(200) NOT NULL,
  `surname` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `courseCode` varchar(10) NOT NULL,
  `currYear` int(10) NOT NULL,

  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- module(code, modName, ECTS, deptName, examDuration, sem1Exam, sem2Exam)
CREATE TABLE `module` (
  `code` varchar(10) NOT NULL,
  `modName` varchar(100) NOT NULL,
  `ECTS` int(10) NOT NULL,
  `deptName` varchar(100) NOT NULL,
  `examDuration` int(10) NOT NULL,
  `sem1Exam` bit NOT NULL,
  `sem2Exam` bit NOT NULL,

  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- modLect(mCode,lecturer)
CREATE TABLE `modLect` (
  `mCode` varchar(10) NOT NULL,
  `lecturer` varchar(200) NOT NULL,

  PRIMARY KEY (`mCode`, `lecturer`),
  KEY `FK_modLect_module_code` (`mCode`),
  CONSTRAINT `FK_modLect_module_code` FOREIGN KEY (`mCode`) REFERENCES `module` (`code`)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- studentReg(sID, modCode)
CREATE TABLE `studentReg` (
  `sID` bigint(20) NOT NULL,
  `modCode` varchar(10) NOT NULL,

  PRIMARY KEY (`sID`, `modCode`),
  KEY `FK_studentReg_module_code` (`modCode`),
  CONSTRAINT `FK_studentReg_module_code` FOREIGN KEY (`modCode`) REFERENCES `module` (`code`)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


-- timetable(sessionID, sesDate, sesTime, venueCode, moduleCode)
CREATE TABLE `timetable` (
  `sessionID` int(10) NOT NULL,
  `sesDate` date NOT NULL,
  `sesTime` time NOT NULL,
  `venueCode` varchar(10) NOT NULL,
  `moduleCode` varchar(10) NOT NULL,

  PRIMARY KEY (`sessionID`),
  KEY `FK_venueCode_venue_vcode` (`venueCode`),
  CONSTRAINT `FK_venueCode_venue_vcode` FOREIGN KEY (`venueCode`) REFERENCES `venue` (`vCode`)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



