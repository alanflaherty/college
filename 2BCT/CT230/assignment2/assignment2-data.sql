-- venue
INSERT INTO venue(vCode, vName, building, capacity, disAccess) VALUES 
	('gb1', 'lettermore suite', 'galway bay hotel', 300, 1),
	('lland1', 'main hall', 'leisureland, salthill', 400, 1),
	('ba1', 'bailey allen hall', 'nuigalway', 200, 1),
	('cube', 'the cube', 'nuigalway', 50, 1),
	('kf1', 'kingfisher', 'nuigalway', 600, 1),	
	('AC105', 'Arts-Science Suite', 'Arts and Science Building, NUI Galway', 45, 0),
	('G0046', 'G0046 Engineering Building', 'Alice Perry Engineering Building', 80, 1), 
	('IT106', 'IT106', 'IT Building, NUI Galway', 67, 1);

-- student(id, fname, surname, email, courseCode, currYear)
INSERT INTO student(id, fname, surname, email, courseCode, currYear) VALUES
	(17123456, 'Donal', 'Nee', 'd.nee123@nuigalway.ie', 'GY101', 1),
	(17112233, 'Ann', 'Beirne', 'a.beirne122@nuigalway.ie', 'GY350', 1),
	(17333222, 'Sadhbh', 'O'' Malley', 's.omalley333@nuigalway.ie', 'GY350', 1),
	(15555666, 'Claire', 'Gavin', 'c.gavin555@nuigalway.ie', 'GY406', 3),
	(17654321, 'Sean', 'Lynch', 's.lynch654@nuigalway.ie', 'GY101', 1),
	(16667788, 'Jack', 'Carr', 'j.carr667@nuigalway.ie', 'GY101', 2),
	(16444455, 'Mark', 'Connor', 'm.connor444@nuigalway.ie', 'GY350', 2),
	(16987654, 'Marie', 'Fahy', 'm.fahy987@nuigalway.ie', 'GY101', 2),
	(16998877, 'Hugh', 'Flynn', 'h.flynn998@nuigalway.ie', 'GY350', 2);

-- module(code, modName, ECTS, deptName, examDuration, sem1Exam, sem2Exam)
INSERT INTO module(code, modName, ECTS, deptName, examDuration, sem1Exam, sem2Exam) VALUES 
	('CT2101', 'Object Oriented Programming 1', 5, 'Information Technology', 2, 1, 0),
	('CT2103', 'Systems Analysis and Design', 5, 'Information Technology', 2, 1, 0),
	('CT2106', 'Object Oriented Programming', 5, 'Information Technology', 2, 1, 0),
	('CT2109', 'OOP Data Structures and Algorithms', 5, 'Information Technology', 2, 0, 1),
	('CT230', 'Database Systems 1', 5, 'Information Technology', 2, 1, 0),
	('CT3531', 'Network and Data Communications 2', 5, 'Information Technology', 2, 1, 0),
	('CT1112', 'Professional Skills', 5, 'Information Technology', 0, 0, 0),
	('CT101', 'Copmuter Systems', 10, 'Information Technology', 2, 0, 1),
	('CT102', 'Algorithms and Information Systems', 10, 'Information Technology', 2, 0, 1),
	('CT103', 'Programming', 10, 'Information Technology', 2, 0, 1),
	('CT335', 'Object Oriented Programming', 5, 'Information Technology', 2, 1, 0),
	('CT1101', 'Programming I', 5, 'Information Technology', 2, 1, 0),
	('CT1100', 'Computer Systems', 5, 'Information Technology', 2, 1, 0),
	('CT1120', 'Algorithms', 5, 'Information Technology', 0, 0, 0),
	('CT874','Programming 1', 5, 'Information Technology', 2, 1, 0),
	('EC273', 'Mathematics for Economics', 5, 'Mathematics, Statistics & Applied Maths', 2, 1, 0),
	('MA190', 'Mathematics', 10, 'Mathematics, Statistics & Applied Maths', 2, 1, 1),
	('MA160', 'Mathematics', 10, 'Mathematics, Statistics & Applied Maths', 2, 1, 1),
	('ST237','Intro to Statistical Data and Probability', 5, 'Mathematics, Statistics & Applied Maths', 2, 1, 0),
	('MA204', 'Discrete Mathematics', 5, 'Mathematics, Statistics & Applied Maths', 2, 1, 0),
	('ST1100', 'Engineering Statistics', 5, 'Mathematics, Statistics & Applied Maths', 2, 0, 1);

-- modLect(mCode,lecturer)
INSERT INTO modLect(mCode,lecturer) VALUES 
	('CT2101', 'Seamus Hill'),
	('CT2103', 'Josephine Griffith'),
	('CT2103', 'Karen Young'),
	('CT2106', 'Conor Hayes'),
	('CT2109', 'Michael Schukat'),
	('CT3531', 'Des Chambers'),
	('CT230', 'Josephine Griffith'),
	('CT102', 'Josephine Griffith'),
	('CT102', 'Martina Fox'),
	('CT103', 'Owen Molloy'),
	('CT101', 'Frank Glavin'),
	('CT1112', 'Enda Barrett'),
	('CT1112', 'Martina Fox'),
	('CT335', 'Matthias Nickles'),
	('CT335', 'Seamus Hill'),
	('CT1101', 'Finlay Smith'),
	('CT1100', 'Frank Galvin'),
	('CT1120', 'Conn Mulvihill'),
	('MA204', 'Graham Ellis'),
	('MA190', 'Graham Ellis'),
	('MA204', 'Aisling McCluskey'), 
	('ST237', 'Cara Dooley'),
	('CT874', 'Seamus Hill'),
	('ST1100', 'John Hinde'),
	('ST1100', 'Cara Dooley');

-- studentReg(sID, modCode)
INSERT INTO studentReg(sID, modCode) VALUES (17123456, 'CT1101'),
	(17123456, 'CT1100'),
	(17123456, 'CT1120'),
	(17112233, 'CT102'),
	(17112233, 'CT103'),
	(17112233, 'CT101'),
	(17112233, 'CT1112'),
	(17333222, 'CT101'),
	(17333222, 'CT102'),
	(17333222, 'CT103'),
	(17333222, 'CT1112'),
	(15555666, 'CT230'),
	(15555666, 'CT3531'),
	(17654321, 'CT1101'),
	(17654321, 'CT1100'),
	(17654321, 'CT1120'),
	(16667788, 'CT230'),
	(16667788, 'CT2101'),
	(16667788, 'CT2103'),
	(16444455, 'CT230'),
	(16444455, 'CT2106'),
	(16444455, 'CT2109'),
	(16998877, 'CT230'),
	(16998877, 'CT2106'),
	(16998877, 'CT2109'),
	(16998877, 'ST237'),
	(16998877, 'MA204'),
	(16987654, 'CT230'),
	(16987654, 'CT2101'),
	(16987654, 'CT2103');

-- timetable(sessionID, sesDate, sesTime, venueCode, moduleCode)
INSERT INTO timetable(sessionID, sesDate, sesTime, venueCode, moduleCode) VALUES 
	(1, '2017/12/04', 0900, 'gb1', 'ct230'),
	(2, '2017/12/04', 0900, 'ac105', 'ct230'),
	(3, '2017/12/04', 0900, 'cube', 'ct230'),
	(4, '2017/12/04', 0900, 'kf1', 'ma160'),
	(5, '2017/12/04', 0900, 'ac105', 'ma160'),
	(6, '2017/12/04', 0900, 'cube', 'ma160'),
	(7, '2017/12/06', 1600, 'ba1', 'ct2103'),
	(8, '2017/12/06', 1600, 'cube', 'ct2103'),
	(9, '2017/12/06', 1600, 'ac105', 'ct2103'),
	(10, '2017/12/11', 0900, 'lland1', 'st237'),
	(11, '2017/12/11', 0900, 'it106', 'st237');




