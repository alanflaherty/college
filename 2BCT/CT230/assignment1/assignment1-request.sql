-- requestiD essn reqDate itemReq reason

DROP TABLE IF EXISTS `request`;

CREATE TABLE `request` (
  `requestID` bigint(20) NOT NULL,
  `essn` bigint(20) NOT NULL,
  `reqDate` date DEFAULT NULL,
  `itemReq` varchar(200) DEFAULT NULL,
  `reason` varchar(300) DEFAULT NULL,

  PRIMARY KEY (`requestID`),
  CONSTRAINT `FK_request_employee_ssn` FOREIGN KEY (`essn`) REFERENCES `employee` (`ssn`)
	ON UPDATE RESTRICT
	ON DELETE RESTRICT 
) ENGINE=InnoDB DEFAULT CHARSET=latin1;