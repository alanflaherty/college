-- Adminer 4.3.1 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `department`;
CREATE TABLE `department` (
  `dnumber` int(20) NOT NULL,
  `dname` varchar(50) DEFAULT NULL,
  `mgrssn` bigint(20) DEFAULT NULL,
  `mgrstatrtdate` date DEFAULT NULL,
  PRIMARY KEY (`dnumber`),
  KEY `mgrssn` (`mgrssn`),
  CONSTRAINT `department_ibfk_2` FOREIGN KEY (`mgrssn`) REFERENCES `employee` (`ssn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `department` (`dnumber`, `dname`, `mgrssn`, `mgrstatrtdate`) VALUES
(1,	'Headquarters',	888665555,	'1981-06-19'),
(4,	'Administration',	987654321,	'1995-01-01'),
(5,	'Research',	333445555,	'1988-05-22');

DROP TABLE IF EXISTS `dependent`;
CREATE TABLE `dependent` (
  `essn` bigint(20) NOT NULL,
  `dependent_name` varchar(50) NOT NULL,
  `sex` char(1) DEFAULT NULL,
  `bdate` date DEFAULT NULL,
  `relationship` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`essn`,`dependent_name`),
  CONSTRAINT `dependent_ibfk_1` FOREIGN KEY (`essn`) REFERENCES `employee` (`ssn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `dependent` (`essn`, `dependent_name`, `sex`, `bdate`, `relationship`) VALUES
(123456789,	'Alice',	'F',	'1988-12-30',	'Daughter'),
(123456789,	'Elizabeth',	'F',	'1967-05-05',	'Spouse'),
(123456789,	'Michael',	'M',	'1988-01-04',	'Son'),
(333445555,	'Alice',	'F',	'1986-04-05',	'Daughter'),
(333445555,	'Joy',	'F',	'1958-05-03',	'Spouse'),
(333445555,	'Theodore',	'M',	'1983-10-25',	'Son'),
(987654321,	'Abner',	'M',	'1942-02-28',	'Spouse');

DROP TABLE IF EXISTS `dept_locations`;
CREATE TABLE `dept_locations` (
  `dnumber` int(11) NOT NULL,
  `dlocation` varchar(20) NOT NULL,
  PRIMARY KEY (`dnumber`,`dlocation`),
  KEY `dnumber` (`dnumber`),
  CONSTRAINT `FK_dept_locations_dnumber` FOREIGN KEY (`dnumber`) REFERENCES `department` (`dnumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `dept_locations` (`dnumber`, `dlocation`) VALUES
(1,	'Houston'),
(4,	'Stafford'),
(5,	'Bellaire'),
(5,	'Houston'),
(5,	'Sugarland');

DROP TABLE IF EXISTS `employee`;
CREATE TABLE `employee` (
  `fname` varchar(50) DEFAULT NULL,
  `minit` varchar(1) DEFAULT NULL,
  `lname` varchar(50) DEFAULT NULL,
  `ssn` bigint(20) NOT NULL,
  `bdate` date DEFAULT NULL,
  `address` varchar(100) DEFAULT NULL,
  `sex` varchar(1) DEFAULT NULL,
  `salary` double DEFAULT NULL,
  `superssn` bigint(20) DEFAULT NULL,
  `dno` int(11) DEFAULT NULL,
  PRIMARY KEY (`ssn`),
  KEY `FK_employee_superssn` (`superssn`),
  KEY `FK_employee_department_dno` (`dno`),
  CONSTRAINT `FK_employee_department_dno` FOREIGN KEY (`dno`) REFERENCES `department` (`dnumber`),
  CONSTRAINT `FK_employee_superssn` FOREIGN KEY (`superssn`) REFERENCES `employee` (`ssn`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `employee` (`fname`, `minit`, `lname`, `ssn`, `bdate`, `address`, `sex`, `salary`, `superssn`, `dno`) VALUES
('John',	'B',	'Smith',	123456789,	'1965-01-09',	'731 Fondren, Houston, Tx',	'M',	30000,	333445555,	5),
('Franklin',	'T',	'Wong',	333445555,	'1955-12-08',	'638 Voss, Houston, TX',	'M',	30000,	888665555,	5),
('Joyce',	'A',	'English',	453453453,	'1972-07-31',	'5631 Rice, Houston, TX',	'F',	25000,	333445555,	5),
('Ramesh',	'K',	'Narayan',	666884444,	'1962-09-15',	'975 Fire Oak, Humble, TX',	'M',	38000,	333445555,	5),
('James',	'E',	'Borg',	888665555,	'1937-11-10',	'450 Stone, Houston, TX',	'M',	55000,	NULL,	1),
('Jennifer',	'S',	'Wallace',	987654321,	'1941-06-20',	'291 Berry, Bellaire, TX',	'F',	43000,	888665555,	4),
('Ahmad',	'V',	'Jabbar',	987987987,	'1969-03-29',	'980 Dallas, Houston, TX',	'M',	25000,	987654321,	4),
('Alicia',	'J',	'Zelaya',	999887777,	'1968-07-19',	'3321 Castle, Spring, TX',	'F',	25000,	987654321,	4);

DROP TABLE IF EXISTS `project`;
CREATE TABLE `project` (
  `pname` varchar(20) DEFAULT NULL,
  `pnumber` int(11) NOT NULL,
  `plocation` varchar(20) DEFAULT NULL,
  `dnum` int(11) DEFAULT NULL,
  PRIMARY KEY (`pnumber`),
  KEY `dnum` (`dnum`),
  CONSTRAINT `project_ibfk_1` FOREIGN KEY (`dnum`) REFERENCES `department` (`dnumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `project` (`pname`, `pnumber`, `plocation`, `dnum`) VALUES
('ProductX',	1,	'Bellaire',	5),
('ProductY',	2,	'Sugarland',	5),
('ProductZ',	3,	'Houston',	5),
('Computerization',	10,	'Stafford',	4),
('Reorganization',	20,	'Houston',	1),
('Newbenefits',	30,	'Stafford',	4);

DROP TABLE IF EXISTS `works_on`;
CREATE TABLE `works_on` (
  `essn` bigint(20) NOT NULL,
  `pno` int(11) NOT NULL,
  `hours` double DEFAULT NULL,
  PRIMARY KEY (`essn`,`pno`),
  KEY `pno` (`pno`),
  CONSTRAINT `FK1_works_on_essn` FOREIGN KEY (`essn`) REFERENCES `employee` (`ssn`),
  CONSTRAINT `works_on_ibfk_1` FOREIGN KEY (`pno`) REFERENCES `project` (`pnumber`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `works_on` (`essn`, `pno`, `hours`) VALUES
(123456789,	1,	32.5),
(123456789,	2,	7.5),
(123456789,	3,	3),
(333445555,	2,	10),
(333445555,	3,	10),
(333445555,	10,	10),
(333445555,	20,	10),
(453453453,	1,	20),
(453453453,	2,	20),
(666884444,	3,	40),
(888665555,	20,	0),
(987654321,	20,	15),
(987654321,	30,	20),
(987987987,	10,	35),
(987987987,	30,	5),
(999887777,	30,	30);

-- 2017-09-05 11:24:21
