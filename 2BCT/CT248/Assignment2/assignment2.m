x = -10:.01:10;
a = 3;
b = 2;
c = 5;

[y, min_loc, max_loc] = f_quad(x, a, b, c);

plot(x,y);

% plot (*, <-- causes issues in octave , Do these show the minima and maxima
%hold on
%plot(*, *, '-s');

% hold on
% plot(x, y,'-p')