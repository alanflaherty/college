function [y, min_loc, max_loc] = f_quad(x, a, b, c)
    y = a * (x.^2) + b * x + c;
    min_loc = min(y);
    max_loc = max(y);
