format short G;
state  = [ 1345402, 41654, 136856, 179048 ];

initialYear = 2016;
finalYear = 2030;

Results = [initialYear state sum(state)];

lm = [ 
	0.95 0.03 0.05 0.05;
	0.02 0.90 0.03 0.02;
	0.01 0.04 0.90 0.02;
	0.02 0.03 0.02 0.91
]';

f = @(s) s * lm;
for t = initialYear:(finalYear-1)
  state = f(state);
  Results = [Results; t+1 state sum(state)];
end

disp(Results);
