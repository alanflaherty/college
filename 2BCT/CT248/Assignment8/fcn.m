function y = fcn(u, t)
    time = int32(floor(t)) + 1;
    y = u(time)==1;
end