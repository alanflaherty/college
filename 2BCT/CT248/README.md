# CT248 Introduction to Modelling

Introduction to Matlab: Data input & output, Manipulating Matrices, Data Visualisation, Programming constructs, Matlab functions and scripts, Introduction to Matlab OO classes. Introduction to Simulink, Basic Model Design & Implementation, Modelling Dynamic Control Systems, Strong emphasis on Energy Systems Case Studies both in lectures and associated labwork.
