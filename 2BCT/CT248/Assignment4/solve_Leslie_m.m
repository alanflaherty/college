function [Results, Growth_estimates] = solve_Leslie_m(L, X, n)

  Results = [];
  lastSum = -intmax;
  Growth_estimates= [];
  
  for t = 1:n
    X = L * X;
    
    sumOfStates = sum(X);
    Results = [Results; t X' sumOfStates];
    
    growth = 0;
    if lastSum == -intmaxX
      growth = 0;
    else
      growth = (sumOfStates - lastSum)/lastSum ;
    endif
    Growth_estimates = [Growth_estimates; growth];
    
    lastSum = sumOfStates;
  end
endfunction