L = [0    0    12  9;
     1/3  0    0   0;
     0    1/2  0   0;
     0    0    1/2 0];

X = [0; 0; 10; 0]; % initial conditions
 
n = 10;

[results, growth] = solve_Leslie_m(L, X, n);

format short g;

disp("results:");
disp(results);

disp("growth:");
disp(growth');