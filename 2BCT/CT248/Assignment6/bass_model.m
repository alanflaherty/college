function [y] = bass_model(t, x, c, i)
  A	=	1;      % adopters
  P	=	9999;   % potential adopters
  N = A + P;  % population

  f = @(v)[((c*i)/N) * v * (N-v)];

  a = f(x(1));
  b = -f(x(2));
  y = [a b];
end