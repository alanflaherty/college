C = 4;       % average contact rate
I = 0.25;    % Probability contact between A and P leads to an adoption

START = 0; 
END = 20;

yd =[];
Cn = 8;
for j = 1:Cn
  [t,y] = ode23(@bass_model, [START:.1:END], [1, 9999], odeset, j, I);
  yd = [yd; y'(1,:)];
end

plot(t, yd, '-o');