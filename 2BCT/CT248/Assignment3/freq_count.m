function [Numbers, Frequencies] = freq_count (v, algorithm = 'optimised_subunique')
  
  function [v_u] = basic_sub_unique(v)
    v_ui = [];
    for i = v
      if isempty(find(v_ui == i))
        v_ui = [v_ui i];      
      endif
    endfor
    v_u = sort(v_ui);
  endfunction
  
  function [v_u] = sub_unique(v)
    v = sort(v);
    v_ui = [];
    last_v = -intmax;
    
    for i = v
      if i != last_v
        v_ui = [v_ui i];
        last_v = i;
      endif
    endfor
    v_u = v_ui;
  endfunction
  
  % default_subunique optimised_subunique builtin_subunique 
  if strcmp(algorithm, 'default_subunique' )
    v_unique = basic_sub_unique(v);
  elseif strcmp(algorithm, 'optimised_subunique')
    v_unique = sub_unique(v);
  else
    v_unique =unique(v);
  endif 
  
  v_frequency = [];

  for i = v_unique
    v_frequency = [v_frequency length(find(v==i))];
  endfor
    
  Numbers = v_unique;
  Frequencies = v_frequency;
endfunction