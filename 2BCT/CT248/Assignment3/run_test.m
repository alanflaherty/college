  function [duration] = run_test(v, algorithm)
    tic
      [unique, freq] = freq_count(v, algorithm);
    duration = toc;
  endfunction