function  assignment3_test ()
  totalTime = tic;
  clc;
  
  iterations = 10;
  warmup  = 500;
  
  vec = randi([1,10],1, 100);
  
  for i=1:warmup
    run_test(vec, 'default_subunique');
    run_test(vec, 'optimised_subunique');
    run_test(vec, 'builtin_subunique');
  endfor
  
  v = [100 200 300 400 500 600 700 800 900 1000 1100 1200 1300 1400 1500 1600 1700 1800 1900 2000 3000 4000 5000 6000 7000 8000 9000 10000 100000];
  r = zeros(1, length(v));

  disp("\nCT248 - Introduction to modelling - Assignment 3: Frequency Analysis");
  disp("  Testing frequency analysis with vectors of specified lengths containing numbers between 1 and 10.\n");
  disp("This test function tests three seperate unique functions and displays therir results on a graph.");
  disp("  * A default simple implementation");
  disp("  * a slightly optimised version (single loop through a sorted array)");
  disp("  * the builtin unique function\n");
  disp("[test output]");
  
  fflush(stdout);

  for i = 1:length(v)
    results = zeros(1, iterations);
    count = v(1, i);
    
    for j = 1:iterations
      % create vector and pass it in to functions
      vec = randi([1,10],1, count);
      
      results(1, j) = run_test(vec, 'default_subunique');
      results(2, j) = run_test(vec, 'optimised_subunique');
      results(3, j) = run_test(vec, 'builtin_unique');
    endfor
    
    r(1,i) = mean(results(1));
    r(2,i) = mean(results(2));
    r(3,i) = mean(results(3));

    fprintf("* Testing %d numbers between 1 and 10, took an average of [%f, %f, %f] sec\n", count, r(1,i), r(2,i), r(3,i) );  
    fflush(stdout);
  endfor

  plot(v, r'(:,1), 'r');
  hold on
  plot(v, r'(:,2), 'b');
  plot(v, r'(:,3), 'g');

  legend('Default SubUnique', 'Optimised SubUnique', 'Builtin unique function');
  hold off
 
  disp("\n\n\n");
  toc(totalTime);
endfunction