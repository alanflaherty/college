# variables
# Scalar
str='A String'
str="A String"
num=100
hexNum=0x100
float=9.123124

# Vector

# row vector
r=[0 1 2 3 4 5]
r2=[1:10]
r3=[1:0.5:10]

# column vector
c=[0 1 2 3 4 5]'
c2=[1:10]'
c3=[1:0.5:10]'

# Vector based operations
vector/matrix . operator 
a .* b
a .+ b

The dot operator does a row by row and column by column operation. 

so 
    [1 2] .* [2 1] 
gives 
    [2 2]

and 
    |5 1| .* |1 2|
    |1 6|    |2 1|
gives
    |5 2|
    |2 6|

this also applies to .+ .- .^ .\ etc..

vector ++
x = [x new_row_data]
x = [x; new_column_data]

transpose operator '
x = [1 2 3]
x' = [1; 2; 3]

# Vector functions.. matlab, everything may be a vector..
# Logical functions
any(v)			returns the scalar 1 if any of the elements of v are non-zero
all(v)			returns the scalar 1 if all the elements of v are non-zero
exists('name')		returns true is a variable called 'name' exists in the workspace 
find(x) 		returns vector containing the subscripts of the non-zero elements of x
v>5			returns 1 in the the element position of v or m that are > 5, others are set to 0

# Logical operators
~	Not
&	And
| 	Or

# Matrix
A(:,n) is the nth column of matrix A.
A(m,:) is the mth row of matrix A.

# Control flow

if CONDITION
end

for var = SEQ
end

while CONDITION
end

# function definition
#function template
function [output, ...] = functionname(input1, input2, ...)
    output = ...
end


# anonymous function definition
f = @(input)[output columns];

# anonymous ode function definition
f = @(t,x, additionalinput1, ...) (Tenv - x) * k;

# builtin matlab functions
printf(template, ...)	Print optional arguments under the control of the template string
disp(value)		Note that the output from 'disp' always ends with a newline.
mod(X,Y)		Compute the modulo of X and Y.
randi([IMIN IMAX],M,N)	Return random integers in the range IMIN:IMAX as a matrix of MxN.

dec2bin(d)
bin2dec(d)
dec2bin(d,n)

# differential functions
[] = ode23 (@FUN, SLOT, INIT, [OPT], [PAR1, PAR2])
    This function file can be used to solve a set of non-stiff ordinary
    differential equations (non-stiff ODEs) or non-stiff differential
    algebraic equations (non-stiff DAEs) with the well known explicit
    Runge-Kutta method of order (2,3).

[] = ode45 (@FUN, SLOT, INIT, [OPT], [PAR1, PAR2])
    This function file can be used to solve a set of non-stiff ordinary
    differential equations (non-stiff ODEs) or non-stiff differential
    algebraic equations (non-stiff DAEs) with the well known explicit
    Runge-Kutta method of order (4,5).


# plotting functions
plot

hold on

legend




