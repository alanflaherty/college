# Source tutorial index page
http://thinkingeek.com/arm-assembler-raspberry-pi/

# Github source for tutorial
https://github.com/rofirrim/raspberry-pi-assembler/

# ARM Assembly using the Raspberry Pi
http://bob.cs.sonoma.edu/IntroCompOrg-RPi/intro-co-rpi.html

# Additional Material to Review
##  University of Cambridge, includes graphics
https://www.cl.cam.ac.uk/projects/raspberrypi/tutorials/os/

## !DaveSpace
http://www.davespace.co.uk/arm/introduction-to-arm/index.html

# To Read
https://github.com/yellowbyte/reverse-engineering-reference-manual

## ARM IRC Bot
https://github.com/wyc/armbot 

## Tove
http://www.toves.org/books/arm/
http://www.toves.org/books/armsub/

!Try get this running on qemu!
## Baremetal Pi OS
https://github.com/blueintegral/BareMetalPi/

## ARM Baremetal Qemu
https://balau82.wordpress.com/2010/02/14/simplest-bare-metal-program-for-arm/
https://balau82.wordpress.com/2010/02/28/hello-world-for-bare-metal-arm-using-qemu/

## ARM Linux Qemu
https://balau82.wordpress.com/2010/03/22/compiling-linux-kernel-for-qemu-arm-emulator/
https://balau82.wordpress.com/2012/03/31/compile-linux-kernel-3-2-for-arm-and-emulate-with-qemu/
https://balau82.wordpress.com/2010/03/27/busybox-for-arm-on-qemu/
https://gist.github.com/pdxjohnny/3de9a9bdd38cacf3ea394207762f1002

## Comparison of the two methods
http://sushihangover.github.io/arm-bare-metal-comparing-llvm-to-arm-gcc/

#
https://github.com/brianwiddas/pi-baremetal

## C++ os based on Bakin OS that runs on Qemu
https://code.google.com/archive/p/pios/

# Report
Should summarise the content of the course and keep as reference material

# Books
Modern Assembly Language Programming with the ARM Processor
https://books.google.ie/books?id=gks1CgAAQBAJ&lpg=PR17&ots=OQdSwJZV3R&dq=arm%20assembly&pg=PA157#v=onepage&q=arm%20assembly&f=false
ARM Assembly Language Fundamentals
https://books.google.ie/books?id=yhjYCwAAQBAJ&lpg=PA30&ots=qVPS_rCbXz&dq=arm%20assembly&pg=PA32#v=onepage&q=arm%20assembly&f=false


# ORGANISE THESE NOTES

https://github.com/brianwiddas/pi-baremetal
https://code.google.com/archive/p/pios/

http://wiki.osdev.org/Raspberry_Pi_Bare_Bones
http://wiki.osdev.org/Raspberry_Pi_Bare_Bones
https://www.raspberrypi.org/forums/viewtopic.php?f=72&t=11682#p226546
http://raspberrycompote.blogspot.ie/
https://github.com/raspberrypi/firmware/wiki/Mailbox-property-interface

http://wiki.osdev.org/Bare_Bones

# Assembly Language Tutorial
https://www.youtube.com/watch?v=ViNnfoE56V8


// raspi2 and raspi3 have peripheral base address 0x3F000000,
// but raspi1 has peripheral base address 0x20000000. Ensure
// you are using the correct peripheral address for your
// hardware.
