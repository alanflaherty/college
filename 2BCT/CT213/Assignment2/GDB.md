# GDB

# Starting
Starting a program called programname from its directory with gdb
```
gdb ./programname
(gdb) start
...
(gdb) disas
...
(gdb) info registers
...
(gdb) stepi
...
(gdb) break
```

# Layouts
Cycle through layouts
```
(gdb) layout next
```
Additional options: prev, src, regs, asm, split

Additional info:
[url:https://sourceware.org/gdb/onlinedocs/gdb/TUI-Commands.html]

# Connecting to a running application
This is useful if the application requires user input ot interaction.

Start the application in a seperate console window and then connect using 
gdb seperately.

Example using tmux start application in one terminal and then connect to
gdb in another session by attachine to the pid, as below.

```gdb --pid $(pgrep factorial01)```

# Printing values in memory
## p, print
```
(gdb) p 0x10570
(gdb) p {0x10570, 0x10574}
(gdb) p $r0
(gdb) p $cpsr
```
## print array starting at 0x10570, 100 elements
p *0x10570@100

## x, examine memory
using raw address
```
(gdb) x 0x10570
(gdb) x/4x 0x10570
```

examine variable from its symbol (pointer to adrress) (address_of_variablename) 
```
(gdb) x address_of_variablename
0x83bc <address_of_variablename>:       0x00010570
(gdb) x/12x 0x00010570
0x10570 <a>     0x00000000  0x00000001  0x00000002  0x00000003
0x10580 <a+16>  0x00000004  0x00000005  0x00000006  0x00000007
0x10590 <a+32>  0x00000008  0x00000009  0x0000000a  0x0000000b
```
```x/12x 0x00010570``` :this prints the 12 word values after the address given.

```x/-12x 0x00010570``` :this prints the 12 word values up to the address given. [gdb 7.2]

## As a String
```
x/s 0x10570
```

# Breakpoints
Add a breakpoint to the current line:
```
break
```

Show breakpoints:
```
info breakpoints
```

Remove breakpoints by number:
```
delete breakpoint 0-10
```

# Performance testing
linux perf tools
## check
```perf_3.2 stat -e cpu-cycles ./programname```

## performance test with input
```yes 19 | perf_3.2 stat --log-fd=3 --repeat=10 -e clock-cycles ./programname 3>&1```

## 
perf_3.2 list

# Info
Display info about a symbol, pointer to variable.
```
info address symbolname
info address addr_of_a
```

## Look into these info functions
```
info files
info float
info function
info handle
info inferiors
info line
info locals
info macro
info proc
info program
info registers
info scope
info selectors
info skips
info stack
info symbol
info target
info tasks
info terminal
info threads
info tracepoints
info tvariables
info types
info variables
info vector
info watchpoints
info win
```

# Notes
p *array@len

http://www.delorie.com/gnu/docs/gdb/gdb_54.html
http://visualgdb.com/gdbreference/commands/x

# References
[1] http://visualgdb.com/gdbreference/commands/
[2] https://sourceware.org/gdb/onlinedocs/gdb/Memory.html
[3] http://visualgdb.com/gdbreference/commands/x
