# CT213 Computer Systems and Organisation

Computer Systems History and Architecture Development; Von Neumann machine; memory systems; storage media; virtual and cache memory; interrupts; concurrency and pipelining; processes; scheduling; critical regions and synchronisation; file systems and management; distributed operating systems and parallel processing; case studies; UNIX, MSDOS and Windows NT.
